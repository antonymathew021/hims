﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Core;
using HIMS.Data.Context;
using HIMS.Data.Entities;
using HIMS.Data.Repository; 
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq; 
namespace HIMS.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private HIMSContext _dbContext { get; set; }
        public Dictionary<Type, object> _repositories = new Dictionary<Type, object>(); 

        [Inject]
        public ISFLSession Session { get; set; }
        public UnitOfWork()
        {
            CreateDbContext();
        }
        public void Commit()
        {
            _dbContext.SaveChanges();
        }
        public IRepository<TEntity, TPrimaryKey> Repository<TEntity, TPrimaryKey>() where TEntity : class, IEntity<TPrimaryKey>
        {
            if (_repositories.Keys.Contains(typeof(TEntity)))
            {
                return _repositories[typeof(TEntity)] as IRepository<TEntity, TPrimaryKey>;
            }

            IRepository<TEntity, TPrimaryKey> repository = new Repository<TEntity, TPrimaryKey>(_dbContext);
            _repositories.Add(typeof(TEntity), repository);
            return repository;
        } 
         
        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public void CreateDbContext()
        {
            _dbContext = new HIMSContext(); 
        }

        public HIMSContext InitializeDbContext()
        {
            _dbContext = new HIMSContext();
            return _dbContext;
        }

        public IRepository Repository()
        {
            IRepository repository = new HIMS.Data.Repository.Repository(_dbContext);
            return repository;
        }
    }
}
