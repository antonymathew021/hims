﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Core;
using HIMS.Data.Context;
using HIMS.Data.Entities;
using HIMS.Data.Repository;  

namespace HIMS.Data.UnitOfWork
{
    public interface IUnitOfWork
    {
        void Commit();
        HIMSContext InitializeDbContext();
        IRepository<TEntity, TPrimaryKey> Repository<TEntity, TPrimaryKey>() where TEntity : class, IEntity<TPrimaryKey>;
        IRepository Repository();
        ISFLSession Session { get; set; }

    }
}
