﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 
using HIMS.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using HIMS.Data.Context;
using HIMS.Data.Entities;
using HIMS.Core.Extensions;
using System.Reflection;

namespace HIMS.Data.Repository
{
	public class Repository<TEntity> : Repository<TEntity, int>
		where TEntity : class, IEntity<int>

	{
		public Repository(HIMSContext dbContext)
			: base(dbContext)
		{
		}

	}
	public class Repository : IRepository
	{
		HIMSContext _dbContext;

		public Repository(HIMSContext dbContext)
		{
			_dbContext = dbContext;
		}

		public string getUserNameById(long userId)
		{

			var usersWithRoles = (from user in _dbContext.Users.Where(model => model.Id == userId)
								  select new
								  {
									  Name = user.FirstName + " " + user.LastName,
								  }).FirstOrDefault();

			if (usersWithRoles != null)
				return usersWithRoles.Name.ToString();
			else
				return "";
		}

		public ApplicationUser GetUserDataById(long userId)
		{

			var usersWithRoles = _dbContext.Users.FirstOrDefault(model => model.Id == userId);

			return usersWithRoles;
		}

		public string GetDelimData(string colName)
		{
			return _dbContext.MstCombos.FirstOrDefault(model => model.Fieldname == colName).Fieldvalue;
		}
	}
	public class Repository<TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey>
		where TEntity : class, IEntity<TPrimaryKey>
	{

		protected HIMSContext DbContext { get; set; }
		protected DbSet<TEntity> DbSet { get; set; }
		//protected long CurrentUserId {get;set;} 
		public Repository(HIMSContext dbContext)
		{
			if (dbContext == null)
				throw new ArgumentNullException("dbContext");
			else
			{
				DbContext = dbContext;
				DbSet = DbContext.Set<TEntity>();

			}
		}

		public IQueryable<TEntity> GetAll()
		{
			return DbSet.AsNoTracking();
		}

		public IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] propertySelectors)
		{
			if (propertySelectors.IsNullOrEmpty())
			{
				return GetAll();
			}

			var query = GetAll();

			foreach (var propertySelector in propertySelectors)
			{
				query = query.Include(propertySelector);
			}

			return query;
		}
		public async Task<List<TEntity>> GetAllListAsync()
		{
			return await GetAll().ToListAsync();
		}




		public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
		{
			return DbSet.Where(predicate);
		}

		public TEntity GetById(object id)
		{
			return DbSet.Find(id);
		}



		public void Update(TEntity entity)
		{
			DbSet.Attach(entity);
			DbContext.Entry(entity).State = EntityState.Modified;
		}



		public List<TEntity> GetAllList()
		{
			return GetAll().ToList();
		}

		public List<TEntity> GetAllList(Expression<Func<TEntity, bool>> predicate)
		{
			return GetAll().Where(predicate).ToList();
		}

		public async Task<List<TEntity>> GetAllListAsync(Expression<Func<TEntity, bool>> predicate)
		{
			return await GetAll().Where(predicate).ToListAsync();
		}

		public TEntity Get(TPrimaryKey id)
		{
			var entity = FirstOrDefault(id);
			if (entity == null)
			{
				throw new EntityNotFoundException(typeof(TEntity), id);
			}

			return entity;
		}

		public async Task<TEntity> GetAsync(TPrimaryKey id)
		{
			var entity = await FirstOrDefaultAsync(id);
			if (entity == null)
			{
				throw new EntityNotFoundException(typeof(TEntity), id);
			}

			return entity;
		}

		public TEntity Single(Expression<Func<TEntity, bool>> predicate)
		{
			return GetAll().Single(predicate);
		}

		public async Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> predicate)
		{
			return await GetAll().SingleAsync(predicate);
		}

		public TEntity FirstOrDefault(TPrimaryKey id)
		{
			return GetAll().FirstOrDefault(CreateEqualityExpressionForId(id));
		}

		public async Task<TEntity> FirstOrDefaultAsync(TPrimaryKey id)
		{
			return await GetAll().FirstOrDefaultAsync(CreateEqualityExpressionForId(id));
		}

		public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
		{
			return GetAll().FirstOrDefault(predicate);
		}

		public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
		{
			return await GetAll().FirstOrDefaultAsync(predicate);
		}

		public TEntity Load(TPrimaryKey id)
		{
			return Get(id);
		}

		protected virtual Expression<Func<TEntity, bool>> CreateEqualityExpressionForId(TPrimaryKey id)
		{
			var lambdaParam = Expression.Parameter(typeof(TEntity));

			var lambdaBody = Expression.Equal(
				Expression.PropertyOrField(lambdaParam, "Id"),
				Expression.Constant(id, typeof(TPrimaryKey))
				);

			return Expression.Lambda<Func<TEntity, bool>>(lambdaBody, lambdaParam);
		}
		protected virtual void AttachIfNot(TEntity entity)
		{
			if (!DbSet.Local.Contains(entity))
			{
				DbSet.Attach(entity);
			}
		}


		public TEntity Insert(TEntity newEntity)
		{
			var hospitalId = DbContext.Users.FirstOrDefault(u => u.Id == newEntity.Addedby).HospitalMasterId; 
			long firstId = 0;
			var Id1 = GetAll().OrderByDescending(u => u.Id).FirstOrDefault(u => u.HospitalMasterId == hospitalId);
			if (Id1 == null)
				firstId = 0;
			else
				firstId = (long)(object)Id1.Id1;
			var entityId = hospitalId + "000" + "" + (Convert.ToInt64(firstId) + 1);
			newEntity.Id = (TPrimaryKey)Convert.ChangeType(entityId, typeof(TPrimaryKey));
			newEntity.Id1 = (TPrimaryKey)Convert.ChangeType((Convert.ToInt64(firstId) + 1), typeof(TPrimaryKey));//(Convert.ToInt64(Id1) + 1);
			newEntity.HospitalMasterId = hospitalId;
			return DbSet.Add(newEntity);

		}

		public Task<TEntity> InsertAsync(TEntity entity)
		{
			var hospitalId = DbContext.Users.FirstOrDefault(u => u.Id == entity.Addedby).HospitalMasterId;
			long firstId = 0;
			var Id1 = GetAll().OrderByDescending(u => u.Id).FirstOrDefault(u => u.HospitalMasterId == hospitalId);
			if (Id1 == null)
				firstId = 0;
			else
				firstId = (long)(object)Id1.Id1;
			var entityId = hospitalId + "000" + "" + (Convert.ToInt64(firstId) + 1);
			entity.Id = (TPrimaryKey)Convert.ChangeType(entityId, typeof(TPrimaryKey));
			entity.Id1 = (TPrimaryKey)Convert.ChangeType((Convert.ToInt64(firstId) + 1), typeof(TPrimaryKey));
			entity.HospitalMasterId = hospitalId;
			return Task.FromResult(DbSet.Add(entity));
		}


		public TPrimaryKey InsertAndGetId(TEntity entity)
		{
			var hospitalId = DbContext.Users.FirstOrDefault(u => u.Id == entity.Addedby).HospitalMasterId; 
			long firstId = 0;
			var Id1 = GetAll().OrderByDescending(u => u.Id).FirstOrDefault(u => u.HospitalMasterId == hospitalId);
			if (Id1 == null)
				firstId = 0;
			else
				firstId = (long)(object)Id1.Id1;
			var entityId = hospitalId + "000" + "" + (Convert.ToInt64(firstId) + 1);
			entity.Id = (TPrimaryKey)Convert.ChangeType(entityId, typeof(TPrimaryKey));
			entity.Id1 = (TPrimaryKey)Convert.ChangeType((Convert.ToInt64(firstId) + 1), typeof(TPrimaryKey));//(Convert.ToInt64(Id1) + 1);
			entity.HospitalMasterId = hospitalId;
			entity = Insert(entity);

			DbContext.SaveChanges();

			return entity.Id;
		}

		public async Task<TPrimaryKey> InsertAndGetIdAsync(TEntity entity)
		{
			var hospitalId = DbContext.Users.FirstOrDefault(u => u.Id == entity.Addedby).HospitalMasterId; 
			long firstId = 0;
			var Id1 = GetAll().OrderByDescending(u => u.Id).FirstOrDefault(u => u.HospitalMasterId == hospitalId);
			if (Id1 == null)
				firstId = 0;
			else
				firstId = (long)(object)Id1.Id1;
			var entityId = hospitalId + "000" + "" + (Convert.ToInt64(firstId) + 1);
			entity.Id = (TPrimaryKey)Convert.ChangeType(entityId, typeof(TPrimaryKey));
			entity.Id1 = (TPrimaryKey)Convert.ChangeType((Convert.ToInt64(firstId) + 1), typeof(TPrimaryKey));//(Convert.ToInt64(Id1) + 1);
			entity.HospitalMasterId = hospitalId;
			entity = await InsertAsync(entity);

			await DbContext.SaveChangesAsync();

			return entity.Id;
		}

		TEntity IRepository<TEntity, TPrimaryKey>.Update(TEntity entity)
		{
			AttachIfNot(entity);
			DbContext.Entry(entity).State = EntityState.Modified;
			return entity;
		}

		public Task<TEntity> UpdateAsync(TEntity entity)
		{
			AttachIfNot(entity);
			DbContext.Entry(entity).State = EntityState.Modified;
			return Task.FromResult(entity);
		}

		public TEntity Update(TPrimaryKey id, Action<TEntity> updateAction)
		{
			var entity = Get(id);
			updateAction(entity);
			return entity;
		}

		public async Task<TEntity> UpdateAsync(TPrimaryKey id, Func<TEntity, Task> updateAction)
		{
			var entity = await GetAsync(id);
			await updateAction(entity);
			return entity;
		}
		public void Delete(TEntity entity)
		{
			AttachIfNot(entity);
			DbSet.Remove(entity);
		}
		public Task DeleteAsync(TEntity entity)
		{
			Delete(entity);
			return Task.FromResult(0);
		}

		public void Delete(TPrimaryKey id)
		{
			var entity = DbSet.Local.FirstOrDefault(ent => EqualityComparer<TPrimaryKey>.Default.Equals(ent.Id, id));
			if (entity == null)
			{
				entity = FirstOrDefault(id);
				if (entity == null)
				{
					return;
				}
			}

			Delete(entity);
		}

		public Task DeleteAsync(TPrimaryKey id)
		{
			Delete(id);
			return Task.FromResult(0);
		}

		public void Delete(Expression<Func<TEntity, bool>> predicate)
		{
			foreach (var entity in GetAll().Where(predicate).ToList())
			{
				Delete(entity);
			}
		}

		public Task DeleteAsync(Expression<Func<TEntity, bool>> predicate)
		{
			Delete(predicate);
			return Task.FromResult(0);
		}

		public int Count()
		{
			return GetAll().Count();
		}


		public Task<int> CountAsync()
		{
			return Task.FromResult(Count());
		}

		public int Count(Expression<Func<TEntity, bool>> predicate)
		{
			return GetAll().Where(predicate).Count();
		}

		public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate)
		{
			return Task.FromResult(Count(predicate));
		}

		public long LongCount()
		{
			return GetAll().LongCount(); ;
		}

		public Task<long> LongCountAsync()
		{
			return Task.FromResult(LongCount());
		}

		public long LongCount(Expression<Func<TEntity, bool>> predicate)
		{
			return GetAll().Where(predicate).LongCount();
		}

		public Task<long> LongCountAsync(Expression<Func<TEntity, bool>> predicate)
		{
			return Task.FromResult(LongCount(predicate));
		}

		public int getNewIdentityKey(string tableName)
		{
			int varMaxAdvertiseId = Convert.ToInt32(DbContext.Database.SqlQuery<decimal>("Select IDENT_CURRENT ('" + tableName + "')", new object[0]).FirstOrDefault());
			return varMaxAdvertiseId;
		}

	}
}