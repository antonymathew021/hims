﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

namespace HIMS.Data.Enumaration
{
	public enum SystemRole
	{ 
		Admin = 1,

		Doctor=2,

		Receptionist=3,
		
		Nurse=4,
		
		Accountent=5,
		
		Patient=6
	}
}
