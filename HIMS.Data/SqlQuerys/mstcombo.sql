 
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('recstatus', N'Active!Inactive')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('prefix', N'!Mr.!Ms.!Mrs.!Miss!Master!Baby!Dr.!Smt.')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('gender', N'Male!Female!Unknown!')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('bloodgroup', N'Unknown!A+!A-!B+!B-!AB+!AB-!O+!O-')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('stafftype', N'Doctor!Nurse!Ward Boy!Accountant!Administrator!Receptionist!Others')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('reftype', N'Doctor!Others')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('taxtype', N'Taxable!Non Taxable')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('salesinvtype', N'Retail Invoice!Tax Invoice')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('salestaxtype', N'!RD (within State)!OGS (Outside State)')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('ratetype', N'Inclusive!Exclusive')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('appstatus', N'Scheduled!Confirm!Waiting!Cancelled!Completed')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('paymentmode', N'!Cash Payment!Cheque Payment!Credit Card!Debit Card!Pay Card!Wallet')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('patcodeformat', N'!Year!Month!YearMonth!Prefix')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('maritalstatus', N'!Single!Separated!Divorced!Married')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('paymentfor', N'Against Bill!Advanced')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('admstatus', N'Admitted!Discharged')
INSERT [dbo].[mstcombo] ([Fieldname], [Fieldvalue]) VALUES ('remarksfor', N'Dose Remarks!Prescription Remarks')
