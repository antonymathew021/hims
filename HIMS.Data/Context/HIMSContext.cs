﻿using HIMS.Data.Authorization;
using HIMS.Data.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace HIMS.Data.Context
{
	public class HIMSContext : IdentityDbContext<ApplicationUser, CustomRole,
	long, CustomUserLogin, CustomUserRole, CustomUserClaim>
	{
		public HIMSContext() : base("DefaultConnection")
		{

		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{       
			modelBuilder.Types().Configure(entity => entity.ToTable(entity.ClrType.Name.ToLower()));
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			modelBuilder.Properties().Configure(c =>
            {
                var name = c.ClrPropertyInfo.Name;
                var newName = name.ToLower();
                c.HasColumnName(newName);
            }); 
			base.OnModelCreating(modelBuilder);
		}

		public static HIMSContext Create()
		{
			return new HIMSContext();
		}

		public DbSet<DesignationMaster> DesignationMasters { get; set; }
		public DbSet<DepartmentMaster> DepartmentMasters { get; set; }
		public DbSet<SpecialityMaster> SpecialityMasters { get; set; }
		public DbSet<CountryMaster> CountryMasters { get; set; }
		public DbSet<StateMaster> StateMasters { get; set; }
		public DbSet<CityMaster> CityMasters { get; set; }
		public DbSet<MstCombo> MstCombos { get; set; }
		public DbSet<HospitalMaster> HospitalMasters { get; set; }
		public DbSet<PagesMaster> PagesMasters { get; set; }
		public DbSet<ReferanceMaster> ReferanceMasters { get; set; }
		public DbSet<RemarkMaster> RemarkEntitys { get; set; }
		public DbSet<WorkTypeMaster> WorkTypeMasters { get; set; }
		public DbSet<VitalDataMaster> VitalDataMasters { get; set; }
		public DbSet<MedicineDrugMaster> MedicineDrugMasters { get; set; }
		public DbSet<MedicineCategoryMaster> MedicineCategoryMasters { get; set; }
		public DbSet<StaffMaster> StaffMasters { get; set; }
		public DbSet<TieupMaster> TieupMasters { get; set; }
		public DbSet<DosageMaster> DosageMasters { get; set; } 
        public DbSet<PatientRegistrationTransaction> PatientMasters { get; set; }
        public DbSet<DoctorOPDMaster> DoctorOPDMasters { get; set; }
    }
}
