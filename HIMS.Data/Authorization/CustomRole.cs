﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using Microsoft.AspNet.Identity.EntityFramework; 

namespace HIMS.Data.Authorization
{
   

    public class CustomRole : IdentityRole<long, CustomUserRole>
    {
        public CustomRole() { }
        public CustomRole(string name) { Name = name; }
    }
}
