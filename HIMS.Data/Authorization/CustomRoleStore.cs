﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using Microsoft.AspNet.Identity.EntityFramework;
using HIMS.Data.Context;

namespace HIMS.Data.Authorization
{
    public class CustomRoleStore : RoleStore<CustomRole, long, CustomUserRole>
    {
        public CustomRoleStore(HIMSContext context)
            : base(context)
        {
        }
    }
}
