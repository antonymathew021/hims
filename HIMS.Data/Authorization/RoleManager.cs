﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using Microsoft.AspNet.Identity; 

namespace HIMS.Data.Authorization
{
    public class RoleManager : RoleManager<CustomRole, long>
    {
        public RoleManager(CustomRoleStore store)
           : base(store)
        {
        }


    }

}
