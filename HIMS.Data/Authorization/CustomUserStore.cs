﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using Microsoft.AspNet.Identity.EntityFramework;
using HIMS.Data.Context;
using HIMS.Data.Entities;

namespace HIMS.Data.Authorization
{
    public class CustomUserStore : UserStore<ApplicationUser, CustomRole, long,
        CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public CustomUserStore(HIMSContext context)
            : base(context)
        {
        }
    }
}
