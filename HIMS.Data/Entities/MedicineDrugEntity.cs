// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class MedicineDrugMaster : Entity<long>
	{
		public string DrugName { get; set; }

		public string DrugDesc { get; set; }
	}
}

