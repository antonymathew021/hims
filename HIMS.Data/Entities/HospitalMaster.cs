﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Data.Entities
{
	public class HospitalMaster
	{
		public long Id { get; set; }

		public string HospitalName { get; set; }

		public string HospitalAddress { get; set; }

		public string HospitalMobile { get; set; } 
	}
}
