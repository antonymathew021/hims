﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

namespace HIMS.Data.Entities
{
	public class SpecialityMaster : Entity<long>
	{
		public string SpecialityCode { get; set; }

		public string SpecialityName { get; set; }
	}
}
