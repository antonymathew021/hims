﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Data.Entities
{
	public class CountryMaster : Entity<long>
	{
		public string Countrycode { get; set; }

		public string Countryname { get; set; }
	}
}
