// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class DosageMaster : Entity<long>
	{
		public string DosageCode { get; set; }

		public string DosageName { get; set; }

		public long? RemarkId { get; set; }

		public virtual RemarkMaster Remark { get; set; }
	}
}

