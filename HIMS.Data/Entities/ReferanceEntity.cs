// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class ReferanceMaster : Entity<long>
	{
		public string Reftype { get; set; }

		public string Refcode { get; set; }

		public string Refname { get; set; }

		public DateTime? BirthDate { get; set; }

		public long? AgeYears { get; set; }

		public long? AgeMonths { get; set; }

		public long? AgeDays { get; set; }

		public string Gender { get; set; }

		public string BloodGroup { get; set; }

		public long? SpeclId { get; set; }

		public string Regno { get; set; }

		public string Address1 { get; set; }

		public string Address2 { get; set; }

		public string Address3 { get; set; }

		public long? Pincode { get; set; }

		public long? CityId { get; set; }

		public long? StateId { get; set; }

        public long? Statecode { get; set; }

        public long? CountryId { get; set; }

		public long? Mobileno { get; set; }

		public string ContactNo { get; set; }

		public string Emailid { get; set; }

		public string Website { get; set; }

		public string PanNo { get; set; }

		public string GstNo { get; set; }

		public byte[] RefSign { get; set; }

		
		public virtual CountryMaster Country { get; set; } 
		public virtual StateMaster State { get; set; } 
		public virtual CityMaster City { get; set; } 
		public virtual SpecialityMaster Specl { get; set; }
	}
}

