﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System; 

namespace HIMS.Data.Entities
{
	public class IEntityBase
	{
		long Id { get; set; }
		string CreatedBy { get; set; }
		string UpdatedBy { get; set; }
		DateTime CreatedDate { get; set; }
		DateTime UpdatedDate { get; set; }
	}
}
