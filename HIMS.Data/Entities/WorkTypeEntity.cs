// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class WorkTypeMaster : Entity<long>
	{
		public string WorktypeCode { get; set; }

		public string WorktypeName { get; set; }
	}
}

