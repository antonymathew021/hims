﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

namespace HIMS.Data.Entities
{
	public class PagesMaster : Entity<long>
	{
		public string RightsName { get; set; }

		public string ModuleName { get; set; }

		public string Menu { get; set; }

		public string Url { get; set; }

		public string Icon { get; set; }
	}
}
