﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class DesignationMaster : Entity<long>
	{  
		public string DesignationCode { get; set; }
		 
		public string DesignationName { get; set; }
	}
}
