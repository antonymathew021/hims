﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Data.Entities
{
	public class RemarkMaster : Entity<long>
	{
		public string RemarkCode { get; set; }

		public string RemarkFor { get; set; }

		public string RemarkName { get; set; }

		public string Hinremarks { get; set; }

		public string Gujremarks { get; set; }

		public string Marremarks { get; set; }

		public string Arbremarks { get; set; }
	}
}
