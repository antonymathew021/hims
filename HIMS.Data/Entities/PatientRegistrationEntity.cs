// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class PatientRegistrationTransaction : Entity<long>
	{
		public string Uhid { get; set; }

		public long? Uhid1 { get; set; }

		public DateTime? Regdate { get; set; }

		public DateTime? Regtime { get; set; }

		public byte[] Patimage { get; set; }

		public string Prefix { get; set; }

        [Required]
        public string FirstName { get; set; }

		public string MiddleName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public DateTime? BirthDate { get; set; }

		public long? AgeYears { get; set; }

		public long? AgeMonths { get; set; }

		public long? AgeDays { get; set; }

		public string Gender { get; set; }

		public string Bloodgroup { get; set; }

		public string MaritalStatus { get; set; }

		public string Refbyid { get; set; }

		public string Occupation { get; set; }

		public string Nationality { get; set; }

		public string TieupId { get; set; }

		public string Address1 { get; set; }

		public string Address2 { get; set; }

		public string Address3 { get; set; }

		public long? Pincode { get; set; }

		public string Cityid { get; set; }

		public string Stateid { get; set; }

		public string Countryid { get; set; }

		public long? Mobileno { get; set; }

		public string Contact { get; set; }

		public string Website { get; set; }

		public string Emailid { get; set; }

        [DataType(DataType.MultilineText)]
        public string Allergydtls { get; set; }

        [DataType(DataType.MultilineText)]
        public string Spnotes { get; set; }

		public string IdCardNo { get; set; }

		public string PatType { get; set; }
	}
}

