﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

namespace HIMS.Data.Entities
{
	public class CityMaster : Entity<long>
	{
		public string Citycode { get; set; }

		public string Cityname { get; set; }

		public long StateMasterId { get; set; }

		public long CountryMasterId { get; set; }

		public virtual CountryMaster CountryMaster { get; set; }

		public virtual StateMaster StateMaster { get; set; }
	}
}
