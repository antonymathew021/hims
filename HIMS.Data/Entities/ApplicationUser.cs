﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using HIMS.Data.Authorization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework; 
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
using System.Security.Claims; 
using System.Threading.Tasks;

namespace HIMS.Data.Entities
{
	public class ApplicationUser : IdentityUser<long, CustomUserLogin, CustomUserRole,
    CustomUserClaim>
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, long> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        public ApplicationUser()
        {
            isActiveUser = true; 
            isDeleted = false;
        }

        [Required(ErrorMessage = "FirstName is required")]
        [StringLength(64)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        [StringLength(64)]
        public string LastName { get; set; }    

        public bool isActiveUser { get; set; }

        public bool isDeleted { get; set; }

		public long? HospitalMasterId { get; set; }

		public virtual HospitalMaster HospitalMaster { get; set; }

		[NotMapped]
        public string Name
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }
    }
}
