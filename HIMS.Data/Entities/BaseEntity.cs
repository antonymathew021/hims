﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HIMS.Data.Entities
{
	public interface IEntity
	{
		object Id { get; set; }
		object Id1 { get; set; }
		long? HospitalMasterId { get; set; }
		string Recstatus { get; set; }
		long? Addedby { get; set; }
		DateTime? Addeddate { get; set; }
		long? Updatedby { get; set; }
		DateTime? Updateddatetime { get; set; }
		bool Delflg { get; set; }
		long? Delby { get; set; }
		DateTime? Deldatetime { get; set; }
		long? Syncflg { get; set; }

	}

	public interface IEntity<T> : IEntity
	{
		new T Id { get; set; }
		new T Id1 { get; set; }
	}

	public abstract class Entity<T> : IEntity<T>
	{
		[Key]
		[Column(Order = 0)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)] 
		public T Id { get; set; }
		[Column(Order = 1)]
		public T Id1 { get; set; }
		public long? Addedby { get; set; }
		private DateTime? createdDate;
		[DataType(DataType.DateTime)]
		public DateTime? Addeddate
		{
			get { return createdDate ?? DateTime.UtcNow; }
			set { createdDate = value; }
		}
		public long? Updatedby { get; set; }
		public DateTime? Updateddatetime { get; set; }
		public bool Delflg { get; set; }
		public long? Delby { get; set; }
		public DateTime? Deldatetime { get; set; }
		public long? Syncflg { get; set; }
		public string Recstatus { get; set; }
		public long? HospitalMasterId { get; set; }

		object IEntity.Id
		{
			get { return this.Id; }
			set { this.Id = (T)Convert.ChangeType(value, typeof(T)); }

		}

		object IEntity.Id1
		{
			get { return this.Id1; }
			set { this.Id1 = (T)Convert.ChangeType(value, typeof(T)); }
		}
	}
}
