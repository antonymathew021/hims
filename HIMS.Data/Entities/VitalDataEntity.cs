// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class VitalDataMaster : Entity<long>
	{
		public string VitalCode { get; set; }

		public string VitalName { get; set; }

		public string VitalUnit { get; set; }
	}
}

