﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
namespace HIMS.Data.Entities
{
	public class MstCombo
	{
		public long Id { get; set; }

		public string Fieldname { get; set; }

		public string Fieldvalue { get; set; }
	}
}
