// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class MedicineCategoryMaster : Entity<long>
	{
		public string CategoryName { get; set; }

		public string CategoryDesc { get; set; }
	}
}

