// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class AppointmentEntryTransaction : Entity<long>
	{
		public string PatId { get; set; }

		public DateTime? AppDate { get; set; }

		public DateTime? AppTime { get; set; }

		public string DepartmentId { get; set; }

		public string DoctorId { get; set; }

		public string Appointmentfor { get; set; }

		public long? AppNo { get; set; }

		public string Appstatus { get; set; }

		public DateTime? StTime { get; set; }

		public DateTime? EndTime { get; set; }
	}
}

