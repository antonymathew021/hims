// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class StaffMaster : Entity<long>
	{
		public string StaffType { get; set; }

		public string StaffCode { get; set; }

		public string Prefix { get; set; }

		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		public string LastName { get; set; }

		public DateTime? BirthDate { get; set; }

		public long? AgeYears { get; set; }

		public long? AgeMonths { get; set; }

		public long? AgeDays { get; set; }

		public string Gender { get; set; }

		public string BloodGroup { get; set; }

		public long? DeptId { get; set; }

		public long? DesigId { get; set; }

		public long? SpeclId { get; set; }

		public string RegNo { get; set; }

		public string Address1 { get; set; }

		public string Address2 { get; set; }

		public string Address3 { get; set; }

		public long? PinCode { get; set; }

		public long? CityId { get; set; }

		public long? StateId { get; set; }

		public long? CountryId { get; set; }

		public long? StdCode { get; set; }

		public long? PhoneNo { get; set; }

		public long? MobileNo { get; set; }

		public string ContactNo { get; set; }

		public string EmailId { get; set; }

		public string Website { get; set; }

		public string PanNo { get; set; }

		public string GstNo { get; set; }

		public byte[] ProfPhoto { get; set; }

		public byte[] StaffSign { get; set; }

		public virtual CountryMaster Country { get; set; }

		public virtual StateMaster State { get; set; }

		public virtual CityMaster City { get; set; }

		public virtual DesignationMaster Desig { get; set; }

		public virtual DepartmentMaster Dept { get; set; }

		public virtual SpecialityMaster Spec { get; set; }
	}
}

