// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class DoctorOPDMaster : Entity<long>
	{
		public long DoctorId { get; set; }

		public string WeekType { get; set; }

		public DateTime? MorningTimeFrom { get; set; }

		public DateTime? MorningTimeTo { get; set; }

		public bool IsAvailableMorning { get; set; }

		public DateTime? AfterNoonTimeFrom { get; set; }

		public DateTime? AfterNoonTimeTo { get; set; }

		public bool IsAvailableAfterNoon { get; set; }

		public DateTime? EveningTimeFrom { get; set; }

		public DateTime? EveningTimeTo { get; set; }

		public bool IsAvailableEvening { get; set; }

		public virtual StaffMaster Doctor { get; set; }

	}
}

