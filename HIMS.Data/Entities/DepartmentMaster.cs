﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

namespace HIMS.Data.Entities
{
	public class DepartmentMaster : Entity<long>
	{  
		public string DepartmentCode { get; set; }
		 
		public string DepartmentName { get; set; }
	}
}
