// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class MedicineMaster : Entity<long>
	{
		public string Medtype { get; set; }

		public string Scheduleofdrug { get; set; }

		public string Medhsncode { get; set; }

		public string Medbarcode { get; set; }

		public string Medname { get; set; }

		public long? DrugId { get; set; }

		public long? CategoryId { get; set; }

		public string Medcompany { get; set; }

		public long? RemarkId { get; set; }

		public double? Minqty { get; set; }

		public double? Maxqty { get; set; }

		public double? Reorderqty { get; set; }

		public bool? Narcoticcat { get; set; }

		public bool? Dpco { get; set; }

		public bool? Medh1 { get; set; }

		public bool? Medotc { get; set; }

		public double? Cgstper { get; set; }

		public double? Sgstper { get; set; }

		public double? Igstper { get; set; }

		public string Medcontents { get; set; }
	}
}

