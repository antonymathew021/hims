// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System.ComponentModel.DataAnnotations;

namespace HIMS.Data.Entities
{
	public class TieupMaster : Entity<long>
	{
		public string Tieupcode { get; set; }

		public string Compname { get; set; }

		public string Address1 { get; set; }

		public string Address2 { get; set; }

		public string Address3 { get; set; }

		public long? Pincode { get; set; }

		public long? Cityid { get; set; }

		public long? Stateid { get; set; }

		public long? Countryid { get; set; }

		public long? Mobileno { get; set; }

		public string Contact { get; set; }

		public string Emailid { get; set; }

		public string Website { get; set; }

		public string Panno { get; set; }

		public string Gstno { get; set; }

		public string Contpername { get; set; }

		public string Designation { get; set; }

		public string Regno { get; set; }
	}
}

