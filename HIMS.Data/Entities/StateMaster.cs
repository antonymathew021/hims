﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

namespace HIMS.Data.Entities
{
	public class StateMaster : Entity<long>
	{
		public string Statecode { get; set; }

		public string Statename { get; set; }

		public long CountryMasterId { get; set; }

		public virtual CountryMaster CountryMaster { get; set; }
	}
}
