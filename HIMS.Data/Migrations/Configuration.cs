﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
namespace HIMS.Data.Migrations
{
	using HIMS.Core.Extensions;
	using HIMS.Data.Authorization;
	using HIMS.Data.Context;
	using HIMS.Data.Entities;
	using HIMS.Data.Enumaration;
	using Microsoft.AspNet.Identity;
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Data.Entity.Validation;
	using System.IO;
	using System.Linq;
	using System.Reflection;
	using System.Text;

	public class Configuration : DbMigrationsConfiguration<HIMS.Data.Context.HIMSContext>
	{
		public Configuration()
		{
			AutomaticMigrationDataLossAllowed = AutomaticMigrationsEnabled = true;
		}

		protected override void Seed(HIMSContext context)
		{
			try
			{
				SeedHospitel(context);
				SeedMembership(context);
				SeedAdminUser(context);
				SeedMstCombo(context);
				PageMasters(context); 
				context.SaveChanges();
			}
			catch (DbEntityValidationException e)
			{
				string errorMessage = string.Empty;
				foreach (var eve in e.EntityValidationErrors)
				{
					errorMessage +=
						$"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
					foreach (var ve in eve.ValidationErrors)
					{
						errorMessage += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"";
					}
				}
				Exception ex = new Exception(errorMessage);
				//ex.LogError(this);
				throw ex;
			}
			catch (Exception)
			{
				// ex.LogError(this);
			}
		}
		private static void SeedAdminUser(HIMSContext context)
		{
			var store = new CustomUserStore(context);
			var manager = new UserManager<ApplicationUser, long>(store);
			var user = new ApplicationUser
			{
				UserName = "admin",
				Email = "admin@vishay.com",
				FirstName = "Roger",
				LastName = "Roberts",
				EmailConfirmed = true,
				HospitalMasterId=1
			};
			if (!manager.Users.Any(x => x.UserName.ToLower().Trim().ToString().Contains(user.UserName.ToLower().Trim().ToString())))
			{
				manager.Create(user, "admin123$");
				manager.AddToRole(user.Id, "Admin");
			}
		}

		private static void SeedMembership(HIMSContext context)
		{
			var values = Enum.GetNames(typeof(SystemRole));
			var store = new CustomRoleStore(context);
			var manager = new RoleManager<CustomRole, long>(store);
			foreach (var item in values)
			{
				var role = new CustomRole { Name = item };
				manager.Create(role);
			}
		}

		private static void SeedHospitel(HIMSContext context)
		{
			context.HospitalMasters.Add(new HospitalMaster
			{
				HospitalName="Admin Hospitel",
				HospitalAddress="Vadodara",
				HospitalMobile="9173713178"
			});
		}

		private static void SeedMstCombo(HIMSContext context)
		{
			if (!context.MstCombos.Any())
			{
				using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("HIMS.Data.SqlQuerys.mstcombo.sql"))
				{
					if (stream != null)
					{
						using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
						{
							var scritpt = reader.ReadToEnd();
							context.Database.ExecuteSqlCommand(scritpt);
						}
					}
				}
			}
		}

		private static void PageMasters(HIMSContext context)
		{
			if (!context.PagesMasters.Any())
			{
				using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("HIMS.Data.SqlQuerys.pagemasters.sql"))
				{
					if (stream != null)
					{
						using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
						{
							var scritpt = reader.ReadToEnd();
							context.Database.ExecuteSqlCommand(scritpt);
						}
					}
				}
			}
		}
	}
}
