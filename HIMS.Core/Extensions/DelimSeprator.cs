﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Core.Extensions
{
	public static class DelimSeprator
	{
		public static List<string> DelimToList(string delim)
		{
			List<string> data = new List<string>();
			data = delim.Split('!').ToList(); 
			return data;
		}
	}
}
