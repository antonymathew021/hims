﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System.IO;
using System.Linq;

namespace HIMS.Core.Extensions
{
	public static class StreamExtensions
	{
		public static byte[] GetAllBytes(this Stream stream)
		{
			using (var memoryStream = new MemoryStream())
			{
				stream.CopyTo(memoryStream);
				return memoryStream.ToArray();
			}
		}
	}
}
