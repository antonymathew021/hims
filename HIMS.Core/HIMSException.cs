﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Core
{
    [Serializable]
    public class HIMSException : System.Exception
    {
        /// <summary>
        /// Creates a new <see cref="HIMSException"/> object.
        /// </summary>
        public HIMSException()
        {

        }

        /// <summary>
        /// Creates a new <see cref="HIMSException"/> object.
        /// </summary>
        public HIMSException(SerializationInfo serializationInfo, StreamingContext context)
            : base(serializationInfo, context)
        {

        }

        /// <summary>
        /// Creates a new <see cref="HIMSException"/> object.
        /// </summary>
        /// <param name="message">Exception message</param>
        public HIMSException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Creates a new <see cref="HIMSException"/> object.
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner exception</param>
        public HIMSException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
