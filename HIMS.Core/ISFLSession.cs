﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

namespace HIMS.Core
{
	public interface ISFLSession
	{
		long? UserId { get; }
		long? HospitalMasterId { get; }
		string UserName { get; }
	}
}
