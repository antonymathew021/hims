// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.DosageServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.DosageServices
{
	public class DosageMasterService : IDosageMasterService
	{
		private readonly IUnitOfWork _unitOfWork;

		public DosageMasterService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<Tuple<List<DosageList>, int, int>> GetAll(DataTableAjaxPostModel<DosageList> modelDT)
		{
			try
			{
				int totalrows, totalrowsafterfiltering = 0;
				var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
				var length = modelDT.length;
				var start = modelDT.start;
				string sortColumnName = string.Empty;
				string sortDirection = string.Empty;
				string searchValue = string.Empty;
				if (searchTerm != null)
				{
					searchValue = searchTerm.ToString();
				}
				if (modelDT.order != null)
				{
					sortColumnName = modelDT.columns[modelDT.order[0].column].data;
					if(sortColumnName.ToLower().Equals("dosagecode"))
					{
						sortColumnName="DosageCode";
					}
					else if(sortColumnName.ToLower().Equals("dosagename"))
					{
						sortColumnName="DosageName";
					} 
					else if(sortColumnName.ToLower().Equals("remarkname"))
					{ 
						sortColumnName="Remark.RemarkName";
					}
					
					else if(sortColumnName.ToLower().Equals("recstatus"))
					{ 
						sortColumnName="Recstatus";
					}
					sortDirection = modelDT.order[0].dir.ToLower();
				}

				var filterData = _unitOfWork.Repository<DosageMaster, long>().GetAll().Where(u => !u.Delflg).OrderBy(sortColumnName + " " + sortDirection)
					 .Skip(start).Take(length).Select(u => new DosageList
					 {
						 Id = u.Id,
						 DosageCode = u.DosageCode,
						 DosageName = u.DosageName,
						 Remark = u.Remark.RemarkName,
						 Recstatus = u.Recstatus
					 }).ToList();

				//var dosageMaster = Mapper.Map<List<DosageDto>>(filterData);
				totalrows = _unitOfWork.Repository<DosageMaster, long>().GetAll().Count();
				return new Tuple<List<DosageList>, int, int>(filterData, totalrows, totalrowsafterfiltering);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<DosageDto> GetById(long Id)
		{
			try
			{
				var filteredData = await _unitOfWork.Repository<DosageMaster, long>().SingleAsync(u => u.Id == Id);
				if (filteredData != null)
					return Mapper.Map<DosageMaster, DosageDto>(filteredData);
				else return null;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public async Task<long> CreateAsync(DosageDto entity)
		{
			var data = Mapper.Map<DosageMaster>(entity);
			data.Addedby = _unitOfWork.Session.UserId;
			data.Addeddate = DateTime.Now;
			data.Syncflg = 0;
			var city = await _unitOfWork.Repository<DosageMaster, long>().InsertAsync(data);
			_unitOfWork.Commit();
			return city.Id;
		}

		public async Task Delete(long Id)
		{

			var data = await _unitOfWork.Repository<DosageMaster, long>().SingleAsync(u => u.Id == Id);
			data.Delflg = true;
			data.Delby = _unitOfWork.Session.UserId;
			data.Deldatetime = DateTime.Now; 
			await _unitOfWork.Repository<DosageMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}

		public async Task Update(DosageDto entity)
		{
			var data = Mapper.Map<DosageMaster>(entity);

			data.Updatedby = _unitOfWork.Session.UserId;
			data.Updateddatetime = DateTime.Now;
			data.Syncflg = 1;
			await _unitOfWork.Repository<DosageMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}
	}
}

