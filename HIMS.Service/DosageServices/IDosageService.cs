// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.DosageServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.DosageServices
{
	public interface IDosageMasterService
	{
		Task<Tuple<List<DosageList>, int, int>> GetAll(DataTableAjaxPostModel<DosageList> modelDT);


		Task<DosageDto> GetById(long id);

		Task<long> CreateAsync(DosageDto entity);

		Task Update(DosageDto entity);

		Task Delete(long id);
	}
}

