// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.DosageServices.Dto
{
	public class DosageDto
	{
		public long? Id { get; set; }

		public string DosageCode { get; set; }

		public string DosageName { get; set; }

		public long? RemarkId { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}

	public class DosageList
	{
		public long Id { get; set; }

		public string DosageCode { get; set; }

		public string DosageName { get; set; }

		public string Remark { get; set; }

		public string Recstatus { get; set; }

	}
}

