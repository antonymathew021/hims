// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.StaffServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.StaffServices
{
    public class StaffMasterService : IStaffMasterService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StaffMasterService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Tuple<List<StaffDto>, int, int> GetAll(DataTableAjaxPostModel<StaffDto> modelDT)
        {
            try
            {
                int totalrows, totalrowsafterfiltering = 0;
                var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
                var length = modelDT.length;
                var start = modelDT.start;
                string sortColumnName = string.Empty;
                string sortDirection = string.Empty;
                string searchValue = string.Empty;
                if (searchTerm != null)
                {
                    searchValue = searchTerm.ToString();
                }
                if (modelDT.order != null)
                {
                    sortColumnName = modelDT.columns[modelDT.order[0].column].data;
                    sortDirection = modelDT.order[0].dir.ToLower();
                }

                var staffMaster = _unitOfWork.Repository<StaffMaster, long>().GetAll().Where(u => !u.Delflg).OrderBy(sortColumnName + " " + sortDirection)
                     .Skip(start).Take(length).Select(u => new StaffDto
                     {
                         Id = u.Id,
                         DepartmentName = u.Dept.DepartmentName,
                         StaffMobile = u.MobileNo.HasValue ? u.MobileNo.Value.ToString() : "",
                         StaffName = u.FirstName + " " + u.MiddleName + " " + u.LastName + "",
                         StaffType = u.StaffType,
                         Recstatus = u.Recstatus
                     }).ToList();

                totalrows = _unitOfWork.Repository<StaffMaster, long>().GetAll().Count();
                return new Tuple<List<StaffDto>, int, int>(staffMaster, totalrows, totalrowsafterfiltering);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<StaffCreateDto> GetById(long Id)
        {
            try
            {
                var filteredData = await _unitOfWork.Repository<StaffMaster, long>().SingleAsync(u => u.Id == Id);
                if (filteredData != null)
                    return Mapper.Map<StaffMaster, StaffCreateDto>(filteredData);
                else return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<long> CreateAsync(StaffCreateDto entity)
        {
            var data = Mapper.Map<StaffMaster>(entity);
            data.Addedby = _unitOfWork.Session.UserId;
            data.Addeddate = DateTime.Now;
            data.Syncflg = 0;
            var city = await _unitOfWork.Repository<StaffMaster, long>().InsertAsync(data);
            _unitOfWork.Commit();

            //insert data in doctor opd schedule
            if (data.StaffType.Equals("Doctor"))
            {
                foreach (var item in Enum.GetValues(typeof(DayOfWeek)))
                {
                    DoctorOPDMaster doctorOPDMaster = new DoctorOPDMaster();
                    doctorOPDMaster.DoctorId = city.Id;
                    doctorOPDMaster.WeekType = item.ToString();
                    doctorOPDMaster.Addedby = _unitOfWork.Session.UserId;
                    doctorOPDMaster.Addeddate = DateTime.Now;
                    await _unitOfWork.Repository<DoctorOPDMaster, long>().InsertAsync(doctorOPDMaster);
                    _unitOfWork.Commit();
                }
              
            }

            return city.Id;
        }

        public async Task Delete(long Id)
        {

            var data = await _unitOfWork.Repository<StaffMaster, long>().SingleAsync(u => u.Id == Id);
            data.Delflg = true;
            data.Delby = _unitOfWork.Session.UserId;
            data.Deldatetime = DateTime.Now;
            await _unitOfWork.Repository<StaffMaster, long>().UpdateAsync(data);
            _unitOfWork.Commit();
        }

        public async Task Update(StaffCreateDto entity)
        {
            var data = Mapper.Map<StaffMaster>(entity);
            data.Updatedby = _unitOfWork.Session.UserId;
            data.Updateddatetime = DateTime.Now;
            data.Syncflg = 1;
            await _unitOfWork.Repository<StaffMaster, long>().UpdateAsync(data);
            _unitOfWork.Commit();
        }

        public List<StaffDto> GetAllDoctors()
        {
            var staffMaster = _unitOfWork.Repository<StaffMaster, long>().GetAll().Where(u => !u.Delflg && u.StaffType == "Doctor")
                     .Select(u => new StaffDto
                     {
                         Id = u.Id,
                         DepartmentName = u.Dept.DepartmentName,
                         StaffMobile = u.MobileNo.HasValue ? u.MobileNo.Value.ToString() : "",
                         StaffName = u.FirstName + " " + u.MiddleName + " " + u.LastName + "",
                         StaffType = u.StaffType,
                         Recstatus = u.Recstatus
                     }).ToList();
            if (staffMaster != null)
                return staffMaster;
            else return null;
        }
    }
}

