// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace HIMS.Service.StaffServices.Dto
{
	public class StaffDto
	{
		public long Id { get; set; }

		public string StaffType { get; set; }

		public string StaffName { get; set; }

		public string DepartmentName { get; set; }

		public string StaffMobile { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}

	public class StaffCreateDto
	{
		public long? Id { get; set; }

		[Display(Name = "Staff Type")]
		public string StaffType { get; set; }

		[Display(Name = "Staff Code")]
		public string StaffCode { get; set; }

		public string Prefix { get; set; }

		[Display(Name = "First Name")]
		public string FirstName { get; set; }

		[Display(Name = "Middle Name")]
		public string MiddleName { get; set; }

		[Display(Name = "Last Name")]
		public string LastName { get; set; }

		[Display(Name = "Birth Date")]
		public DateTime? BirthDate { get; set; }

		[Display(Name = "Age Year")]
		public long? AgeYears { get; set; }

		[Display(Name = "Age Month")]
		public long? AgeMonths { get; set; }

		[Display(Name = "Age Days")]
		public long? AgeDays { get; set; }

		[Display(Name = "Gender")]
		public string Gender { get; set; }

		[Display(Name = "Blood Group")]
		public string BloodGroup { get; set; }

		[Display(Name = "Department")]
		public long? DeptId { get; set; }

		[Display(Name = "Designation")]
		public long? DesigId { get; set; }

		[Display(Name = "Speciality")]
		public long? SpeclId { get; set; }

		[Display(Name = "Registration Number")]
		public string RegNo { get; set; }

		[Display(Name = "Address 1")]
		public string Address1 { get; set; }

		[Display(Name = "Address 2")]
		public string Address2 { get; set; }

		[Display(Name = "Address 3")]
		public string Address3 { get; set; }

		[Display(Name = "Pin Code")]
		public long? PinCode { get; set; }

		[Display(Name = "City")]
		public long? CityId { get; set; }

		[Display(Name = "State")]
		public long? StateId { get; set; }

		[Display(Name = "Country")]
		public long? CountryId { get; set; }

		[Display(Name = "STD Code")]
		public long? StdCode { get; set; }

		[Display(Name = "Phone No")]
		public long? PhoneNo { get; set; }

		[Display(Name = "Mobile No")]
		public long? MobileNo { get; set; }

		[Display(Name = "Contact No")]
		public string ContactNo { get; set; }

		[Display(Name = "Email")]
		public string EmailId { get; set; }

		[Display(Name = "Website")]
		public string Website { get; set; }

		[Display(Name = "PAN No")]
		public string PanNo { get; set; }

		[Display(Name = "GST No")]
		public string GstNo { get; set; }

		[Display(Name = "Profie Photo")]
		public byte[] ProfPhoto { get; set; }

		[Display(Name = "Staff Sign")]
		public byte[] StaffSign { get; set; }

		public HttpPostedFileBase ProfilePhotoFile { get; set;}

		public HttpPostedFileBase StaffSignFile { get; set;}

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}
}

