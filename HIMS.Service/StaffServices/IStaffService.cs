// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.StaffServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.StaffServices
{
	public interface IStaffMasterService
	{
		Tuple<List<StaffDto>, int, int> GetAll(DataTableAjaxPostModel<StaffDto> modelDT);
		
		Task<StaffCreateDto> GetById(long id);

		Task<long> CreateAsync(StaffCreateDto entity);

		Task Update(StaffCreateDto entity);

		Task Delete(long id);

        List<StaffDto> GetAllDoctors();
	}
}

