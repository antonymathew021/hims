﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Model;
using HIMS.Service.SpecialityMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.SpecialityMasterServices
{
	public interface ISpecialityMasterService
	{
		Task<Tuple<List<SpecialityMasterDto>, int, int>> GetAll(DataTableAjaxPostModel<SpecialityMasterDto> modelDT);

		Task<SpecialityMasterDto> GetById(long id);

		Task<long> CreateAsync(SpecialityMasterDto entity);

		Task Update(SpecialityMasterDto entity);

		Task Delete(long id);
	}
}
