﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.SpecialityMasterServices.Dto
{
	public class SpecialityMasterDto
	{
		public long? Id { get; set; }
		
		[Display(Name ="Speciality Code")]
		public string SpecialityCode { get; set; }
		
		[Display(Name ="Speciality Name")]
		public string SpecialityName { get; set; }
		
		[Display(Name ="Status")]
		public string Recstatus { get; set; }
	}
}
