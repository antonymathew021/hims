// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.InvestigationServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.InvestigationServices
{
	public interface IInvestigationMasterService
	{
		Task<Tuple<List<InvestigationDto>, int, int>> GetAll(DataTableAjaxPostModel<InvestigationDto> modelDT);


		Task<InvestigationDto> GetById(long id);

		Task<long> CreateAsync(InvestigationDto entity);

		Task Update(InvestigationDto entity);

		Task Delete(long id);
	}
}

