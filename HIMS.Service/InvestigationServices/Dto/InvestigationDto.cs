// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.InvestigationServices.Dto
{
	public class InvestigationDto
	{
		public long? Id { get; set; }
		  
		[Display(Name ="Status")]
		public string Recstatus { get; set; }
	}
}

