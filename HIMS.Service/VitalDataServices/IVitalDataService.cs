// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.VitalDataServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.VitalDataServices
{
	public interface IVitalDataMasterService
	{
		Task<Tuple<List<VitalDataDto>, int, int>> GetAll(DataTableAjaxPostModel<VitalDataDto> modelDT);


		Task<VitalDataDto> GetById(long id);

		Task<long> CreateAsync(VitalDataDto entity);

		Task Update(VitalDataDto entity);

		Task Delete(long id);
	}
}

