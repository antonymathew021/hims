// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.VitalDataServices.Dto
{
	public class VitalDataDto
	{
		public long? Id { get; set; }

		public string VitalCode { get; set; }

		public string VitalName { get; set; }

		public string VitalUnit { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}
}

