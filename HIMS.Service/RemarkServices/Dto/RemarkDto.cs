﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.RemarkServices.Dto
{
	public class RemarkDto
	{
		public long Id { get; set; }
		 
		[Display(Name ="Remark Code")]
		public string RemarkCode { get; set; }
		
		[Display(Name ="Remark For")]
		public string RemarkFor { get; set; }
		
		[Display(Name ="English Remark")]
		[DataType(DataType.MultilineText)]
		public string RemarkName { get; set; }
		
		[Display(Name ="Hindi Remark")]
		[DataType(DataType.MultilineText)]
		public string Hinremarks { get; set; }
		
		[Display(Name ="Gujarati Remark")]
		[DataType(DataType.MultilineText)]
		public string Gujremarks { get; set; }
		
		[Display(Name ="Marathi Remark")]
		[DataType(DataType.MultilineText)]
		public string Marremarks { get; set; }
		
		[Display(Name ="Aribik Remark")]
		[DataType(DataType.MultilineText)]
		public string Arbremarks { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}
}
