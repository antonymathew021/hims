﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Model;
using HIMS.Service.RemarkServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.RemarkServices
{
	public interface IRemarkService
	{
		Task<Tuple<List<RemarkDto>, int, int>> GetAll(DataTableAjaxPostModel<RemarkDto> modelDT);

		List<RemarkDto> GetAll();

		Task<RemarkDto> GetById(long id);

		Task<long> CreateAsync(RemarkDto entity);

		Task Update(RemarkDto entity);

		Task Delete(long id);
	}
}
