﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.Model;
using HIMS.Service.RemarkServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.RemarkServices
{
	public class RemarkService : IRemarkService
	{
		
		private readonly IUnitOfWork _unitOfWork;

		public RemarkService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<Tuple<List<RemarkDto>, int, int>> GetAll(DataTableAjaxPostModel<RemarkDto> modelDT)
		{
			try
			{
				int totalrows, totalrowsafterfiltering = 0;
				var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
				var length = modelDT.length;
				var start = modelDT.start;
				string sortColumnName = string.Empty;
				string sortDirection = string.Empty;
				string searchValue = string.Empty;
				if (searchTerm != null)
				{
					searchValue = searchTerm.ToString();
				}
				if (modelDT.order != null)
				{
					sortColumnName = modelDT.columns[modelDT.order[0].column].data;
					sortDirection = modelDT.order[0].dir.ToLower();
				}

				var filterData = _unitOfWork.Repository<RemarkMaster, long>().GetAll().Where(u => !u.Delflg).OrderBy(sortColumnName + " " + sortDirection)
					 .Skip(start).Take(length).ToList();

				var referanceMaster = Mapper.Map<List<RemarkDto>>(filterData);
				totalrows = _unitOfWork.Repository<RemarkMaster, long>().GetAll().Count();
				return new Tuple<List<RemarkDto>, int, int>(referanceMaster, totalrows, totalrowsafterfiltering);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public List<RemarkDto> GetAll()
		{
			try
			{  
				var filterData = _unitOfWork.Repository<RemarkMaster, long>().GetAll().Where(u => !u.Delflg && u.Recstatus=="Active").ToList(); 
				var referanceMaster = Mapper.Map<List<RemarkDto>>(filterData); 
				return referanceMaster; 
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		public async Task<RemarkDto> GetById(long Id)
		{
			try
			{
				var filteredData = await _unitOfWork.Repository<RemarkMaster, long>().SingleAsync(u => u.Id == Id);
				if (filteredData != null)
					return Mapper.Map<RemarkMaster, RemarkDto>(filteredData);
				else return null;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public async Task<long> CreateAsync(RemarkDto entity)
		{
			var data = Mapper.Map<RemarkMaster>(entity);
			data.Addedby = _unitOfWork.Session.UserId;
			data.Addeddate = DateTime.Now;
			data.Syncflg = 0;
			var city = await _unitOfWork.Repository<RemarkMaster, long>().InsertAsync(data);
			_unitOfWork.Commit();
			return city.Id;
		}

		public async Task Delete(long Id)
		{

			var data = await _unitOfWork.Repository<RemarkMaster, long>().SingleAsync(u => u.Id == Id);
			data.Delflg = true;
			data.Delby = _unitOfWork.Session.UserId;
			data.Deldatetime = DateTime.Now; 
			await _unitOfWork.Repository<RemarkMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}

		public async Task Update(RemarkDto entity)
		{
			var data = Mapper.Map<RemarkMaster>(entity);

			data.Updatedby = _unitOfWork.Session.UserId;
			data.Updateddatetime = DateTime.Now;
			data.Syncflg = 1;
			await _unitOfWork.Repository<RemarkMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}
	}
}
