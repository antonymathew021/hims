// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.MedicineServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.MedicineServices
{
	public interface IMedicineMasterService
	{
		Task<Tuple<List<MedicineDto>, int, int>> GetAll(DataTableAjaxPostModel<MedicineDto> modelDT);


		Task<MedicineDto> GetById(long id);

		Task<long> CreateAsync(MedicineDto entity);

		Task Update(MedicineDto entity);

		Task Delete(long id);
	}
}

