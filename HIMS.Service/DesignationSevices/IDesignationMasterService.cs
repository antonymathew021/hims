﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.DesignationSevices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.DesignationSevices
{
	public interface IDesignationMasterService
	{
		Task<Tuple<List<DesignationMasterDto>, int, int>> GetAll(DataTableAjaxPostModel<DesignationMasterDto> modelDT);

		Task<DesignationMasterDto> GetById(long id);

		Task<long> CreateAsync(DesignationMasterDto entity);

		Task Update(DesignationMasterDto entity);

		Task Delete(long id);
	}
}
