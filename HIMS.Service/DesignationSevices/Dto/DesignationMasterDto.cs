﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.DesignationSevices.Dto
{
	public class DesignationMasterDto
	{
		public long? Id { get; set; }
		
		[Display(Name ="Designation Name")]
		public string DesignationCode { get; set; }
		
		[Display(Name ="Designation Name")]
		public string DesignationName { get; set; }

		[Display(Name ="Status")]
		public string Recstatus { get; set; }
	}
}
