﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.DepartmentServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.DepartmentServices
{
	public interface IDepartmentMasterService
	{
		Task<Tuple<List<DepartmentMasterDto>, int, int>> GetAll(DataTableAjaxPostModel<DepartmentMasterDto> modelDT);


		Task<DepartmentMasterDto> GetById(long id);

		Task<long> CreateAsync(DepartmentMasterDto entity);

		Task Update(DepartmentMasterDto entity);

		Task Delete(long id);
	}
}
