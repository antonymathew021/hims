﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.DepartmentServices.Dto
{
	public class DepartmentMasterDto
	{
		public long? Id { get; set; }
		
		[Display(Name ="Department Code")]
		public string DepartmentCode { get; set; }
		
		[Display(Name ="Department Name")]
		public string DepartmentName { get; set; }

		[Display(Name ="Status")]
		public string Recstatus { get; set; }
	}
}
