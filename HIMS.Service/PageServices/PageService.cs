﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.PageServices.Dto;

namespace HIMS.Service.PageServices
{
	public class PageService : IPageService
	{
		private readonly IUnitOfWork _unitOfWork;

		public PageService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public List<PageDto> GetPageListAsync()
		{
			List<PageDto> pageDtos = new List<PageDto>();
			var pageList = _unitOfWork.Repository<PagesMaster, long>().GetAllList();

			var mainModules = pageList.Select(u => u.ModuleName).Distinct().ToList();

			foreach (var module in mainModules)
			{
				PageDto newPageDto = new PageDto();
				newPageDto.ModuleName = module;
				newPageDto.SubPageNames = new List<SubPageName>();
				var subModuleItem = pageList.Where(u => u.ModuleName.Equals(module)).OrderBy(u => u.Menu).Select(u => u.Menu).Distinct().ToList();
				foreach (var subModule in subModuleItem)
				{
					SubPageName subPageName = new SubPageName();
					subPageName.Menu = subModule;
					subPageName.MainMenuNames = new List<MainMenuName>();
					newPageDto.SubPageNames.Add(subPageName);
					var mainMenuItem = pageList.Where(u => u.ModuleName.Equals(module) && u.Menu.Equals(subModule)).Distinct().OrderBy(u => u.RightsName).ToList();
					foreach (var mainMenu in mainMenuItem)
					{
						MainMenuName mainMenuName = new MainMenuName();
						mainMenuName.RightsName = mainMenu.RightsName;
						mainMenuName.Url = mainMenu.Url;
						subPageName.MainMenuNames.Add(mainMenuName);
					}
				}
				pageDtos.Add(newPageDto);
			}
			return pageDtos;
		}
	}
}
