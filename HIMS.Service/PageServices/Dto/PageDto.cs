﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.PageServices.Dto
{
	public class PageDto
	{

		public string ModuleName { get; set; }

		public string Icon { get; set; } 

		public bool HasPermission { get; set; }

		public List<SubPageName> SubPageNames{get;set;}
	}

	public class SubPageName
	{
		public string Menu { get; set; }

		public string Icon { get; set; }

		public bool HasPermission { get; set; }

		public List<MainMenuName> MainMenuNames{get;set;}
	}

	public class MainMenuName
	{
		public string RightsName { get; set; }

		public string Url { get; set; }

		public bool HasPermission { get; set; }
	}
}
