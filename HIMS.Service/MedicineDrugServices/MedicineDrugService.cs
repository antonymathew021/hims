// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.MedicineDrugServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.MedicineDrugServices
{
	public class MedicineDrugMasterService: IMedicineDrugMasterService
	{
		private readonly IUnitOfWork _unitOfWork;

		public MedicineDrugMasterService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<Tuple<List<MedicineDrugDto>, int, int>> GetAll(DataTableAjaxPostModel<MedicineDrugDto> modelDT)
		{
			try
			{
				int totalrows, totalrowsafterfiltering = 0;
				var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
				var length = modelDT.length;
				var start = modelDT.start;
				string sortColumnName = string.Empty;
				string sortDirection = string.Empty;
				string searchValue = string.Empty;
				if (searchTerm != null)
				{
					searchValue = searchTerm.ToString();
				}
				if (modelDT.order != null)
				{
					sortColumnName = modelDT.columns[modelDT.order[0].column].data;
					sortDirection = modelDT.order[0].dir.ToLower();
				}

				var filterData = _unitOfWork.Repository<MedicineDrugMaster, long>().GetAll().Where(u => !u.Delflg).OrderBy(sortColumnName + " " + sortDirection)
					 .Skip(start).Take(length).ToList();

				var medicinedrugMaster = Mapper.Map<List<MedicineDrugDto>>(filterData);
				totalrows = _unitOfWork.Repository<MedicineDrugMaster, long>().GetAll().Count();
				return new Tuple<List<MedicineDrugDto>, int, int>(medicinedrugMaster, totalrows, totalrowsafterfiltering);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<MedicineDrugDto> GetById(long Id)
		{
			try
			{
				var filteredData = await _unitOfWork.Repository<MedicineDrugMaster, long>().SingleAsync(u => u.Id == Id);
				if (filteredData != null)
					return Mapper.Map<MedicineDrugMaster, MedicineDrugDto>(filteredData);
				else return null;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public async Task<long> CreateAsync(MedicineDrugDto entity)
		{
			var data = Mapper.Map<MedicineDrugMaster>(entity);
			data.Addedby = _unitOfWork.Session.UserId;
			data.Addeddate = DateTime.Now;
			data.Syncflg = 0;
			var city = await _unitOfWork.Repository<MedicineDrugMaster, long>().InsertAsync(data);
			_unitOfWork.Commit();
			return city.Id;
		}

		public async Task Delete(long Id)
		{

			var data = await _unitOfWork.Repository<MedicineDrugMaster, long>().SingleAsync(u => u.Id == Id);
			data.Delflg = true;
			data.Delby = _unitOfWork.Session.UserId;
			data.Deldatetime = DateTime.Now; 
			await _unitOfWork.Repository<MedicineDrugMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}

		public async Task Update(MedicineDrugDto entity)
		{
			var data = Mapper.Map<MedicineDrugMaster>(entity);

			data.Updatedby = _unitOfWork.Session.UserId;
			data.Updateddatetime = DateTime.Now;
			data.Syncflg = 1;
			await _unitOfWork.Repository<MedicineDrugMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}
	}
}

