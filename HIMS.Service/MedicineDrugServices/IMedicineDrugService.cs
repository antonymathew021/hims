// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.MedicineDrugServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.MedicineDrugServices
{
	public interface IMedicineDrugMasterService
	{
		Task<Tuple<List<MedicineDrugDto>, int, int>> GetAll(DataTableAjaxPostModel<MedicineDrugDto> modelDT);


		Task<MedicineDrugDto> GetById(long id);

		Task<long> CreateAsync(MedicineDrugDto entity);

		Task Update(MedicineDrugDto entity);

		Task Delete(long id);
	}
}

