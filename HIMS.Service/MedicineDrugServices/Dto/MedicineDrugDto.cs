// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.MedicineDrugServices.Dto
{
	public class MedicineDrugDto
	{
		public long? Id { get; set; }
		
		[Display(Name ="Drug Name")]
		public string DrugName { get; set; } 
		
		[Display(Name ="Drug Description")]
		[DataType(DataType.MultilineText)]
		public string DrugDesc { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}
}

