﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.CountryMasterServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HIMS.Service.CountryMasterServices
{
    public interface ICountryMasterService
    {
        Tuple<List<CountryMasterDto>, int, int> GetAll(DataTableAjaxPostModel<CountryMasterDto> modelDT);

        Task<CountryMasterDto> GetById(long id);

        Task<long> CreateAsync(CountryMasterDto entity);

        Task Update(CountryMasterDto entity);

        Task Delete(long id);

        Task<List<CountryMasterDto>> GetList();
    }
}
