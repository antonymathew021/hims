﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 

using System;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.CountryMasterServices.Dto
{
	public class CountryMasterDto
	{
		public long Id {get;set; }
		
		[Display(Name ="Country Code")]
		public string Countrycode { get; set; }

		[Display(Name = "Country Name")]
		public string Countryname { get; set; }

		[Display(Name ="Status")]
		public string Recstatus { get; set; }

	}
}
