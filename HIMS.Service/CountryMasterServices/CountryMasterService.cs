﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.CountryMasterServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.CountryMasterServices
{
    public class CountryMasterService : ICountryMasterService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CountryMasterService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Tuple<List<CountryMasterDto>, int, int> GetAll(DataTableAjaxPostModel<CountryMasterDto> modelDT)
        {
            try
            {
                int totalrows, totalrowsafterfiltering = 0;
                var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
                var length = modelDT.length;
                var start = modelDT.start;
                string sortColumnName = string.Empty;
                string sortDirection = string.Empty;
                string searchValue = string.Empty;
                if (searchTerm != null)
                {
                    searchValue = searchTerm.ToString().ToLower();
                }
                if (modelDT.order != null)
                {
                    sortColumnName = modelDT.columns[modelDT.order[0].column].data;
                    sortDirection = modelDT.order[0].dir.ToLower();
                }

                var filterData = _unitOfWork.Repository<CountryMaster, long>().GetAll()
                     .Where(x => (x.Countrycode.ToLower().Contains(searchValue)
                             || x.Countryname.ToLower().Contains(searchValue))
                             && x.Delflg == false)
                     .OrderBy(sortColumnName + " " + sortDirection)
                     .Skip(start)
                     .Take(length)
                     .ToList();

                var countryMasterDto = Mapper.Map<List<CountryMasterDto>>(filterData);
                totalrowsafterfiltering = filterData.Count();
                totalrows = _unitOfWork.Repository<CountryMaster, long>().GetAll().Where(x => x.Delflg == false).Count();
                return new Tuple<List<CountryMasterDto>, int, int>(countryMasterDto, totalrows, totalrowsafterfiltering);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<CountryMasterDto> GetById(long Id)
        {
            try
            {
                var filteredData = await _unitOfWork.Repository<CountryMaster, long>().SingleAsync(u => u.Id == Id);
                if (filteredData != null)
                    return Mapper.Map<CountryMaster, CountryMasterDto>(filteredData);
                else return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<long> CreateAsync(CountryMasterDto entity)
        {
            var data = Mapper.Map<CountryMaster>(entity);
            data.Addedby = _unitOfWork.Session.UserId;
            data.Addeddate = DateTime.Now;
            data.Syncflg = 0;
            data.Delflg = false;
            var city = await _unitOfWork.Repository<CountryMaster, long>().InsertAsync(data);
            _unitOfWork.Commit();
            return city.Id;
        }

        public async Task Delete(long Id)
        {
            var data = await _unitOfWork.Repository<CountryMaster, long>().SingleAsync(u => u.Id == Id);
            data.Delflg = true;
            data.Delby = _unitOfWork.Session.UserId;
			data.Deldatetime = DateTime.Now; 
            await _unitOfWork.Repository<CountryMaster, long>().UpdateAsync(data);
            _unitOfWork.Commit();
        }

        public async Task Update(CountryMasterDto entity)
        {
            var data = Mapper.Map<CountryMaster>(entity);

            data.Updatedby = _unitOfWork.Session.UserId;
            data.Updateddatetime = DateTime.Now;
            data.Syncflg = 1;
            await _unitOfWork.Repository<CountryMaster, long>().UpdateAsync(data);
            _unitOfWork.Commit();
        }

        public async Task<List<CountryMasterDto>> GetList()
        {
            var filterData = await _unitOfWork.Repository<CountryMaster, long>().GetAllListAsync(x => x.Delflg == false && x.Recstatus == "Active");
            var countryMasterDtoList = Mapper.Map<List<CountryMasterDto>>(filterData);
            return countryMasterDtoList;
        }
    }
}
