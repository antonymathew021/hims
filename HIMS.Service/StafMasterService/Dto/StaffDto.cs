﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.StafMasterService.Dto
{
	public class StaffDto
	{
		public long? Id { get; set; }

		public string stafftype { get; set; }

		public string staffcode { get; set; }

		public string prefix { get; set; }

		public string firstname { get; set; }

		public string middlename { get; set; }

		public string lastname { get; set; }

		public DateTime? birthdate { get; set; }

		public long? ageyears { get; set; }

		public long? agemonths { get; set; }

		public long? agedays { get; set; }

		public string gender { get; set; }

		public string bloodgroup { get; set; }

		public string deptid { get; set; }

		public string desigid { get; set; }

		public string speclid { get; set; }

		public string regno { get; set; }

		public string address1 { get; set; }

		public string address2 { get; set; }

		public string address3 { get; set; }

		public long? pincode { get; set; }

		public string cityid { get; set; }

		public string stateid { get; set; }

		public string countryid { get; set; }

		public long? stdcode { get; set; }

		public long? phoneno { get; set; }

		public long? mobileno { get; set; }

		public string contactno { get; set; }

		public string emailid { get; set; }

		public string website { get; set; }

		public string panno { get; set; }

		public string gstno { get; set; }

		public byte[] profphoto { get; set; }

		public byte[] staffsign { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}
}
