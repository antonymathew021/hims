// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.DoctorOPDServices.Dto
{
    public class DoctorOPDDto
    {
        public long? Id { get; set; }

        [Display(Name ="Doctor Name")]
        public long DoctorId { get; set; }

        [Display(Name = "Day")]
        public string WeekType { get; set; }

        [Display(Name = "Morning From")]
        public DateTime? MorningTimeFrom { get; set; }

        [Display(Name = "Morning To")]
        public DateTime? MorningTimeTo { get; set; }

        public bool IsAvailableMorning { get; set; }

        [Display(Name = "AfterNoon From")]
        public DateTime? AfterNoonTimeFrom { get; set; }

        [Display(Name = "AfterNoon To")]
        public DateTime? AfterNoonTimeTo { get; set; }

        public bool IsAvailableAfterNoon { get; set; }

        [Display(Name = "Evening From")]
        public DateTime? EveningTimeFrom { get; set; }

        [Display(Name = "Evening To")]
        public DateTime? EveningTimeTo { get; set; }

        public bool IsAvailableEvening { get; set; }

        [Display(Name = "Status")]
        public string Recstatus { get; set; }
        
    }
}

