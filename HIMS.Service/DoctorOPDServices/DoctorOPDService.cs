// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.DoctorOPDServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.DoctorOPDServices
{
    public class DoctorOPDMasterService : IDoctorOPDMasterService
    {
        private readonly IUnitOfWork _unitOfWork;

        public DoctorOPDMasterService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Tuple<List<DoctorOPDDto>, int, int>> GetAll(DataTableAjaxPostModel<DoctorOPDDto> modelDT)
        {
            try
            {
                int totalrows, totalrowsafterfiltering = 0;
                var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
                var length = modelDT.length;
                var start = modelDT.start;
                string sortColumnName = string.Empty;
                string sortDirection = string.Empty;
                string searchValue = string.Empty;
                if (searchTerm != null)
                {
                    searchValue = searchTerm.ToString();
                }
                if (modelDT.order != null)
                {
                    sortColumnName = modelDT.columns[modelDT.order[0].column].data;
                    sortDirection = modelDT.order[0].dir.ToLower();
                }

                var filterData = _unitOfWork.Repository<DoctorOPDMaster, long>().GetAll().Where(u => !u.Delflg).OrderBy(sortColumnName + " " + sortDirection)
                     .Skip(start).Take(length).ToList();

                var doctoropdMaster = Mapper.Map<List<DoctorOPDDto>>(filterData);
                totalrows = _unitOfWork.Repository<DoctorOPDMaster, long>().GetAll().Count();
                return new Tuple<List<DoctorOPDDto>, int, int>(doctoropdMaster, totalrows, totalrowsafterfiltering);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<DoctorOPDDto> GetById(long Id)
        {
            try
            {
                var filteredData = await _unitOfWork.Repository<DoctorOPDMaster, long>().SingleAsync(u => u.Id == Id);
                if (filteredData != null)
                    return Mapper.Map<DoctorOPDMaster, DoctorOPDDto>(filteredData);
                else return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<long> CreateAsync(DoctorOPDDto entity)
        {
            var data = Mapper.Map<DoctorOPDMaster>(entity);
            data.Addedby = _unitOfWork.Session.UserId;
            data.Addeddate = DateTime.Now;
            data.Syncflg = 0;
            var city = await _unitOfWork.Repository<DoctorOPDMaster, long>().InsertAsync(data);
            _unitOfWork.Commit();
            return city.Id;
        }

        public async Task Delete(long Id)
        {

            var data = await _unitOfWork.Repository<DoctorOPDMaster, long>().SingleAsync(u => u.Id == Id);
            data.Delflg = true;
            data.Delby = _unitOfWork.Session.UserId;
            data.Deldatetime = DateTime.Now;
            await _unitOfWork.Repository<DoctorOPDMaster, long>().UpdateAsync(data);
            _unitOfWork.Commit();
        }

        public async Task<List<DoctorOPDDto>> Update(DoctorOPDDto entity)
        {
            try
            {
                var doctorOPDMaster = await _unitOfWork.Repository<DoctorOPDMaster, long>().FirstOrDefaultAsync(x => !x.Delflg && x.DoctorId == entity.DoctorId && x.WeekType == entity.WeekType);
                if (doctorOPDMaster != null)
                {
                    var data = Mapper.Map<DoctorOPDMaster>(entity);
                    data.Id = doctorOPDMaster.Id;
                    data.HospitalMasterId = doctorOPDMaster.HospitalMasterId;
                    data.Addedby = doctorOPDMaster.Addedby;
                    data.Addeddate = doctorOPDMaster.Addeddate;
                    data.Updatedby = _unitOfWork.Session.UserId;
                    data.Updateddatetime = DateTime.Now;
                    data.Syncflg = 1;
                    await _unitOfWork.Repository<DoctorOPDMaster, long>().UpdateAsync(data);
                    _unitOfWork.Commit();
                }
                var doctorList = await _unitOfWork.Repository<DoctorOPDMaster, long>().GetAllListAsync(x => x.DoctorId == entity.DoctorId && !x.Delflg);
                var result = Mapper.Map<List<DoctorOPDDto>>(doctorList);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<List<DoctorOPDDto>> GetAllByDoctor(long DoctorId)
        {
            try
            {
                var doctorList = await _unitOfWork.Repository<DoctorOPDMaster, long>().GetAllListAsync(x => x.DoctorId == DoctorId && !x.Delflg);
                var result = Mapper.Map<List<DoctorOPDDto>>(doctorList);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

