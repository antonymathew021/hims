// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.DoctorOPDServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.DoctorOPDServices
{
	public interface IDoctorOPDMasterService
	{
		Task<Tuple<List<DoctorOPDDto>, int, int>> GetAll(DataTableAjaxPostModel<DoctorOPDDto> modelDT);


		Task<DoctorOPDDto> GetById(long id);

		Task<long> CreateAsync(DoctorOPDDto entity);

		Task<List<DoctorOPDDto>> Update(DoctorOPDDto entity);

		Task Delete(long id);

        Task<List<DoctorOPDDto>> GetAllByDoctor(long DoctorId);
    }
}

