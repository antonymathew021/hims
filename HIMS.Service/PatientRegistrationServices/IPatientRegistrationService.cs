// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.PatientRegistrationServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.PatientRegistrationServices
{
	public interface IPatientRegistrationTransactionService
	{
		Task<Tuple<List<PatientRegistrationDto>, int, int>> GetAll(DataTableAjaxPostModel<PatientRegistrationDto> modelDT);


		Task<PatientRegistrationDto> GetById(long id);

		Task<long> CreateAsync(PatientRegistrationDto entity);

		Task Update(PatientRegistrationDto entity);

		Task Delete(long id);

        Task<List<PatientRegistrationDto>> GetList();

    }
}

