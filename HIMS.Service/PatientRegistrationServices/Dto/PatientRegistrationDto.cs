// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace HIMS.Service.PatientRegistrationServices.Dto
{
	public class PatientRegistrationDto
	{
		public long? Id { get; set; }
		
		public string Uhid { get; set; }

		public long? Uhid1 { get; set; }

        [Display(Name = "Registration Date")]
        [Required]
        public DateTime? Regdate { get; set; }

        [Display(Name = "Time")]
        [Required]
        public DateTime? Regtime { get; set; }

		public byte[] Patimage { get; set; }

		public string Prefix { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Birth Date")]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "Age Years")]
        public long? AgeYears { get; set; }

        [Display(Name = "Age Months")]
        public long? AgeMonths { get; set; }

        [Display(Name = "Age Days")]
        public long? AgeDays { get; set; }

		public string Gender { get; set; }

        [Display(Name = "Blood Group")]
        public string Bloodgroup { get; set; }

        [Display(Name = "Marital Status")]
        public string MaritalStatus { get; set; }

        [Display(Name = "Referred By")]
        public string Refbyid { get; set; }

		public string Occupation { get; set; }

		public string Nationality { get; set; }

		public string TieupId { get; set; }

        [Display(Name = "Address 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        [Display(Name = "Address 3")]
        public string Address3 { get; set; }

		public long? Pincode { get; set; }

        [Display(Name = "City")]
        public string Cityid { get; set; }

        [Display(Name = "State")]
        public string Stateid { get; set; }

        [Display(Name = "Country")]
        public string Countryid { get; set; }

        [Display(Name = "Mobile No")]
        public long? Mobileno { get; set; }

        [Display(Name = "Contact No")]
        public string Contact { get; set; }

		public string Website { get; set; }

        [Display(Name = "Email-Id")]
        public string Emailid { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Allergy Details")]
        public string Allergydtls { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Special Notes")]
        public string Spnotes { get; set; }

        [Display(Name ="Id No (ID Card No)")]
		public string IdCardNo { get; set; }

		public string PatType { get; set; } 

		[Display(Name ="Status")]
		public string Recstatus { get; set; }

        public HttpPostedFileBase ProfilePhoto { get; set; }

        public string Addedby { get; set; }
        private DateTime? createdDate;
        [DataType(DataType.DateTime)]
        public DateTime? Addeddate
        {
            get { return createdDate ?? DateTime.UtcNow; }
            set { createdDate = value; }
        }
    }
}

