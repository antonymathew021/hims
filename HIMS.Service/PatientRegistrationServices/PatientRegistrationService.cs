// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.PatientRegistrationServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.PatientRegistrationServices
{
	public class PatientRegistrationTransactionService: IPatientRegistrationTransactionService
	{
		private readonly IUnitOfWork _unitOfWork;

		public PatientRegistrationTransactionService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<Tuple<List<PatientRegistrationDto>, int, int>> GetAll(DataTableAjaxPostModel<PatientRegistrationDto> modelDT)
		{
			try
			{
				int totalrows, totalrowsafterfiltering = 0;
				var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
				var length = modelDT.length;
				var start = modelDT.start;
				string sortColumnName = string.Empty;
				string sortDirection = string.Empty;
				string searchValue = string.Empty;
				if (searchTerm != null)
				{
					searchValue = searchTerm.ToString();
				}
				if (modelDT.order != null)
				{
					sortColumnName = modelDT.columns[modelDT.order[0].column].data;
					sortDirection = modelDT.order[0].dir.ToLower();
				}

				var filterData = _unitOfWork.Repository<PatientRegistrationTransaction, long>().GetAll().Where(u => !u.Delflg).OrderBy(sortColumnName + " " + sortDirection)
					 .Skip(start).Take(length).ToList();

				var patientregistrationTransaction = Mapper.Map<List<PatientRegistrationDto>>(filterData);
				totalrows = _unitOfWork.Repository<PatientRegistrationTransaction, long>().GetAll().Count();
				return new Tuple<List<PatientRegistrationDto>, int, int>(patientregistrationTransaction, totalrows, totalrowsafterfiltering);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<PatientRegistrationDto> GetById(long Id)
		{
			try
			{
				var filteredData = await _unitOfWork.Repository<PatientRegistrationTransaction, long>().SingleAsync(u => u.Id == Id);
				if (filteredData != null)
					return Mapper.Map<PatientRegistrationTransaction, PatientRegistrationDto>(filteredData);
				else return null;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public async Task<long> CreateAsync(PatientRegistrationDto entity)
		{
			var data = Mapper.Map<PatientRegistrationTransaction>(entity);
			data.Addedby = _unitOfWork.Session.UserId;
			data.Addeddate = DateTime.Now;
			data.Syncflg = 0;
			var city = await _unitOfWork.Repository<PatientRegistrationTransaction, long>().InsertAsync(data);
			_unitOfWork.Commit();
			return city.Id;
		}

		public async Task Delete(long Id)
		{

			var data = await _unitOfWork.Repository<PatientRegistrationTransaction, long>().SingleAsync(u => u.Id == Id);
			data.Delflg = true;
			data.Delby = _unitOfWork.Session.UserId;
			data.Deldatetime = DateTime.Now;
			await _unitOfWork.Repository<PatientRegistrationTransaction, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}

		public async Task Update(PatientRegistrationDto entity)
		{
			var data = Mapper.Map<PatientRegistrationTransaction>(entity);

			data.Updatedby = _unitOfWork.Session.UserId;
			data.Updateddatetime = DateTime.Now;
			data.Syncflg = 1;
			await _unitOfWork.Repository<PatientRegistrationTransaction, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}

        public async Task<List<PatientRegistrationDto>> GetList()
        {
            var filterData = await _unitOfWork.Repository<PatientRegistrationTransaction, long>().GetAllListAsync(x => x.Delflg == false && x.Recstatus == "Active");
            var countryMasterDtoList = Mapper.Map<List<PatientRegistrationDto>>(filterData);
            return countryMasterDtoList;
        }
    }
}

