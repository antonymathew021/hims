// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.MedicineCategoryServices.Dto
{
	public class MedicineCategoryDto
	{
		public long? Id { get; set; }
		 
		public string CategoryName { get; set; }

		public string CategoryDesc { get; set; }

		[Display(Name ="Status")]
		public string Recstatus { get; set; }
	}
}

