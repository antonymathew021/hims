// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.MedicineCategoryServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.MedicineCategoryServices
{
	public interface IMedicineCategoryMasterService
	{
		Task<Tuple<List<MedicineCategoryDto>, int, int>> GetAll(DataTableAjaxPostModel<MedicineCategoryDto> modelDT);


		Task<MedicineCategoryDto> GetById(long id);

		Task<long> CreateAsync(MedicineCategoryDto entity);

		Task Update(MedicineCategoryDto entity);

		Task Delete(long id);
	}
}

