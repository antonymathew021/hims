﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.CityMasterServices.Dto;
using HIMS.Service.CountryMasterServices.Dto;
using HIMS.Service.DepartmentServices.Dto;
using HIMS.Service.DesignationSevices.Dto;
using HIMS.Service.SpecialityMasterServices.Dto;
using HIMS.Service.StateMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.Constant
{
	public interface IConstantService
	{
		List<string> GetDropDownListFromTable(string coloumName);

		List<CountryMasterDto> GetCountryList();

		List<StateMasterDto> GetStateList();

		List<CityMasterDto> GetCityList();
		
		List<DesignationMasterDto> GetDesignationList();

		List<DepartmentMasterDto> GetDepartmentList();

		List<SpecialityMasterDto> GetSpecialityList();
	}
}
