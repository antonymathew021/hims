﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Core.Extensions;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.CityMasterServices.Dto;
using HIMS.Service.CountryMasterServices.Dto;
using HIMS.Service.DepartmentServices.Dto;
using HIMS.Service.DesignationSevices.Dto;
using HIMS.Service.SpecialityMasterServices.Dto;
using HIMS.Service.StateMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.Constant
{
	public class ConstantService : IConstantService
	{
		private readonly IUnitOfWork _unitOfWork;
		public ConstantService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public List<CityMasterDto> GetCityList()
		{
			var filterData = _unitOfWork.Repository<CityMaster, long>().GetAll()
					 .Where(x => !x.Delflg && x.Recstatus == "Active").ToList();
			var cityMasterDto = Mapper.Map<List<CityMasterDto>>(filterData);
			return cityMasterDto;
		}

		public List<CountryMasterDto> GetCountryList()
		{
			var filterData = _unitOfWork.Repository<CountryMaster, long>().GetAll()
					 .Where(x => !x.Delflg && x.Recstatus == "Active").ToList();
			var cityMasterDto = Mapper.Map<List<CountryMasterDto>>(filterData);
			return cityMasterDto;
		}

		public List<DepartmentMasterDto> GetDepartmentList()
		{
				var filterData = _unitOfWork.Repository<DepartmentMaster, long>().GetAll()
					 .Where(x => !x.Delflg && x.Recstatus == "Active").ToList();
			var cityMasterDto = Mapper.Map<List<DepartmentMasterDto>>(filterData);
			return cityMasterDto;
		}

		public List<DesignationMasterDto> GetDesignationList()
		{
			var filterData = _unitOfWork.Repository<DesignationMaster, long>().GetAll()
					 .Where(x => !x.Delflg && x.Recstatus == "Active").ToList();
			var cityMasterDto = Mapper.Map<List<DesignationMasterDto>>(filterData);
			return cityMasterDto;
		}

		public List<string> GetDropDownListFromTable(string coloumName)
		{
			List<string> data = DelimSeprator.DelimToList(_unitOfWork.Repository().GetDelimData(coloumName));
			return data;
		}

		public List<SpecialityMasterDto> GetSpecialityList()
		{
			var filterData = _unitOfWork.Repository<SpecialityMaster, long>().GetAll()
					 .Where(x => !x.Delflg && x.Recstatus == "Active").ToList();
			var cityMasterDto = Mapper.Map<List<SpecialityMasterDto>>(filterData);
			return cityMasterDto;
		}

		public List<StateMasterDto> GetStateList()
		{
			var filterData = _unitOfWork.Repository<StateMaster, long>().GetAll()
					 .Where(x => !x.Delflg && x.Recstatus == "Active").ToList();
			var cityMasterDto = Mapper.Map<List<StateMasterDto>>(filterData);
			return cityMasterDto;
		}
	}
}
