﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.Model;
using HIMS.Service.StateMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.StateMasterServices
{
    public class StateMasterService : IStateMasterService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StateMasterService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Tuple<List<StateMasterDto>, int, int> GetAll(DataTableAjaxPostModel<StateMasterDto> modelDT)
        {
            try
            {
                int totalrows, totalrowsafterfiltering = 0;
                var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
                var length = modelDT.length;
                var start = modelDT.start;
                string sortColumnName = string.Empty;
                string sortDirection = string.Empty;
                string searchValue = string.Empty;
                if (searchTerm != null)
                {
                    searchValue = searchTerm.ToString().ToLower();
                }
                if (modelDT.order != null)
                {
                    sortColumnName = modelDT.columns[modelDT.order[0].column].data;
                    sortDirection = modelDT.order[0].dir.ToLower();
                }

                var filterData = _unitOfWork.Repository<StateMaster, long>().GetAll()
					.Where(x => (x.Statecode.ToLower().Contains(searchValue)
                             || x.Statename.ToLower().Contains(searchValue)
							 || x.CountryMaster.Countryname.ToLower().Contains(searchValue))  
                             && x.Delflg == false)
					 .OrderBy(sortColumnName + " " + sortDirection)
                     .Skip(start).Take(length).ToList();

                var stateMasterDto = Mapper.Map<List<StateMasterDto>>(filterData);
                totalrows = _unitOfWork.Repository<StateMaster, long>().GetAll().Count();
                return new Tuple<List<StateMasterDto>, int, int>(stateMasterDto, totalrows, totalrowsafterfiltering);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<StateMasterDto> GetById(long Id)
        {
            try
            {
                var filteredData = await _unitOfWork.Repository<StateMaster, long>().SingleAsync(u => u.Id == Id);
                if (filteredData != null)
                    return Mapper.Map<StateMaster, StateMasterDto>(filteredData);
                else return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<long> CreateAsync(StateMasterDto entity)
        {
            var data = Mapper.Map<StateMaster>(entity);
            data.Addedby = _unitOfWork.Session.UserId;
            data.Addeddate = DateTime.Now;
            data.Syncflg = 0;
            var city = await _unitOfWork.Repository<StateMaster, long>().InsertAsync(data);
            _unitOfWork.Commit();
            return city.Id;
        }

        public async Task Delete(long Id)
        {

            var data = await _unitOfWork.Repository<StateMaster, long>().SingleAsync(u => u.Id == Id);
            data.Delflg = true;
            data.Delby = _unitOfWork.Session.UserId;
			data.Deldatetime = DateTime.Now; 
            await _unitOfWork.Repository<StateMaster, long>().UpdateAsync(data);
            _unitOfWork.Commit();
        }

        public async Task Update(StateMasterDto entity)
        {
            var data = Mapper.Map<StateMaster>(entity);

            data.Updatedby = _unitOfWork.Session.UserId;
            data.Updateddatetime = DateTime.Now;
            data.Syncflg = 1;
            await _unitOfWork.Repository<StateMaster, long>().UpdateAsync(data);
            _unitOfWork.Commit();
        }

        public async Task<List<StateMasterDto>> GetList()
        {
            var filterData = await _unitOfWork.Repository<StateMaster, long>().GetAllListAsync(x => x.Delflg == false && x.Recstatus == "Active");
            var stateMasterDtoList = Mapper.Map<List<StateMasterDto>>(filterData);
            return stateMasterDtoList;
        }
    }
}
