﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.StateMasterServices.Dto
{
	public class StateMasterDto
	{
		public long Id { get; set; }

		[Display(Name = "State Code")]
		public string Statecode { get; set; }

		[Display(Name = "State")]
		public string Statename { get; set; }

		[Display(Name = "Country")]
		public long CountryMasterId { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }

		public CountryMaster CountryMaster { get; set; }
	}
}
