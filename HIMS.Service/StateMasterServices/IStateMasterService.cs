﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Model;
using HIMS.Service.StateMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.StateMasterServices
{
    public interface IStateMasterService
    {
        Tuple<List<StateMasterDto>, int, int> GetAll(DataTableAjaxPostModel<StateMasterDto> modelDT);

        Task<StateMasterDto> GetById(long id);

        Task<long> CreateAsync(StateMasterDto entity);

        Task Update(StateMasterDto entity);

        Task Delete(long id);

        Task<List<StateMasterDto>> GetList();
    }
}
