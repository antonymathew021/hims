﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.User
{
	public interface IUserService
	{  
        bool ValidateUserById(long userId);
		
		long? GetHospitalByUserId(long Id);
	}
}
