﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Data.Authorization;
using HIMS.Data.UnitOfWork;
using System.Linq;

namespace HIMS.Service.User
{
	public class UserService : IUserService
	{

		public readonly ApplicationUserManager _userManager;
		public readonly IUnitOfWork _unitOfWork;

		public UserService(ApplicationUserManager userManager, IUnitOfWork unitOfWork)
		{
			_userManager = userManager;
			_unitOfWork = unitOfWork;
		}

		public long? GetHospitalByUserId(long Id)
		{
			var query = _userManager.Users.Where(model => model.Id == Id
			&& model.isActiveUser == true && model.isDeleted == false).SingleOrDefault();
			if (query != null)
				return query.HospitalMasterId;
			else
				return 0;

		}

		public bool ValidateUserById(long userId)
		{
			var query = _userManager.Users.Where(model => model.Id == userId && model.isActiveUser == true && model.isDeleted == false).ToList();
			if (query != null && query.Count() > 0)
			{
				return true;
			}
			return false;
		}
	}
}
