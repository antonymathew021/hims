﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.Model
{
	public class ListFilterViewModel
	{
		public string SortBy { get; set; }

		public string Direction { get; set; }

		public virtual int Page { get; set; } = 0;

		public virtual int ItemsCount { get; set; } = 10;
	}
}
