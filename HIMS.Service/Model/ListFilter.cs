﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.Model
{
	public class ListFilter
	{
		public int Take { get; set; }

		public int Skip { get; set; }

		public string SortBy { get; set; }

		public string Direction { get; set; }
	}
}
