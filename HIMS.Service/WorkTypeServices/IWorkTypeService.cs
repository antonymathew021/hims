// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.WorkTypeServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.WorkTypeServices
{
	public interface IWorkTypeMasterService
	{
		Task<Tuple<List<WorkTypeDto>, int, int>> GetAll(DataTableAjaxPostModel<WorkTypeDto> modelDT);


		Task<WorkTypeDto> GetById(long id);

		Task<long> CreateAsync(WorkTypeDto entity);

		Task Update(WorkTypeDto entity);

		Task Delete(long id);
	}
}

