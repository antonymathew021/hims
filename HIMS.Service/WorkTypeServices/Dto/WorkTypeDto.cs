// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.WorkTypeServices.Dto
{
	public class WorkTypeDto
	{
		public long? Id { get; set; }

		[Display(Name ="WorkType Code")]
		public string WorktypeCode { get; set; }
		
		[Display(Name ="WorkType Name")]
		public string WorktypeName { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}
}

