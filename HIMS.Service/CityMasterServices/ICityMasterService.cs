﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.CityMasterServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HIMS.Service.CityMasterServices
{
	public interface ICityMasterService
	{
		Task<Tuple<List<CityMasterDto>, int, int>> GetAll(DataTableAjaxPostModel<CityMasterDto> modelDT);

		Task<CityMasterDto> GetById(long id);

		Task<long> CreateAsync(CityMasterDto entity);

		Task Update(CityMasterDto entity);

		Task Delete(long id);

        Task<List<CityMasterDto>> GetList();
    }
}
