﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.CityMasterServices.Dto;
using HIMS.Service.Model;
using System.Linq.Dynamic;

namespace HIMS.Service.CityMasterServices
{
	public class CityMasterService : ICityMasterService
	{
		private readonly IUnitOfWork _unitOfWork;

		public CityMasterService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}
		public async Task<Tuple<List<CityMasterDto>, int, int>> GetAll(DataTableAjaxPostModel<CityMasterDto> modelDT)
		{
			try
			{
				int totalrows, totalrowsafterfiltering = 0;
				var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
				var length = modelDT.length;
				var start = modelDT.start;
				string sortColumnName = string.Empty;
				string sortDirection = string.Empty;
				string searchValue = string.Empty;
				if (searchTerm != null)
				{
					searchValue = searchTerm.ToString().ToLower();
				}
				if (modelDT.order != null)
				{
					sortColumnName = modelDT.columns[modelDT.order[0].column].data;
					sortDirection = modelDT.order[0].dir.ToLower();
				}

				var filterData = _unitOfWork.Repository<CityMaster, long>().GetAll()
					 .Where(x => (x.Citycode.ToLower().Contains(searchValue)
							 || x.Cityname.ToLower().Contains(searchValue)
							 || x.CountryMaster.Countryname.ToLower().Contains(searchValue)
							 || x.StateMaster.Statename.ToLower().Contains(searchValue))
							 && x.Delflg == false)
					 .OrderBy(sortColumnName + " " + sortDirection)
					 .Skip(start).Take(length).ToList();

				var cityMasterDto = Mapper.Map<List<CityMasterDto>>(filterData);
				totalrows = _unitOfWork.Repository<CityMaster, long>().GetAll().Count();
				return new Tuple<List<CityMasterDto>, int, int>(cityMasterDto, totalrows, totalrowsafterfiltering);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		public async Task<CityMasterDto> GetById(long Id)
		{
			try
			{
				var filteredData = await _unitOfWork.Repository<CityMaster, long>().SingleAsync(u => u.Id == Id);
				if (filteredData != null)
					return Mapper.Map<CityMaster, CityMasterDto>(filteredData);
				else return null;
			}
			catch (Exception)
			{

				throw;
			}
		}
		public async Task<long> CreateAsync(CityMasterDto entity)
		{
			var data = Mapper.Map<CityMaster>(entity);
			data.Addedby = _unitOfWork.Session.UserId;
			data.Addeddate = DateTime.Now;
			data.Syncflg = 0;
			var city = await _unitOfWork.Repository<CityMaster, long>().InsertAsync(data);
			_unitOfWork.Commit();
			return city.Id;
		}
		public async Task Delete(long Id)
		{

			var data = await _unitOfWork.Repository<CityMaster, long>().SingleAsync(u => u.Id == Id);
			data.Delflg = true;
			data.Delby = _unitOfWork.Session.UserId;
			data.Deldatetime = DateTime.Now; 
			await _unitOfWork.Repository<CityMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}
		public async Task Update(CityMasterDto entity)
		{
			var data = Mapper.Map<CityMaster>(entity);

			data.Updatedby = _unitOfWork.Session.UserId;
			data.Updateddatetime = DateTime.Now;
			data.Syncflg = 1;
			await _unitOfWork.Repository<CityMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}

		public async Task<List<CityMasterDto>> GetList()
		{
			var filterData = await _unitOfWork.Repository<CityMaster, long>().GetAllListAsync(x => x.Delflg == false && x.Recstatus == "Active");
			var cityMasterDtoList = Mapper.Map<List<CityMasterDto>>(filterData);
			return cityMasterDtoList;
		}
	}
}
