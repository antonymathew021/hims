﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using HIMS.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.CityMasterServices.Dto
{
	public class CityMasterDto
	{
		public long Id { get; set; }

        [Display(Name = "City Code")]
        public string Citycode { get; set; }

        [Display(Name = "City")]
        public string Cityname { get; set; }

        [Display(Name = "State")]
        public long StateMasterId { get; set; }

        [Display(Name = "Country")]
        public long CountryMasterId { get; set; }

        [Display(Name = "Status")]
        public string Recstatus { get; set; }

        public CountryMaster CountryMaster { get; set; }
        public StateMaster StateMaster { get; set; }
    }
}
