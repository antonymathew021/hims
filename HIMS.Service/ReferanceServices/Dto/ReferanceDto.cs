// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using HIMS.Data.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web;


namespace HIMS.Service.ReferanceServices.Dto
{
    public class ReferanceDto
    {
        public long? Id { get; set; }

        [Display(Name = "Reference Type")]
        public string Reftype { get; set; }

        [Display(Name ="Reference Code")]
        [Required]
        public string Refcode { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string Refname { get; set; }

        [Display(Name = "Birth Date")]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "Age Years")]
        public long? AgeYears { get; set; }

        [Display(Name = "Age Months")]
        public long? AgeMonths { get; set; }

        [Display(Name = "Age Days")]
        public long? AgeDays { get; set; }

        public string Gender { get; set; }

        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }

        [Display(Name = "Speciality")]
        public long? Speclid { get; set; }

        public string Regno { get; set; }

        [Display(Name = "Address 1")]
        [DataType(DataType.MultilineText)]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        [DataType(DataType.MultilineText)]
        public string Address2 { get; set; }

        [Display(Name = "Address 3")]
        [DataType(DataType.MultilineText)]
        public string Address3 { get; set; }

        public long? Pincode { get; set; }

        [Display(Name = "City")]
        public long? CityId { get; set; }

        [Display(Name = "State")]
        public long? StateId { get; set; }

        [Display(Name = "State Code")]
        public long? Statecode { get; set; }

        [Display(Name = "Country")]
        public long? CountryId { get; set; }

        [Display(Name = "Mobile No")]
        public long? Mobileno { get; set; }

        [Display(Name = "Contact No")]
        public string ContactNo { get; set; }

        [Display(Name = "Email-Id")]
        public string Emailid { get; set; }

        public string Website { get; set; }

        [Display(Name = "Pan No")]
        public string PanNo { get; set; }

        [Display(Name = "GSTIN")]
        public string GstNo { get; set; }

        public byte[] RefSign { get; set; }

        [Display(Name = "Status")]
        public string Recstatus { get; set; }

        public HttpPostedFileBase RefSignFile { get; set; }

        public string Addedby { get; set; }
        private DateTime? createdDate;
        [DataType(DataType.DateTime)]
        public DateTime? Addeddate
        {
            get { return createdDate ?? DateTime.UtcNow; }
            set { createdDate = value; }
        }

        public CountryMaster Country { get; set; }
        public StateMaster State { get; set; }
        public CityMaster City { get; set; }
        public SpecialityMaster Specl { get; set; }
    }

    public class ReferanceDtoForList
    {
        public long? Id { get; set; }

        [Display(Name = "Reference Type")]
        public string Reftype { get; set; }

        [Display(Name = "Reference Code")]
        [Required]
        public string Refcode { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string Refname { get; set; }

        [Display(Name = "Birth Date")]
        public DateTime? BirthDate { get; set; }

        
        [Display(Name = "Speciality")]
        public long? Speclid { get; set; }

        [Display(Name = "Status")]
        public string Recstatus { get; set; }
        public SpecialityMaster Specl { get; set; }
    }
}

