// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.ReferanceServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.ReferanceServices
{
	public interface IReferanceMasterService
	{
		Task<Tuple<List<ReferanceDtoForList>, int, int>> GetAll(DataTableAjaxPostModel<ReferanceDtoForList> modelDT);

		Task<ReferanceDto> GetById(long id);

		Task<long> CreateAsync(ReferanceDto entity);

		Task Update(ReferanceDto entity);

		Task Delete(long id);

        List<ReferanceDto> GetList();
    }
}

