// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Data.UnitOfWork;
using HIMS.Service.TieupServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;

namespace HIMS.Service.TieupServices
{
	public class TieupMasterService : ITieupMasterService
	{
		private readonly IUnitOfWork _unitOfWork;

		public TieupMasterService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<Tuple<List<TieUpDataDto>, int, int>> GetAll(DataTableAjaxPostModel<TieupDto> modelDT)
		{
			try
			{
				int totalrows, totalrowsafterfiltering = 0;
				var searchTerm = (modelDT.search != null) ? modelDT.search.value : null;
				var length = modelDT.length;
				var start = modelDT.start;
				string sortColumnName = string.Empty;
				string sortDirection = string.Empty;
				string searchValue = string.Empty;
				if (searchTerm != null)
				{
					searchValue = searchTerm.ToString();
				}
				if (modelDT.order != null)
				{

					sortColumnName = modelDT.columns[modelDT.order[0].column].data;
					if (sortColumnName.ToLower().Equals("companyname"))
					{
						sortColumnName = "Compname";
					}
					else if (sortColumnName.ToLower().Equals("contactperson"))
					{
						sortColumnName = "Contpername";
					}
					else if (sortColumnName.ToLower().Equals("mobile"))
					{
						sortColumnName = "Mobileno";

					}
					else if (sortColumnName.ToLower().Equals("status"))
					{
						sortColumnName = "Recstatus";
					}
					sortDirection = modelDT.order[0].dir.ToLower();
				}

				var filterData = _unitOfWork.Repository<TieupMaster, long>().GetAll().Where(u => !u.Delflg).OrderBy(sortColumnName + " " + sortDirection)
					 .Skip(start).Take(length).Select(u =>
						new TieUpDataDto
						{
							Id = u.Id,
							CompanyName = u.Compname,
							ContactPerson = u.Contpername,
							Mobile = u.Mobileno.HasValue ? u.Mobileno.Value.ToString() : "-",
							Status = u.Recstatus
						}
					).ToList();

				//var tieupMaster = Mapper.Map<List<TieupDto>>(filterData);
				totalrows = _unitOfWork.Repository<TieupMaster, long>().GetAll().Count();
				return new Tuple<List<TieUpDataDto>, int, int>(filterData, totalrows, totalrowsafterfiltering);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<TieupDto> GetById(long Id)
		{
			try
			{
				var filteredData = await _unitOfWork.Repository<TieupMaster, long>().SingleAsync(u => u.Id == Id);
				if (filteredData != null)
					return Mapper.Map<TieupMaster, TieupDto>(filteredData);
				else return null;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public async Task<long> CreateAsync(TieupDto entity)
		{
			var data = Mapper.Map<TieupMaster>(entity);
			data.Addedby = _unitOfWork.Session.UserId;
			data.Addeddate = DateTime.Now;
			data.Syncflg = 0;
			var city = await _unitOfWork.Repository<TieupMaster, long>().InsertAsync(data);
			_unitOfWork.Commit();
			return city.Id;
		}

		public async Task Delete(long Id)
		{

			var data = await _unitOfWork.Repository<TieupMaster, long>().SingleAsync(u => u.Id == Id);
			data.Delflg = true;
			data.Delby = _unitOfWork.Session.UserId;
			data.Deldatetime = DateTime.Now; 
			await _unitOfWork.Repository<TieupMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}

		public async Task Update(TieupDto entity)
		{
			var data = Mapper.Map<TieupMaster>(entity);

			data.Updatedby = _unitOfWork.Session.UserId;
			data.Updateddatetime = DateTime.Now;
			data.Syncflg = 1;
			await _unitOfWork.Repository<TieupMaster, long>().UpdateAsync(data);
			_unitOfWork.Commit();
		}
	}
}

