// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.TieupServices.Dto
{
    public class TieupDto
    {
        public long? Id { get; set; }

        [Display(Name = "Tieup Code")]
        public string Tieupcode { get; set; }

        [Display(Name = "Company Name")]
        public string Compname { get; set; }

        [Display(Name = "Address1")]
        public string Address1 { get; set; }

        [Display(Name = "Address2")]
        public string Address2 { get; set; }

        [Display(Name = "Address3")]
        public string Address3 { get; set; }

        [Display(Name = "PinCode")]
        public long? Pincode { get; set; }

        [Display(Name = "City")]
        public long? Cityid { get; set; }

        [Display(Name = "State")]
        public long? Stateid { get; set; }

        [Display(Name = "Country")]
        public long? Countryid { get; set; }

        [Display(Name = "MobileNo")]
        public long? Mobileno { get; set; }

        [Display(Name = "Contact")]
        public string Contact { get; set; }

        [Display(Name = "Email Id")]
        public string Emailid { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "Pan No")]
        public string Panno { get; set; }

        [Display(Name = "Gst No")]
        public string Gstno { get; set; }

        [Display(Name = "Contact Person")]
        public string Contpername { get; set; }

        [Display(Name = "Designation")]
        public string Designation { get; set; }

        [Display(Name = "Reg No")]
        public string Regno { get; set; }

        [Display(Name = "Status")]
        public string Recstatus { get; set; }
    }

    public class TieUpDataDto
    {
        public long Id { get; set; }

        public string CompanyName { get; set; }

        public string ContactPerson { get; set; }

		public string Mobile { get; set; }

		public string Status { get; set; }
	}
}

