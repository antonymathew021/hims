// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.TieupServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.TieupServices
{
	public interface ITieupMasterService
	{
		Task<Tuple<List<TieUpDataDto>, int, int>> GetAll(DataTableAjaxPostModel<TieupDto> modelDT);


		Task<TieupDto> GetById(long id);

		Task<long> CreateAsync(TieupDto entity);

		Task Update(TieupDto entity);

		Task Delete(long id);
	}
}

