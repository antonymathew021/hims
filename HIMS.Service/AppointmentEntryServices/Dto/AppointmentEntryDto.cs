// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using System;
using System.ComponentModel.DataAnnotations;

namespace HIMS.Service.AppointmentEntryServices.Dto
{
	public class AppointmentEntryDto
	{
		public long? Id { get; set; }

		public long PatId { get; set; }

		public DateTime? AppDate { get; set; }

		public DateTime? AppTime { get; set; }

		public long DepartmentId { get; set; }

		public long DoctorId { get; set; }

		public string Appointmentfor { get; set; }

		public long? AppNo { get; set; }

		public string Appstatus { get; set; }

		public DateTime? StTime { get; set; }

		public DateTime? EndTime { get; set; }

		[Display(Name = "Status")]
		public string Recstatus { get; set; }
	}
}

