// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.AppointmentEntryServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HIMS.Service.AppointmentEntryServices
{
	public interface IAppointmentEntryTransactionService
	{
		Task<Tuple<List<AppointmentEntryDto>, int, int>> GetAll(DataTableAjaxPostModel<AppointmentEntryDto> modelDT);  

		Task<AppointmentEntryDto> GetById(long id);

		Task<long> CreateAsync(AppointmentEntryDto entity);

		Task Update(AppointmentEntryDto entity);

		Task Delete(long id);
	}
}

