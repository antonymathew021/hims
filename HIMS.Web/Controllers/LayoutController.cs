﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.PageServices;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class LayoutController : BaseController
	{
		private readonly IPageService pageService;
		public LayoutController(IPageService _pageService)
		{
			pageService = _pageService;
		}
		[ChildActionOnly]
		public PartialViewResult TopBar()
		{
			return PartialView("_TopBar");

		}

		[ChildActionOnly]
		public PartialViewResult SideBarNav(string activeMenu = "")
		{
			var item =  pageService.GetPageListAsync();
			return PartialView("_LeftSideBar", item);
		}
	}
}