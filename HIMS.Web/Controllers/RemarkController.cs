﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.Model;
using HIMS.Service.RemarkServices;
using HIMS.Service.RemarkServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class RemarkController : BaseController
    {
       private readonly IRemarkService _referanceService;
		private readonly IConstantService _constantService;

		public RemarkController(IRemarkService referanceService, IConstantService constantService)
		{
			_referanceService = referanceService;
			_constantService = constantService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetRemarkForDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("remarksfor");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<RemarkDto> modelDT)
		{
			try
			{
				var designationList = await _referanceService.GetAll(modelDT);
				return Json(new { data = designationList.Item1, draw = Request["draw"], recordsTotal = designationList.Item2, recordsFiltered = designationList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(RemarkDto remarkDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (remarkDto.Id == 0)
					{
						await _referanceService.CreateAsync(remarkDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _referanceService.Update(remarkDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _referanceService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _referanceService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}