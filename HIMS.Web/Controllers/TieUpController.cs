﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.Model;
using HIMS.Service.TieupServices;
using HIMS.Service.TieupServices.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class TieUpController : BaseController
	{
		private readonly ITieupMasterService _tieupMasterService;
		private readonly IConstantService _constantService;

		public TieUpController(ITieupMasterService referanceService, IConstantService constantService)
		{
			_tieupMasterService = referanceService;
			_constantService = constantService;
		}

		#region Bind Dropdowns

		public void BindAllDropdowns()
		{
			BindStatus();
			BindReferenceType();
			BindBloodGroup();
			BindCity();
			BindCountry();
			BindGender();
			BindSpeciality();
			BindState();
		}
		private void BindStatus()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			if (data != null)
			{
				IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
				ViewBag.Status = _reftype;
			}
		}

		private void BindReferenceType()
		{
			var data = _constantService.GetDropDownListFromTable("reftype");
			if (data != null)
			{
				IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
				ViewBag.ReferenceType = _reftype;
			}
		}

		private void BindBloodGroup()
		{
			var data = _constantService.GetDropDownListFromTable("bloodgroup");
			if (data != null)
			{ 
				IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
 				ViewBag.BloodGroup = _reftype;
			}
		}

		private void BindCountry()
		{
			var data = _constantService.GetCountryList();
			if (data != null)
			{
				var _reftype = new SelectList(data.ToList(), "Id", "Countryname");
				ViewBag.Country = _reftype;
			}
		}

		private void BindState()
		{
			var data = _constantService.GetStateList();
			if (data != null)
			{
				var _reftype = new SelectList(data.ToList(), "Id", "Statename");
				ViewBag.State = _reftype;
			}
		}

		private void BindCity()
		{
			var data = _constantService.GetCityList();
			if (data != null)
			{
				var _reftype = new SelectList(data.ToList(), "Id", "Cityname");
				ViewBag.City = _reftype;
			}
		}

		private void BindGender()
		{
			var data = _constantService.GetDropDownListFromTable("gender");
			if (data != null)
			{
				IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
				ViewBag.Gender = _reftype;
			}
		}

		private void BindSpeciality()
		{
			var data = _constantService.GetSpecialityList();
			if (data != null)
			{
				var _reftype = new SelectList(data.ToList(), "Id", "SpecialityName");
				ViewBag.Speciality = _reftype;
			}
		}
		#endregion

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<TieupDto> modelDT)
		{
			try
			{
				var designationList = await _tieupMasterService.GetAll(modelDT);
				return Json(new { data = designationList.Item1, draw = Request["draw"], recordsTotal = designationList.Item2, recordsFiltered = designationList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpGet]
		public ActionResult Create()
		{
			BindAllDropdowns();
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Create(TieupDto referanceDto)
		{
			try
			{
				if (ModelState.IsValid)
				{ 
					await _tieupMasterService.CreateAsync(referanceDto);
					return RedirectToAction("Index");
				}
				else
				{
					BindAllDropdowns();
					return View("Create", referanceDto);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpGet]
		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				BindAllDropdowns();
				var _country = await _tieupMasterService.GetById(Id);
				return View(_country);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpPost]
		public async Task<ActionResult> Edit(long Id, TieupDto referanceDto)
		{
			try
			{
				if (ModelState.IsValid)
				{ 
					await _tieupMasterService.Update(referanceDto);
					return RedirectToAction("Index");
				}
				else
				{
					BindAllDropdowns();
					return View("Edit", referanceDto);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _tieupMasterService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}