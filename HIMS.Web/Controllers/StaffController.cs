﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================

using HIMS.Service.Constant;
using HIMS.Service.Model;
using HIMS.Service.StaffServices;
using HIMS.Service.StaffServices.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class StaffController : BaseController
    {
        private readonly IStaffMasterService _sstaffService;
        private readonly IConstantService _constantService;

        public StaffController(IStaffMasterService staffService, IConstantService constantService)
        {
            _sstaffService = staffService;
            _constantService = constantService;
        }
        public void BindAllDropdowns()
        {
            BindStatus();
            BindSaffType();
            BindPrefix();
            BindGender();
            BindBloodGroup();
            BindCountry();
            BindState();
            BindCity();
            BindDesignation();
            BindDepartment();
            BindSpeciality();
        }
        // GET: Staff
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(DataTableAjaxPostModel<StaffDto> modelDT)
        {
            try
            {
                var specialityList = _sstaffService.GetAll(modelDT);
                return Json(new { data = specialityList.Item1, draw = Request["draw"], recordsTotal = specialityList.Item2, recordsFiltered = specialityList.Item3 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            BindAllDropdowns();
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(StaffCreateDto staffMasterDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!staffMasterDto.Id.HasValue)
                    {
                        if (staffMasterDto.StaffSignFile != null)
                        {
                            using (Stream inputStream = staffMasterDto.StaffSignFile?.InputStream)
                            {
                                var memoryStream = inputStream as MemoryStream;
                                if (memoryStream == null)
                                {
                                    memoryStream = new MemoryStream();
                                    inputStream.CopyTo(memoryStream);
                                }
                                staffMasterDto.StaffSign = memoryStream.ToArray();
                            }
                        }
                        if (staffMasterDto.ProfilePhotoFile != null)
                        {
                            using (Stream inputStream = staffMasterDto.ProfilePhotoFile?.InputStream)
                            {
                                var memoryStream = inputStream as MemoryStream;
                                if (memoryStream == null)
                                {
                                    memoryStream = new MemoryStream();
                                    inputStream.CopyTo(memoryStream);
                                }
                                staffMasterDto.ProfPhoto = memoryStream.ToArray();
                            }
                        }
                        await _sstaffService.CreateAsync(staffMasterDto);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        if (staffMasterDto.StaffSignFile != null)
                        {
                            using (Stream inputStream = staffMasterDto.StaffSignFile?.InputStream)
                            {
                                var memoryStream = inputStream as MemoryStream;
                                if (memoryStream == null)
                                {
                                    memoryStream = new MemoryStream();
                                    inputStream.CopyTo(memoryStream);
                                }
                                staffMasterDto.StaffSign = memoryStream.ToArray();
                            }
                        }
                        if (staffMasterDto.ProfilePhotoFile != null)
                        {
                            using (Stream inputStream = staffMasterDto.ProfilePhotoFile?.InputStream)
                            {
                                var memoryStream = inputStream as MemoryStream;
                                if (memoryStream == null)
                                {
                                    memoryStream = new MemoryStream();
                                    inputStream.CopyTo(memoryStream);
                                }
                                staffMasterDto.ProfPhoto = memoryStream.ToArray();
                            }
                        }
                        await _sstaffService.Update(staffMasterDto);
                        return RedirectToAction("Index");
                    }
                }
                else
                    return View(staffMasterDto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit(long Id)
        {
            try
            {
                BindAllDropdowns();
                var _staff = await _sstaffService.GetById(Id);
                return View("Edit", _staff);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> Delete(long Id)
        {
            try
            {
                await _sstaffService.Delete(Id);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        private void BindStatus()
        {
            var data = _constantService.GetDropDownListFromTable("recstatus");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.Status = _reftype;
            }
        }

        private void BindBloodGroup()
        {
            var data = _constantService.GetDropDownListFromTable("bloodgroup");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.BloodGroup = _reftype;
            }
        }

        private void BindCountry()
        {
            var data = _constantService.GetCountryList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "Countryname");
                ViewBag.Country = _reftype;
            }
        }

        private void BindState()
        {
            var data = _constantService.GetStateList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "Statename");
                ViewBag.State = _reftype;
            }
        }

        private void BindCity()
        {
            var data = _constantService.GetCityList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "Cityname");
                ViewBag.City = _reftype;
            }
        }

        private void BindGender()
        {
            var data = _constantService.GetDropDownListFromTable("gender");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.Gender = _reftype;
            }
        }

        private void BindSpeciality()
        {
            var data = _constantService.GetSpecialityList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "SpecialityName");
                ViewBag.Speciality = _reftype;
            }
        }

        public void BindSaffType()
        {
            var data = _constantService.GetDropDownListFromTable("stafftype");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.SaffType = _reftype;
            }
        }

        public void BindPrefix()
        {
            var data = _constantService.GetDropDownListFromTable("prefix");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.Prefix = _reftype;
            }
        }


        public void BindDesignation()
        {
            var data = _constantService.GetDesignationList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "DesignationName");
                ViewBag.Designation = _reftype;
            }
        }

        public void BindDepartment()
        {
            var data = _constantService.GetDepartmentList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "DepartmentName");
                ViewBag.Department = _reftype;
            }
        }
    }
}