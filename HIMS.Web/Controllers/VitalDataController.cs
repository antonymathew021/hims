﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.Model;
using HIMS.Service.VitalDataServices;
using HIMS.Service.VitalDataServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class VitalDataController : BaseController
    {
        private readonly IVitalDataMasterService _tieupService;
		private readonly IConstantService _constantService;

		public VitalDataController(IVitalDataMasterService tieupService, IConstantService constantService)
		{
			_tieupService = tieupService;
			_constantService = constantService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<VitalDataDto> modelDT)
		{
			try
			{
				var specialityList = await _tieupService.GetAll(modelDT);
				return Json(new { data = specialityList.Item1, draw = Request["draw"], recordsTotal = specialityList.Item2, recordsFiltered = specialityList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(VitalDataDto vitalDataDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (vitalDataDto.Id == 0)
					{
						await _tieupService.CreateAsync(vitalDataDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _tieupService.Update(vitalDataDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _tieupService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _tieupService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}