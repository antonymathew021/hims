﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.Model;
using HIMS.Service.ReferanceServices;
using HIMS.Service.ReferanceServices.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class ReferanceController : BaseController
	{
		private readonly IReferanceMasterService _referanceService;
		private readonly IConstantService _constantService;

		public ReferanceController(IReferanceMasterService referanceService, IConstantService constantService)
		{
			_referanceService = referanceService;
			_constantService = constantService;
		}

		#region Bind Dropdowns
		public void BindAllDropdowns()
		{
			BindStatus();
			BindReferenceType();
			BindBloodGroup();
			BindCity();
			BindCountry();
			BindGender();
			BindSpeciality();
			BindState();
		}
		private void BindStatus()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			if (data != null)
			{
				IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
				ViewBag.Status = _reftype;
			}
		}

		private void BindReferenceType()
		{
			var data = _constantService.GetDropDownListFromTable("reftype");
			if (data != null)
			{
				IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
				ViewBag.ReferenceType = _reftype;
			}
		}

		private void BindBloodGroup()
		{
			var data = _constantService.GetDropDownListFromTable("bloodgroup");
			if (data != null)
			{ 
				IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
 				ViewBag.BloodGroup = _reftype;
			}
		}

		private void BindCountry()
		{
			var data = _constantService.GetCountryList();
			if (data != null)
			{
				var _reftype = new SelectList(data.ToList(), "Id", "Countryname");
				ViewBag.Country = _reftype;
			}
		}

		private void BindState()
		{
			var data = _constantService.GetStateList();
			if (data != null)
			{
				var _reftype = new SelectList(data.ToList(), "Id", "Statename");
				ViewBag.State = _reftype;
			}
		}

		private void BindCity()
		{
			var data = _constantService.GetCityList();
			if (data != null)
			{
				var _reftype = new SelectList(data.ToList(), "Id", "Cityname");
				ViewBag.City = _reftype;
			}
		}

		private void BindGender()
		{
			var data = _constantService.GetDropDownListFromTable("gender");
			if (data != null)
			{
				IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
				ViewBag.Gender = _reftype;
			}
		}

		private void BindSpeciality()
		{
			var data = _constantService.GetSpecialityList();
			if (data != null)
			{
				var _reftype = new SelectList(data.ToList(), "Id", "SpecialityName");
				ViewBag.Speciality = _reftype;
			}
		}
		#endregion

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<ReferanceDtoForList> modelDT)
		{
			try
			{
				var designationList = await _referanceService.GetAll(modelDT);
				return Json(new { data = designationList.Item1, draw = Request["draw"], recordsTotal = designationList.Item2, recordsFiltered = designationList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpGet]
		public ActionResult Create()
		{
			BindAllDropdowns();
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Create(ReferanceDto referanceDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (referanceDto.RefSignFile != null)
					{
						using (Stream inputStream = referanceDto.RefSignFile.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							referanceDto.RefSign = memoryStream.ToArray();
						}
					}
					await _referanceService.CreateAsync(referanceDto);
					return RedirectToAction("Index");
				}
				else
				{
					BindAllDropdowns();
					return View("Create", referanceDto);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpGet]
		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				BindAllDropdowns();
				var _country = await _referanceService.GetById(Id);
				return View(_country);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpPost]
		public async Task<ActionResult> Edit(long Id, ReferanceDto referanceDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (referanceDto.RefSignFile != null)
					{
						using (Stream inputStream = referanceDto.RefSignFile.InputStream)
						{
							MemoryStream memoryStream = inputStream as MemoryStream;
							if (memoryStream == null)
							{
								memoryStream = new MemoryStream();
								inputStream.CopyTo(memoryStream);
							}
							referanceDto.RefSign = memoryStream.ToArray();
						}
					}
					await _referanceService.Update(referanceDto);
					return RedirectToAction("Index");
				}
				else
				{
					BindAllDropdowns();
					return View("Edit", referanceDto);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _referanceService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}