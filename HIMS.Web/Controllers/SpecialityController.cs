﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.Model;
using HIMS.Service.SpecialityMasterServices;
using HIMS.Service.SpecialityMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class SpecialityController : BaseController
	{
		private readonly ISpecialityMasterService _specialityMasterService;
		private readonly IConstantService _constantService;

		public SpecialityController(ISpecialityMasterService specialityMasterService, IConstantService constantService)
		{
			_specialityMasterService = specialityMasterService;
			_constantService = constantService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<SpecialityMasterDto> modelDT)
		{
			try
			{
				var specialityList = await _specialityMasterService.GetAll(modelDT);
				return Json(new { data = specialityList.Item1, draw = Request["draw"], recordsTotal = specialityList.Item2, recordsFiltered = specialityList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(SpecialityMasterDto countryMasterDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (countryMasterDto.Id == 0)
					{
						await _specialityMasterService.CreateAsync(countryMasterDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _specialityMasterService.Update(countryMasterDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _specialityMasterService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _specialityMasterService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}