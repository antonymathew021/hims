﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.MedicineCategoryServices;
using HIMS.Service.MedicineCategoryServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class MedicineCategoryController : BaseController
	{
		private readonly IMedicineCategoryMasterService _medicineCategoryService;
		private readonly IConstantService _constantService;

		public MedicineCategoryController(IMedicineCategoryMasterService medicineCategoryService, IConstantService constantService)
		{
			_medicineCategoryService = medicineCategoryService;
			_constantService = constantService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<MedicineCategoryDto> modelDT)
		{
			try
			{
				var designationList = await _medicineCategoryService.GetAll(modelDT);
				return Json(new { data = designationList.Item1, draw = Request["draw"], recordsTotal = designationList.Item2, recordsFiltered = designationList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(MedicineCategoryDto countryMasterDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (countryMasterDto.Id == 0)
					{
						await _medicineCategoryService.CreateAsync(countryMasterDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _medicineCategoryService.Update(countryMasterDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _medicineCategoryService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _medicineCategoryService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}