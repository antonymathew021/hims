﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.MedicineDrugServices;
using HIMS.Service.MedicineDrugServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class MedicineDrugController : BaseController
    {
        private readonly IMedicineDrugMasterService _medicineDrugService;
		private readonly IConstantService _constantService;

		public MedicineDrugController(IMedicineDrugMasterService medicineDrugService, IConstantService constantService)
		{
			_medicineDrugService = medicineDrugService;
			_constantService = constantService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<MedicineDrugDto> modelDT)
		{
			try
			{
				var designationList = await _medicineDrugService.GetAll(modelDT);
				return Json(new { data = designationList.Item1, draw = Request["draw"], recordsTotal = designationList.Item2, recordsFiltered = designationList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(MedicineDrugDto medicineDrugDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (medicineDrugDto.Id == 0)
					{
						await _medicineDrugService.CreateAsync(medicineDrugDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _medicineDrugService.Update(medicineDrugDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _medicineDrugService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _medicineDrugService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}