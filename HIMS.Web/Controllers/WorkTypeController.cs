﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.Model;
using HIMS.Service.WorkTypeServices;
using HIMS.Service.WorkTypeServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class WorkTypeController : BaseController
	{
		private readonly IWorkTypeMasterService _workTypeService;
		private readonly IConstantService _constantService;

		public WorkTypeController(IWorkTypeMasterService workTypeService, IConstantService constantService)
		{
			_workTypeService = workTypeService;
			_constantService = constantService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<WorkTypeDto> modelDT)
		{
			try
			{
				var specialityList = await _workTypeService.GetAll(modelDT);
				return Json(new { data = specialityList.Item1, draw = Request["draw"], recordsTotal = specialityList.Item2, recordsFiltered = specialityList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(WorkTypeDto vitalDataDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (vitalDataDto.Id == 0)
					{
						await _workTypeService.CreateAsync(vitalDataDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _workTypeService.Update(vitalDataDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _workTypeService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _workTypeService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}