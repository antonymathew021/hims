﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using HIMS.Service.Constant;
using HIMS.Service.DosageServices;
using HIMS.Service.DosageServices.Dto;
using HIMS.Service.Model;
using HIMS.Service.RemarkServices;

namespace HIMS.Web.Controllers
{
	public class DosageController : BaseController
	{
		private readonly IDosageMasterService _dosageService;
		private readonly IRemarkService _remarkService;
		private readonly IConstantService _constantService;

		public DosageController(IDosageMasterService dosageService, IRemarkService remarkService, IConstantService constantService)
		{
			_dosageService = dosageService;
			_constantService = constantService;
			_remarkService = remarkService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetRemarkDropDownList()
		{
			var data = _remarkService.GetAll();
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Dosage
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<DosageList> modelDT)
		{
			try
			{
				var designationList = await _dosageService.GetAll(modelDT);
				return Json(new { data = designationList.Item1, draw = Request["draw"], recordsTotal = designationList.Item2, recordsFiltered = designationList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(DosageDto dosageDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (dosageDto.Id == 0)
					{
						await _dosageService.CreateAsync(dosageDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _dosageService.Update(dosageDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _dosageService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _dosageService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}