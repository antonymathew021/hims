﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.DesignationSevices;
using HIMS.Service.DesignationSevices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class DesignationController : BaseController
	{
		private readonly IDesignationMasterService _designationMasterService;
		private readonly IConstantService _constantService;

		public DesignationController(IDesignationMasterService designationMasterService, IConstantService constantService)
		{
			_designationMasterService = designationMasterService;
			_constantService = constantService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<DesignationMasterDto> modelDT)
		{
			try
			{
				var designationList = await _designationMasterService.GetAll(modelDT);
				return Json(new { data = designationList.Item1, draw = Request["draw"], recordsTotal = designationList.Item2, recordsFiltered = designationList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(DesignationMasterDto countryMasterDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (countryMasterDto.Id == 0)
					{
						await _designationMasterService.CreateAsync(countryMasterDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _designationMasterService.Update(countryMasterDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _designationMasterService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _designationMasterService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}