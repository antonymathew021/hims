﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
			var hospitalId=SflSession.HospitalMasterId;
            return View();
        }
    }
}