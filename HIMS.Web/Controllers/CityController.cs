﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.CityMasterServices;
using HIMS.Service.CityMasterServices.Dto;
using HIMS.Service.Constant;
using HIMS.Service.CountryMasterServices;
using HIMS.Service.Model;
using HIMS.Service.StateMasterServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class CityController : BaseController
	{
		// GET: City
		private readonly ICityMasterService _cityMasterService;
		private readonly IStateMasterService _stateMasterService;
		private readonly ICountryMasterService _countryMasterService;
		private readonly IConstantService _constantService;

		public CityController(IStateMasterService stateMasterService, ICityMasterService cityMasterService, ICountryMasterService countryMasterService, IConstantService constantService)
		{
			_cityMasterService = cityMasterService;
			_countryMasterService = countryMasterService;
			_constantService = constantService;
			_stateMasterService = stateMasterService;
		}
		// GET: City
		public ActionResult Index()
		{
			return View();
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetCountryDropDownList()
		{
			var data = _countryMasterService.GetAll(null).Item1.Where(u => u.Recstatus == "Active");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetStateDropDownList()
		{
			var data = _stateMasterService.GetAll(null).Item1.Where(u => u.Recstatus == "Active");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<CityMasterDto> modelDT)
		{
			try
			{
				var cityList = await _cityMasterService.GetAll(modelDT);
				return Json(new { data = cityList.Item1, draw = Request["draw"], recordsTotal = cityList.Item2, recordsFiltered = cityList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(CityMasterDto cityMasterDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (cityMasterDto.Id == 0)
					{
						await _cityMasterService.CreateAsync(cityMasterDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _cityMasterService.Update(cityMasterDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _state = await _cityMasterService.GetById(Id);
				return Json(_state, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _cityMasterService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public async Task<ActionResult> GetList()
        {
            try
            {
                var cityList = await _cityMasterService.GetList();
                return Json(cityList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}