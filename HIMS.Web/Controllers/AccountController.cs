﻿using HIMS.Data.Authorization;
using HIMS.Data.Entities;
using HIMS.Service.User;
using HIMS.Web.Models.Account;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class AccountController : BaseController
	{
		private ApplicationSignInManager _signInManager;
		public IUserService _userAppService;

		public AccountController(IUserService userAppService, ApplicationSignInManager signInManager)
		{
			_userAppService = userAppService;
			SignInManager = signInManager;
		}

		public ApplicationSignInManager SignInManager
		{
			get
			{
				return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
			}
			private set
			{
				_signInManager = value;
			}
		}

		[AllowAnonymous] 
		public ActionResult Login(string returnUrl)
		{
			HttpCookie reqCookies = Request.Cookies["userInfo"];
			LoginViewModel model = new LoginViewModel();
			if (reqCookies != null)
			{
				model.Username = reqCookies["UserName"].ToString();
				model.Password = reqCookies["Password"].ToString();
			}
			ViewBag.ReturnUrl = returnUrl;
			return View(model);
		}

		[HttpPost]
		[AllowAnonymous]
		//[ValidateAntiForgeryToken]
		public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}
			RemoveCookie("userInfo");

			var result = await SignInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, shouldLockout: false);
			switch (result)
			{
				case SignInStatus.Success:
					if (model.RememberMe)
					{
						HttpCookie userInfo = new HttpCookie("userInfo");
						userInfo["UserName"] = model.Username;
						userInfo["Password"] = model.Password;
						userInfo.Expires.Add(new TimeSpan(0, 1, 0));
						Response.Cookies.Add(userInfo);
					}
					var userId = SignInManager.AuthenticationManager.AuthenticationResponseGrant.Identity.GetUserId();
					if (_userAppService.ValidateUserById(Int64.Parse(userId)))
						return RedirectToLocal(returnUrl);
					else
						ModelState.AddModelError("", "User is Inactive contact Admin");
					return View(model);
				case SignInStatus.LockedOut:
					return View("Lockout");
				case SignInStatus.RequiresVerification:
					return RedirectToAction($"SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
				case SignInStatus.Failure:
				default:
					ModelState.AddModelError("", "Invalid login attempt.");
					return View(model);
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult LogOff()
		{
			AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
			return RedirectToAction("Login", "Account");
		}

		public void RemoveCookie(string cookieName)
		{
			if (Response.Cookies[cookieName] != null)
			{
				Response.Cookies[cookieName].Value = null;
				Response.Cookies[cookieName].Expires = DateTime.UtcNow.AddMonths(-1);
			}
		}


		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return HttpContext.GetOwinContext().Authentication;
			}
		}

		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			return RedirectToAction("Index", "Home");
		}
	}
}