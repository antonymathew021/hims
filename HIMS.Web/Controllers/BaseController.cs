﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Core;
using log4net.Core;
using Ninject;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
	public class BaseController : Controller
	{
		/// <summary>
		/// Gets current session information.
		/// </summary>
		[Inject]
		public ISFLSession SflSession { get; set; }
		
		//[Inject]
		//public ILogger Logger { get; set; }

	}
}