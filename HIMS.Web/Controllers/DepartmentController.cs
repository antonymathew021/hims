﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.DepartmentServices;
using HIMS.Service.DepartmentServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class DepartmentController : BaseController
    {
        private readonly IDepartmentMasterService _departmentMasterService;
		private readonly IConstantService _constantService;

		public DepartmentController(IDepartmentMasterService departmentMasterService, IConstantService constantService)
		{
			_departmentMasterService = departmentMasterService;
			_constantService = constantService;
		}

		public JsonResult GetStatusDropDownList()
		{
			var data = _constantService.GetDropDownListFromTable("recstatus");
			return Json(data, JsonRequestBehavior.AllowGet);
		}

		// GET: Country
		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> Index(DataTableAjaxPostModel<DepartmentMasterDto> modelDT)
		{
			try
			{
				var departmentList = await _departmentMasterService.GetAll(modelDT);
				return Json(new { data = departmentList.Item1, draw = Request["draw"], recordsTotal = departmentList.Item2, recordsFiltered = departmentList.Item3 }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Create(DepartmentMasterDto countryMasterDto)
		{
			try
			{
				if (ModelState.IsValid)
				{
					if (countryMasterDto.Id == 0)
					{
						await _departmentMasterService.CreateAsync(countryMasterDto);
						return Json(new { status = "Created" });
					}
					else
					{
						await _departmentMasterService.Update(countryMasterDto);
						return Json(new { status = "Updated" });
					}
				}
				else
					return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Edit(long Id)
		{
			try
			{
				var _country = await _departmentMasterService.GetById(Id);
				return Json(_country, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public async Task<ActionResult> Delete(long Id)
		{
			try
			{
				await _departmentMasterService.Delete(Id);
				return Json(true, JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
    }
}