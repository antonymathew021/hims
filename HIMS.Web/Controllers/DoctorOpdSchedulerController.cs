﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.DoctorOPDServices;
using HIMS.Service.DoctorOPDServices.Dto;
using HIMS.Service.StaffServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class DoctorOpdSchedulerController : BaseController
    {
        private readonly IConstantService _constantService;
        private readonly IDoctorOPDMasterService _doctorOPDMasterService;
        private readonly IStaffMasterService _staffMasterService;

        public DoctorOpdSchedulerController(IConstantService constantService,
                                            IDoctorOPDMasterService doctorOPDMasterService,
                                            IStaffMasterService staffMasterService)
        {
            _constantService = constantService;
            _doctorOPDMasterService = doctorOPDMasterService;
            _staffMasterService = staffMasterService;
        }

        #region Bind Dropdowns
        public void BindAllDropdowns()
        {
            BindDoctors();
            BindWeekType();
        }
        public void BindDoctors()
        {
            var data = _staffMasterService.GetAllDoctors();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "StaffName");
                ViewBag.Doctors = _reftype;
            }
        }

        public void BindWeekType()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Monday", Value = "Monday"},
                new SelectListItem{Text = "Tuesday", Value = "Tuesday"},
                new SelectListItem{Text = "Wednesday", Value = "Wednesday"},
                new SelectListItem{Text = "Thursday", Value = "Thursday"},
                new SelectListItem{Text = "Friday", Value = "Friday"},
                new SelectListItem{Text = "Saturday", Value = "Saturday"},
                new SelectListItem{Text = "Sunday", Value = "Sunday"}
            };
            IEnumerable<SelectListItem> _reftype = items;
            ViewBag.WeekType = _reftype;
        }
        #endregion

        // GET: DoctorOpdScheduler
        [HttpGet]
        public ActionResult Index()
        {
            BindAllDropdowns();
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Index(DoctorOPDDto doctorOPDDto)
        {
            try
            {
                BindAllDropdowns();
                var result = await _doctorOPDMasterService.Update(doctorOPDDto);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<JsonResult> GetListByDoctor(long Id)
        {
            try
            {
                var result = await _doctorOPDMasterService.GetAllByDoctor(Id);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}