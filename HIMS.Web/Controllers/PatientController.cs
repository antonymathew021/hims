﻿using HIMS.Service.Constant;
using HIMS.Service.Model;
using HIMS.Service.PatientRegistrationServices;
using HIMS.Service.PatientRegistrationServices.Dto;
using HIMS.Service.ReferanceServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class PatientController : Controller
    {
        private readonly IPatientRegistrationTransactionService _patientRegistrationService;
        private readonly IConstantService _constantService;
        private readonly IReferanceMasterService _referanceMasterService;

        public PatientController(IPatientRegistrationTransactionService patientRegistrationService,
                                 IConstantService constantService,
                                 IReferanceMasterService referanceMasterService)
        {
            _patientRegistrationService = patientRegistrationService;
            _constantService = constantService;
            _referanceMasterService = referanceMasterService;
        }

        #region Bind Dropdowns
        public void BindAllDropdowns()
        {
            BindStatus();
            BindReferenceType();
            BindBloodGroup();
            BindCity();
            BindCountry();
            BindGender();
            BindState();
            BindPrefix();
            BindMaritialStatus();
            BindNationality();
            BindOccupation();
            BindReferredBy();
        }
        private void BindStatus()
        {
            var data = _constantService.GetDropDownListFromTable("recstatus");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.Status = _reftype;
            }
        }

        private void BindReferenceType()
        {
            var data = _constantService.GetDropDownListFromTable("reftype");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.ReferenceType = _reftype;
            }
        }
        private void BindBloodGroup()
        {
            var data = _constantService.GetDropDownListFromTable("bloodgroup");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.BloodGroup = _reftype;
            }
        }
        private void BindCountry()
        {
            var data = _constantService.GetCountryList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "Countryname");
                ViewBag.Country = _reftype;
            }
        }
        private void BindState()
        {
            var data = _constantService.GetStateList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "Statename");
                ViewBag.State = _reftype;
            }
        }
        private void BindCity()
        {
            var data = _constantService.GetCityList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "Id", "Cityname");
                ViewBag.City = _reftype;
            }
        }
        private void BindGender()
        {
            var data = _constantService.GetDropDownListFromTable("gender");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.Gender = _reftype;
            }
        }
        private void BindPrefix()
        {
            var data = _constantService.GetDropDownListFromTable("prefix");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.Prefix = _reftype;
            }
        }
        private void BindNationality()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Indian", Value = "Indian"},
                new SelectListItem{Text = "American", Value = "American"}
            };
            IEnumerable<SelectListItem> _reftype = items;
            ViewBag.Nationality = _reftype;
        }
        private void BindMaritialStatus()
        {
            var data = _constantService.GetDropDownListFromTable("maritalstatus");
            if (data != null)
            {
                IEnumerable<SelectListItem> _reftype = data.Select(x => new SelectListItem { Value = x, Text = x });
                ViewBag.MaritalStatus = _reftype;
            }
        }
        private void BindOccupation()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem{Text = "Service", Value = "Service"}
            };
            IEnumerable<SelectListItem> _reftype = items;
            ViewBag.Occupation = _reftype;
        }
        private void BindReferredBy()
        {
            var data = _referanceMasterService.GetList();
            if (data != null)
            {
                var _reftype = new SelectList(data.ToList(), "id", "refname");
                ViewBag.ReferredBy = _reftype;
            }
        }
        #endregion

        // GET: Country
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(DataTableAjaxPostModel<PatientRegistrationDto> modelDT)
        {
            try
            {
                var patientRegistration = await _patientRegistrationService.GetAll(modelDT);
                return Json(new { data = patientRegistration.Item1, draw = Request["draw"], recordsTotal = patientRegistration.Item2, recordsFiltered = patientRegistration.Item3 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            BindAllDropdowns();
            var patientRegistrationDto = new PatientRegistrationDto();
            return View(patientRegistrationDto);
        }

        [HttpPost]
        public async Task<ActionResult> Create(PatientRegistrationDto patientRegistrationDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (patientRegistrationDto.ProfilePhoto != null)
                    {
                        using (Stream inputStream = patientRegistrationDto.ProfilePhoto.InputStream)
                        {
                            MemoryStream memoryStream = inputStream as MemoryStream;
                            if (memoryStream == null)
                            {
                                memoryStream = new MemoryStream();
                                inputStream.CopyTo(memoryStream);
                            }
                            patientRegistrationDto.Patimage = memoryStream.ToArray();
                        }
                    }

                    await _patientRegistrationService.CreateAsync(patientRegistrationDto);
                    return RedirectToAction("Index");
                }
                else
                {
                    BindAllDropdowns();
                    return View("Create", patientRegistrationDto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit(long Id)
        {
            try
            {
                BindAllDropdowns();
                var _country = await _patientRegistrationService.GetById(Id);
                return View(_country);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<ActionResult> Edit(long Id, PatientRegistrationDto patientRegistrationDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (patientRegistrationDto.ProfilePhoto != null)
                    {
                        using (Stream inputStream = patientRegistrationDto.ProfilePhoto.InputStream)
                        {
                            MemoryStream memoryStream = inputStream as MemoryStream;
                            if (memoryStream == null)
                            {
                                memoryStream = new MemoryStream();
                                inputStream.CopyTo(memoryStream);
                            }
                            patientRegistrationDto.Patimage = memoryStream.ToArray();
                        }
                    }
                    await _patientRegistrationService.Update(patientRegistrationDto);
                    return RedirectToAction("Index");
                }
                else
                {
                    BindAllDropdowns();
                    return View("Edit", patientRegistrationDto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> Delete(long Id)
        {
            try
            {
                await _patientRegistrationService.Delete(Id);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> GetList()
        {
            try
            {
                var patientRegistration = await _patientRegistrationService.GetList();
                return Json(patientRegistration, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}