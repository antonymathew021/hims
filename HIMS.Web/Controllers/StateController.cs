﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.CountryMasterServices;
using HIMS.Service.Model;
using HIMS.Service.StateMasterServices;
using HIMS.Service.StateMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class StateController : BaseController
    {
        private readonly IStateMasterService _stateMasterService;
        private readonly ICountryMasterService _countryMasterService;
        private readonly IConstantService _constantService;

        public StateController(IStateMasterService stateMasterService, ICountryMasterService countryMasterService, IConstantService constantService)
        {
            _stateMasterService = stateMasterService;
            _countryMasterService = countryMasterService;
            _constantService = constantService;
        }
        // GET: State
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(DataTableAjaxPostModel<StateMasterDto> modelDT)
        {
            try
            {
                var stateList = _stateMasterService.GetAll(modelDT);
                return Json(new { data = stateList.Item1, draw = Request["draw"], recordsTotal = stateList.Item2, recordsFiltered = stateList.Item3 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> Create(StateMasterDto stateMasterDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (stateMasterDto.Id == 0)
                    {
                        await _stateMasterService.CreateAsync(stateMasterDto);
                        return Json(new { status = "Created" });
                    }
                    else
                    {
                        await _stateMasterService.Update(stateMasterDto);
                        return Json(new { status = "Updated" });
                    }
                }
                else
                    return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> Edit(long Id)
        {
            try
            {
                var _state = await _stateMasterService.GetById(Id);
                return Json(_state, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> Delete(long Id)
        {
            try
            {
                await _stateMasterService.Delete(Id);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> GetList()
        {
            try
            {
                var countryList = await _stateMasterService.GetList();
                return Json(countryList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}