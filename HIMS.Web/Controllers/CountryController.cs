﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Service.Constant;
using HIMS.Service.CountryMasterServices;
using HIMS.Service.CountryMasterServices.Dto;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HIMS.Web.Controllers
{
    public class CountryController : BaseController
    {
        private readonly ICountryMasterService _countryMasterService;
        private readonly IConstantService _constantService;

        public CountryController(ICountryMasterService countryMasterService, IConstantService constantService)
        {
            _countryMasterService = countryMasterService;
            _constantService = constantService;
        }

        public JsonResult GetStatusDropDownList()
        {
            var data = _constantService.GetDropDownListFromTable("recstatus");
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Country
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(DataTableAjaxPostModel<CountryMasterDto> modelDT)
        {
            try
            {
                var countryList = _countryMasterService.GetAll(modelDT);
                return Json(new { data = countryList.Item1, draw = Request["draw"], recordsTotal = countryList.Item2, recordsFiltered = countryList.Item3 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> Create(CountryMasterDto countryMasterDto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (countryMasterDto.Id == 0)
                    {
                        await _countryMasterService.CreateAsync(countryMasterDto);
                        return Json(new { status = "Created" });
                    }
                    else
                    {
                        await _countryMasterService.Update(countryMasterDto);
                        return Json(new { status = "Updated" });
                    }
                }
                else
                    return Json(new { status = "failed" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> Edit(long Id)
        {
            try
            {
                var _country = await _countryMasterService.GetById(Id);
                return Json(_country, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> Delete(long Id)
        {
            try
            {
                await _countryMasterService.Delete(Id);
                return Json(true, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ActionResult> GetList()
        {
            try
            {
                var countryList = await _countryMasterService.GetList();
                return Json(countryList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}