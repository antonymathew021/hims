﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using HIMS.Data.Authorization;
using HIMS.Data.Context;
using HIMS.Data.Entities;

[assembly: OwinStartupAttribute(typeof(HIMS.Web.Startup))]
namespace HIMS.Web
{
	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			ConfigureAuth(app); 
		}

        public void ConfigureAuth(IAppBuilder app)
        {
            app.CreatePerOwinContext(HIMSContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator
                        .OnValidateIdentity<ApplicationUserManager, ApplicationUser, long>(
                            validateInterval: TimeSpan.FromMinutes(30),
                             regenerateIdentityCallback: (manager, user) =>
                             user.GenerateUserIdentityAsync(manager),
                             getUserIdCallback: (id) => (id.GetUserId<int>()))
                },
                ExpireTimeSpan = TimeSpan.FromMinutes(30),
                LogoutPath = new PathString("/Account/Logoff")
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

        }
    }
}