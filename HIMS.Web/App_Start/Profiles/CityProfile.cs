﻿using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Service.CityMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HIMS.Web.App_Start.Profiles
{
    public class CityProfile : Profile
    {
        public CityProfile()
        {
            CreateMap<CityMasterDto, CityMaster>(MemberList.Source);
            CreateMap<CityMaster, CityMasterDto>();
        }
    }
}