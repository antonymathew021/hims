﻿using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Service.StateMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HIMS.Web.App_Start.Profiles
{
    public class StateProfile : Profile
    {
        public StateProfile()
        {
            CreateMap<StateMasterDto, StateMaster>(MemberList.Source);
            CreateMap<StateMaster, StateMasterDto>();
        }
    }
}