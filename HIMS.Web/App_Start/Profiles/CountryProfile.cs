﻿using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Service.CountryMasterServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HIMS.Web.App_Start.Profiles
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<CountryMasterDto, CountryMaster>(MemberList.Source);
            CreateMap<CountryMaster, CountryMasterDto>();
        }
    }
}