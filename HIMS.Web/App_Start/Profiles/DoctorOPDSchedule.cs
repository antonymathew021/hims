﻿using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Service.DoctorOPDServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HIMS.Web.App_Start.Profiles
{
    public class DoctorOPDSchedule : Profile
    {
        public DoctorOPDSchedule()
        {
            CreateMap<DoctorOPDDto, DoctorOPDMaster>(MemberList.Source);
            CreateMap<DoctorOPDMaster, DoctorOPDDto>();
        }
    }
}