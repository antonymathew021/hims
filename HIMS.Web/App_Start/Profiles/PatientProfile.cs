﻿using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Service.PatientRegistrationServices.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HIMS.Web.App_Start.Profiles
{
    public class PatientProfile : Profile
    {
        public PatientProfile()
        {
            CreateMap<PatientRegistrationDto, PatientRegistrationTransaction>(MemberList.Source);
            CreateMap<PatientRegistrationTransaction, PatientRegistrationDto>();
        }
    }
}