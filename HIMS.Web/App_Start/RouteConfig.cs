﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 
using System.Web.Mvc;
using System.Web.Routing;

namespace HIMS.Web
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
			);
		}
	}
}
