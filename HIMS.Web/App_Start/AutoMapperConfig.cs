﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;

namespace HIMS.Web.App_Start
{
	public class AutoMapperConfig
	{
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
                cfg.AddProfiles(new[] {
                    "HIMS.Web",
                    "HIMS.Service"
                }));
        }
    }
}