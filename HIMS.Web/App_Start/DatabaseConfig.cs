﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace HIMS.Web.App_Start
{
	public class DatabaseConfig
	{
		public static void Configure()
		{
			var migrator = new DbMigrator(new HIMS.Data.Migrations.Configuration());
			migrator.Update();
		}
	}
}