﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// ============================= 
using System.IO; 
using System.Web.Hosting;

namespace HIMS.Web.App_Start
{
	public class LogConfig
	{
        public static void Register()
        { 
            var log4NetFileInfo = new FileInfo(Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "log4net.config"));
             
            log4net.Config.XmlConfigurator.Configure(log4NetFileInfo);
        }
    }
}