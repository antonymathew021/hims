﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using System.Web;
using System.Web.Optimization;

namespace HIMS.Web
{
	public class BundleConfig
	{
		// For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{

			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.validate*"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
					  "~/Scripts/bootstrap.js"));

			bundles.Add(new StyleBundle("~/Content/css").Include(
					  "~/Content/bootstrap.css",
					  "~/Content/site.css"));

			//APPLICATION RESOURCES

			//~/Bundles/css
			bundles.Add(
				new StyleBundle("~/Bundles/css")
					.Include("~/css/main.css")
				);

			//Dashboard css
			bundles.Add(
				new StyleBundle("~/Bundles/Dashboard-Css")
					 .Include("~/Theme/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css", new CssRewriteUrlTransformWrapper()));

			//Dashboard JS
			bundles.Add(
				new ScriptBundle("~/Bundles/Dashboard-Js")
						.Include("~/Theme/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js")
						.Include("~/Theme/assets/app/js/dashboard.js"));

			//Global css
			bundles.Add(
				new StyleBundle("~/Bundles/Global-Css")
						.Include("~/Theme/assets/vendors/base/vendors.bundle.css", new CssRewriteUrlTransformWrapper())
						.Include("~/Theme/assets/demo/default/base/style.bundle.css", new CssRewriteUrlTransformWrapper())
						.Include("~/Theme/assets/vendors/custom/datatables/datatables.bundle.css", new CssRewriteUrlTransformWrapper())
						.Include("~/Content/PagedList.css", new CssRewriteUrlTransformWrapper())
						.Include("~/Theme/assets/demo/default/base/style.css", new CssRewriteUrlTransformWrapper()));

			//Global js
			bundles.Add(
				new ScriptBundle("~/Bundles/Global-Js")
						.Include("~/Theme/assets/vendors/base/vendors.bundle.js")
						.Include("~/Theme/assets/demo/default/base/scripts.bundle.js")
						.Include("~/Theme/assets/demo/default/custom/crud/forms/widgets/bootstrap-select.js")
						.Include("~/Scripts/Custom/currency.js")
						.Include("~/Theme/assets/demo/default/custom/components/base/toastr.js")
						.Include("~/Theme/assets/vendors/custom/datatables/datatables.bundle.js")
						.Include("~/Scripts/Custom/Common.js")
						.Include("~/Theme/assets/demo/default/custom/crud/forms/widgets/form-repeater.js"));
		}

	}
	public class CssRewriteUrlTransformWrapper : IItemTransform
	{
		public string Process(string includedVirtualPath, string input)
		{
			return new CssRewriteUrlTransform().Process("~" + System.Web.VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
		}
	}
}
