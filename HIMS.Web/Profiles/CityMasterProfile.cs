﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Service.CityMasterServices.Dto; 

namespace HIMS.Web.Profiles
{
	public class CityMasterProfile : Profile
	{
		public CityMasterProfile()
		{
			CreateMap<CityMaster, CityMasterDto>();
			CreateMap<CityMasterDto, CityMaster>(MemberList.Source);
		}
	}
}