﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Service.DepartmentServices.Dto;
using HIMS.Service.DesignationSevices.Dto;
using HIMS.Service.DosageServices.Dto;
using HIMS.Service.MedicineCategoryServices.Dto;
using HIMS.Service.MedicineDrugServices.Dto;
using HIMS.Service.ReferanceServices.Dto;
using HIMS.Service.RemarkServices.Dto;
using HIMS.Service.SpecialityMasterServices.Dto;
using HIMS.Service.StaffServices.Dto;
using HIMS.Service.TieupServices.Dto;
using HIMS.Service.VitalDataServices.Dto;
using HIMS.Service.WorkTypeServices.Dto;

namespace HIMS.Web.Profiles
{
	public class SpecialityMasterProfile : Profile
	{
		public SpecialityMasterProfile()
		{
			CreateMap<SpecialityMaster, SpecialityMasterDto>();
			CreateMap<SpecialityMasterDto, SpecialityMaster>(MemberList.Source);
			
			CreateMap<DesignationMaster, DesignationMasterDto>();
			CreateMap<DesignationMasterDto, DesignationMaster>(MemberList.Source);

			
			CreateMap<DepartmentMaster, DepartmentMasterDto>();
			CreateMap<DepartmentMasterDto, DepartmentMaster>(MemberList.Source);

			CreateMap<ReferanceMaster, ReferanceDto>();
			CreateMap<ReferanceDto, ReferanceMaster>(MemberList.Source);

			CreateMap<RemarkMaster, RemarkDto>();
			CreateMap<RemarkDto, RemarkMaster>(MemberList.Source);
			

			CreateMap<WorkTypeDto, WorkTypeMaster>();
			CreateMap<WorkTypeMaster, WorkTypeDto>(MemberList.Source);

			
			CreateMap<VitalDataMaster, VitalDataDto>();
			CreateMap<VitalDataDto, VitalDataMaster>(MemberList.Source); 

			CreateMap<MedicineDrugMaster, MedicineDrugDto>();
			CreateMap<MedicineDrugDto, MedicineDrugMaster>(MemberList.Source); 

			
			CreateMap<MedicineCategoryMaster, MedicineCategoryDto>();
			CreateMap<MedicineCategoryDto, MedicineCategoryMaster>(MemberList.Source); 

			
			CreateMap<StaffMaster, StaffCreateDto>();
			CreateMap<StaffCreateDto, StaffMaster>(MemberList.Source); 

			
			CreateMap<StaffMaster, StaffDto>();
			CreateMap<StaffDto, StaffMaster>(MemberList.Source); 

			CreateMap<TieupMaster, TieupDto>();
			CreateMap<TieupDto, TieupMaster>(MemberList.Source); 
			
			CreateMap<DosageMaster, DosageDto>();
			CreateMap<DosageDto, DosageMaster>(MemberList.Source);

		}
	}
}