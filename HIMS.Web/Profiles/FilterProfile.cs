﻿using AutoMapper;
using HIMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HIMS.Web.Profiles
{
	public class FilterProfile : Profile
	{
		public FilterProfile()
		{
			CreateMap<ListFilterViewModel, ListFilter>()
				.ForMember(b => b.Take, cfg => cfg.MapFrom(v => v.ItemsCount))
				.ForMember(b => b.Skip, cfg => cfg.MapFrom(v => v.Page * v.ItemsCount));
		}
	}
}