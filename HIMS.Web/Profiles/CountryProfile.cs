﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using AutoMapper;
using HIMS.Data.Entities;
using HIMS.Service.CountryMasterServices.Dto; 

namespace HIMS.Web.Profiles
{
	public class CountryProfile : Profile
	{
		public CountryProfile()
		{
			CreateMap<CountryMasterDto, CountryMaster>(MemberList.Source);
			CreateMap<CountryMaster, CountryMasterDto>();
		}

	}
}