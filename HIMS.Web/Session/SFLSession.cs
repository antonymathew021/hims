﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Core;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace HIMS.Web.Session
{
	[Serializable]
	public class SFLSession : ISFLSession
	{
		private HttpContext _context;
		private ClaimsIdentity _claims;
		public SFLSession(HttpContext context)
		{
			_context = context;
			_claims = (ClaimsIdentity)_context.User.Identity;
			//var userId = _claims.Claims.SingleOrDefault(c => c.Type == CustomClaims.UserId);
		}


		public long? UserId
		{
			get
			{
				return (_context != null && _context.User != null && _context.User.Identity != null && _context.User.Identity.IsAuthenticated) ? _context.User.Identity.GetUserId<long>() : default(long);
			}
		}
		public string UserName
		{
			get
			{
				return (_context != null && _context.User != null && _context.User.Identity != null && _context.User.Identity.IsAuthenticated) ? _context.User.Identity.GetUserName() : null;
			}
		}
		public string Name
		{
			get
			{
				return (_context != null && _context.User != null && _context.User.Identity != null && _context.User.Identity.IsAuthenticated) ? _context.User.Identity.Name : null;
			}
		}

		public long? HospitalMasterId
		{
			get
			{
				return (_context != null && _context.User != null && _context.User.Identity != null && _context.User.Identity.IsAuthenticated) ? Convert.ToInt64(((ClaimsIdentity)_context.User.Identity).FindFirst("HospitalMasterId")) : 0;
			}
		}
	}
}