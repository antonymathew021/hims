﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using Ninject;
using System.Web;
using HIMS.Data.UnitOfWork;
using Ninject.Web.Common;
using HIMS.Data.Authorization;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using Ninject.Extensions.Conventions;
using HIMS.Data.Repository;
using log4net.Core;
using Ninject.Modules;
using log4net;

namespace HIMS.Web.Ninject
{
	public class NinjectWebCommon
	{
        public static void RegisterServices(IKernel kernel)
        { 
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
            kernel.Bind(typeof(IRepository<>)).To(typeof(Repository<>)); 
            kernel.Bind(x => x.FromAssembliesMatching("*").SelectAllClasses().Excluding<UnitOfWork>().BindDefaultInterface());

            kernel.Bind<IAuthenticationManager>().ToMethod(c =>System.Web.HttpContext.Current.GetOwinContext().Authentication).InRequestScope();

            kernel.Bind<ApplicationUserManager>().ToMethod(c =>System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>()).InRequestScope(); 
        } 
	}
}