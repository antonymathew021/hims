﻿// =============================
// Email: info@blackbrackets.com
// www.blackbrackets.com
// =============================
using HIMS.Web.App_Start;
using HIMS.Web.Ninject;
using Ninject;
using Ninject.Web.Common.WebHost; 
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace HIMS.Web
{
	public class MvcApplication : NinjectHttpApplication
	{
		protected override void OnApplicationStarted()
		{ 
			LogConfig.Register();
			DatabaseConfig.Configure();
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AutoMapperConfig.Configure(); 
			ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
		}

		protected override IKernel CreateKernel()
		{
			var kernel = new StandardKernel();

			NinjectWebCommon.RegisterServices(kernel);
			return kernel;
		}

		protected void Application_Error()
		{ 
			//Elmah.ErrorSignal.FromCurrentContext().Raise(Server.GetLastError());
		}


		protected override void OnApplicationStopped()
		{ 
		}
	}
}
