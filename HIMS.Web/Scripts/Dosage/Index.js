﻿(function () {
    var model = {
        Id: 0,
        DosageCode: '',
        DosageName: '',
        RemarkId: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindStateList();
    });
    //Bind country list
    bindStateList = function () {
        var tblStates = $("#tblDosage").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Dosage/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "DosageCode", name: "DosageCode" },
                { data: "DosageName", name: "DosageName" },
                { data: "Remark", name: "Remark" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editDosage(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteDosage(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
        $('#txtSearch').on('keyup', function () {
            tblStates.search(this.value).draw();
        });
    }
    //Bind Status Dropdown
    bindStatusDropdown = function () {
        Common.AjaxJson('POST', "/Dosage/GetStatusDropDownList", "", 'application/x-www-form-urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Bind country dropdown
    bindRemarkDropdown = function () {
        Common.AjaxJson('POST', "/Dosage/GetRemarkDropDownList", "", 'application/x-www-form-urlencoded', successRemarkFill);
    };
    successRemarkFill = function (response) {
        $("#RemarkId").empty();
        $("#RemarkId").append($("<option></option>").val('Select').html('Select'));
        if (response != null) {
            $.each(response, function (key, value) {
                $("#RemarkId").append($("<option></option>").val(value.Id).attr("text", value.Id).html(value.RemarkName));
            });
        }
        $('#RemarkId').selectpicker('refresh');
    };
    //Open modal
    openDosageModal = function () {
        $("#Id").val('');
        $("#DosageCode").val('');
        $("#DosageName").val('');
        bindStatusDropdown();
        bindRemarkDropdown();
        $("#DosageModal").modal("toggle");
    };
    //Validate Dosage
    IsValidDosage = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#RemarkId").val()) || $("#RemarkId").val() == "Select" || typeof $('#RemarkId').val() == typeof undefined) {
            $("#lblRemarkError").text("Remark is required");
            isValid = false;
        }
        else { $("#lblRemarkError").text("") }
        if (Common.IsNullOrEmpty($("#DosageCode").val())) {
            $("#lblDosageCodeError").text("Dosage Code is required");
            isValid = false;
        } else { $("#lblDosageCodeError").text("") }
        if (Common.IsNullOrEmpty($("#DosageName").val())) {
            $("#lblDosageNameError").text("Dosage Name is required");
            isValid = false;
        } else { $("#lblDosageNameError").text("") }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.RemarkId = $("#RemarkId").val();
            model.DosageCode = $("#DosageCode").val();
            model.DosageName = $("#DosageName").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/Dosage/Create", model, 'application/x-www-form-urlencoded', successCreatedState);
        }
    }
    successCreatedState = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Dosage Created Successfully");
                $('#tblDosage').DataTable().ajax.reload();
                $("#DosageModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Dosage Updated Successfully");
                $('#tblDosage').DataTable().ajax.reload();
                $("#DosageModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editDosage = function (Id, btn) {
        bindRemarkDropdown();
        bindStatusDropdown();
        Common.AjaxJson("Get", "/Dosage/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillState);
    }
    successFillState = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#DosageCode").val(response.DosageCode);
            $("#DosageName").val(response.DosageName);
            $("#RemarkId").val(response.RemarkId);
            $('#RemarkId').selectpicker('refresh');
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#DosageModal").modal("toggle");
        }
    }
    //Delete country
    deleteDosage = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/Dosage/Delete/' + Id, 'Dosage Deleted Successfully', 'Dosage Not Deleted', '', 'Are you sure that you want to delete this Dosage?');
    }
})();