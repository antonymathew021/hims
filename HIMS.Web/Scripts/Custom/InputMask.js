﻿var Inputmask = {
    init: function () {
        $(".datemask").inputmask("mm/dd/yyyy", {
            autoUnmask: !0
        }), $("#m_inputmask_2").inputmask("mm/dd/yyyy", {
            placeholder: "*"
        }), $(".phonenumbermask").inputmask("mask", {
            mask: "(999) 999-9999"
        }), $(".phonenumberextmask").inputmask("mask", {
            mask: "9999"
        }), $(".phonenumberextmask1").inputmask("mask", {
            mask: "99999"
        }), $("#m_inputmask_4").inputmask({
            mask: "99-9999999",
            placeholder: ""
        }), $("#m_inputmask_5").inputmask({
            mask: "9",
            repeat: 10,
            greedy: !1
        }), $(".righalignnumericmask").inputmask("decimal", {
            rightAlignNumerics: !1
        }), $("#m_inputmask_7").inputmask("€ 999.999.999,99", {
            numericInput: !0
        }), $(".ipaddressmask").inputmask({
            mask: "999.999.999.999"
        }), $(".emailaddressmask").inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: !1,
            onBeforePaste: function (m, a) {
                return (m = m.toLowerCase()).replace("mailto:", "")
            },
            definitions: {
                "*": {
                    validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",
                    cardinality: 1,
                    casing: "lower"
                }
            }
        }),
            $(".numbermask").inputmask("mask", {
                mask: "99999-9999"
            })
    }
};
jQuery(document).ready(function () {
    Inputmask.init()
});