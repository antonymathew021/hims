var KeyBoard = {
    Backspace: 8,
    One: 48,
    Nine: 57,
    Zero: 0
}
var Common = {
    Ajax: function (httpMethod, url, data, type, successCallBack, async, cache) {
        if (typeof async == "undefined") {
            async = true;
        }
        if (typeof cache == "undefined") {
            cache = false;
        }

        var ajaxObj = $.ajax({
            type: httpMethod.toUpperCase(),
            url: url,
            data: data,
            dataType: type,
            async: async,
            cache: cache,
            success: successCallBack,
            error: function (err, type, httpStatus) {
                Common.AjaxFailureCallback(err, type, httpStatus);
            },
            beforeSend: function () {
                Common.onPageLoader();
            },
            complete: function () {
                Common.onPageLoadClose();
            }
        });

        return ajaxObj;
    },
    //AjaxJson: function (httpMethod, url, data, type, successCallBack, async, cache) {
    //    if (typeof async == "undefined") {
    //        async = true;
    //    }
    //    if (typeof cache == "undefined") {
    //        cache = false;
    //    }

    //    var ajaxObj = $.ajax({
    //        type: httpMethod.toUpperCase(),
    //        url: url,
    //        data: data,
    //        dataType: "json",
    //        async: async,
    //        cache: cache,
    //        success: successCallBack,
    //        error: function (err, type, httpStatus) {
    //            Common.AjaxFailureCallback(err, type, httpStatus);
    //        },
    //        beforeSend: function () {
    //            Common.onPageLoader();
    //        },
    //        complete: function () {
    //            Common.onPageLoadClose();
    //        }
    //    });
    //    return ajaxObj;
    //},

    AjaxJson: function (httpMethod, url, data, type, successCallBack, async, cache) {
        if (typeof async == "undefined") {
            async = true;
        }
        if (typeof cache == "undefined") {
            cache = false;
        }
        var ajaxObj = $.ajax({
            type: httpMethod.toUpperCase(),
            url: url,
            data: data,
            dataType: "json",
            async: async,
            cache: cache,
            success: successCallBack,
            error: function (err, type, httpStatus) {
                Common.AjaxFailureCallback(err, type, httpStatus);
            },
            beforeSend: function () {
                Common.onPageLoader();
            },
            complete: function () {
                Common.onPageLoadClose();
            }
        });
        return ajaxObj;
    },

    DisplaySuccess: function (message) {
        Common.ShowSuccessSavedMessage(message);
    },

    DisplayError: function (message) {
        Common.ShowFailSavedMessage(message);
    },

    AjaxFailureCallback: function (err, type, httpStatus) {
        var failureMessage = 'Error occurred in ajax call' + err.status + " - " + err.responseText + " - " + httpStatus;
    },

    ShowSuccessSavedMessage: function (messageText) {

        //use jquery BlockUI library to display message

        $.blockUI({ message: messageText });
        setTimeout($.unblockUI, 1500);
    },

    ShowFailSavedMessage: function (messageText) {

        //use jquery BlockUI library to display message

        $.blockUI({ message: messageText });
        setTimeout($.unblockUI, 1500);
    },
    SucessToaster: function (message, IsListPage) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"

        };
        toastr.options.onHidden = function () {
            // this will be executed after fadeout, i.e. 2secs after notification has been show
            if (IsListPage) {
                window.location.reload();
            }
        };
        if (message != "")
            toastr.success(message);
    },
    ErrorToaster: function (message) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        if (message != "")
            toastr.error(message);
    },
    WarningToaster: function (message) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        if (message != "")
            toastr.warning(message);
    },
    DeleteFromTable: function deleteUser(userId, btn, url, sucessMessage, errorMessage, warningMessage, message, IsListPage) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "POST"
        };
        swal({
            title: "Are you sure?",
            text: message,
            type: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }).then(function (isConfirm) {
            if (isConfirm.dismiss != "cancel") {
                $.ajax(settings).done(function (response) {
                    if (response == 1) {
                        //m_table_1
                        //document.getElementById("m_table_1").deleteRow(rowNumber);
                        var row = btn.parentNode.parentNode;
                        row.parentNode.removeChild(row);
                        Common.SucessToaster(sucessMessage, IsListPage);
                    }
                    else if (response == 2) {
                        Common.WarningToaster(warningMessage);
                    }
                    else {
                        Common.WarningToaster(errorMessage);
                    }
                });
            }
            else {
                Common.WarningToaster(errorMessage);
            }
        });
    },
    DeleteFromDiv: function deleteDiv(userId, btn, url, sucessMessage, errorMessage, warningMessage, message) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "POST"
        };
        swal({
            title: "Are you sure?",
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            allowOutsideClick: false,
            confirmButtonColor: "#ec6c62"
        }).then(function (isConfirm) {
            if (isConfirm.dismiss != "cancel") {
                $.ajax(settings).done(function (response) {
                    if (response == 1) {
                        //m_table_1
                        //document.getElementById("m_table_1").deleteRow(rowNumber);
                        var row = btn.parentNode.parentNode.parentNode.parentNode.parentNode;
                        row.parentNode.removeChild(row);
                        Common.SucessToaster(sucessMessage);
                        location.reload();

                    }
                    else if (response == 2) {
                        Common.WarningToaster(warningMessage);
                    }
                    else {
                        Common.WarningToaster(errorMessage);
                    }
                });
            }
            else {
                Common.WarningToaster(errorMessage);
            }
        });
    },
    DeleteFromSeqDiv: function deleteDiv(userId, childeNode, btn, url, sucessMessage, errorMessage, warningMessage, message) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "POST"
        };
        swal({
            title: "Are you sure?",
            text: message,
            type: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: "Yes, delete it!",
            confirmButtonColor: "#ec6c62"
        }).then(function (isConfirm) {
            if (isConfirm.dismiss != "cancel") {
                $.ajax(settings).done(function (response) {
                    if (response == 1) {
                        //m_table_1
                        //document.getElementById("m_table_1").deleteRow(rowNumber);
                        var row = btn.parentNode.parentNode.parentNode.parentNode;
                        row.parentNode.removeChild(row);
                        Common.SucessToaster(sucessMessage);
                        $(childeNode).html('');
                        location.reload();
                    }
                    else if (response == 2) {
                        Common.WarningToaster(warningMessage);
                    }
                    else {
                        Common.WarningToaster(errorMessage);
                    }
                });
            }
            else {
                Common.WarningToaster(errorMessage);
            }
        });
    },
    UpdateStatusFromTable: function updateStatus(userId, btn, url, sucessMessage, errorMessage, warningMessage, message) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "POST"
        };
        swal({
            title: "Are you sure?",
            text: message,
            type: "warning",
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: "Yes",
            confirmButtonColor: "#ec6c62"
        }).then(function (isConfirm) {
            if (isConfirm.dismiss != "cancel") {
                $.ajax(settings).done(function (response) {
                    if (response == 1) {
                        //m_table_1
                        //document.getElementById("m_table_1").deleteRow(rowNumber);
                        var row = btn.parentNode.parentNode;
                        row.parentNode.removeChild(row);
                        Common.SucessToaster(sucessMessage);
                        window.location.reload();
                    }
                    else if (response == 2) {
                        Common.WarningToaster(warningMessage);
                    }
                    else {
                        Common.WarningToaster(errorMessage);
                    }
                });
            }
            else {
                Common.WarningToaster(errorMessage);
            }
        });
    },
    DashboardActions: function updateStatus(userId, btn, url, sucessMessage, errorMessage, warningMessage, message, ToasterType) {
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": url,
            "method": "POST"
        };
        swal({
            title: "Are you sure?",
            text: message,
            type: ToasterType,
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: "Yes",
            confirmButtonColor: "#ec6c62"
        }).then(function (isConfirm) {
            if (isConfirm.dismiss != "cancel") {
                $.ajax(settings).done(function (response) {
                    if (response == 1) {
                        //m_table_1
                        //document.getElementById("m_table_1").deleteRow(rowNumber);
                        var row = btn.parentNode.parentNode;
                        row.parentNode.removeChild(row);
                        Common.SucessToaster(sucessMessage);
                    }
                    else if (response == 2) {
                        Common.WarningToaster(warningMessage);
                    }
                    else {
                        Common.WarningToaster(errorMessage);
                    }
                });
            }
            else {
                Common.WarningToaster(errorMessage);
            }
        });
    },
    GetVarFromQueryString: function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    number_to_price: function (v) {
        var price = currency(v, { formatWithSymbol: true }).format();
        return price;
    },
    number_to_price_six: function (v) {
        var s = v + "",
            d = s.indexOf('.') + 1;
        var precessionValue = !d ? 0 : s.length - d;
        if (precessionValue <= 0) {
            precessionValue = 6;
        }
        var price = currency(v, { formatWithSymbol: true, precision: precessionValue }).format();
        return price;
    },
    price_to_number: function (v) {
        var price = currency(v, { formatWithSymbol: false }).format();
        return price;
    },
    AutoCompleteVishayStyle: function (referenceId, refId, url, dataUrl) {

        $(referenceId).select2({
            ajax: {
                url: url,
                type: "POST",
                dataType: 'json',
                width: 'resolve',
                data: function (params) {
                    return {
                        q: params.term
                    };
                }, processResults: function (data, search) {
                    return {
                        results: $.map(data, function (sendData) {
                            return {
                                id: sendData.id,
                                text: sendData.text
                            }
                        })
                    };
                },
                cache: true
            },
            templateResult: function (item) {
                if (item.loading) return item.text;
                return item.text;
            }
        });
        if (dataUrl != null && dataUrl !== "") {
            Common.AjaxJson('POST', dataUrl + "/" + $(refId).val(), "", 'json', function (data) {
                $(referenceId).append('<option value="' + data.id + '">' + data.text + '</option>');
            });
        }
        //if ($(refId).val() != null && $(refId).val() !== "") {
        //    Common.AjaxJson('POST', "../Specifications/BindVishayStyleById/" + $(refId).val(), "", 'json', function (data) { 
        //        $("#Product").select2('data', {id: data.id, a_key: data.text});
        //    });
        //}
        $(referenceId).change(function () {
            var value = $(this).val();
            $(refId).val(value);
        });
    },
    AutoCompleteReplacedByPCB: function (referenceId, refId, url, dataUrl, paramValue) {
        $(referenceId).select2({
            ajax: {
                url: url,
                type: "POST",
                dataType: 'json',
                width: 'resolve',
                data: function (params) {
                    return {
                        q: params.term,
                        param: paramValue
                    };
                }, processResults: function (data, search) {
                    return {
                        results: $.map(data, function (sendData) {
                            return {
                                id: sendData.id,
                                text: sendData.text
                            }
                        })
                    };
                },
                cache: true
            },
            templateResult: function (item) {
                if (item.loading) return item.text;
                return item.text;
            }
        });
        if (dataUrl != null && dataUrl !== "") {
            Common.AjaxJson('POST', dataUrl + "/" + $(refId).val(), "", 'json', function (data) {
                var newOption = new Option(data.text, data.id, true, true);
                $(referenceId).append(newOption).trigger('change');
            });
        }
        $(referenceId).change(function () {
            var value = $(this).val();
            $(refId).val(value);
        });
    },
    AutoComplete: function (referenceId, refId, url, dataUrl) {
        $(referenceId).select2({
            ajax: {
                url: url,
                type: "POST",
                dataType: 'json',
                width: 'resolve',
                data: function (params) {
                    return {
                        q: params.term
                    };
                }, processResults: function (data, search) {
                    return {
                        results: $.map(data, function (sendData) {
                            return {
                                id: sendData.id,
                                text: sendData.text
                            }
                        })
                    };
                },
                cache: true
            },
            templateResult: function (item) {
                if (item.loading) return item.text;
                return item.text;
            }
        });
        if (dataUrl != null && dataUrl !== "") {
            Common.AjaxJson('POST', dataUrl + "/" + $(refId).val(), "", 'json', function (data) {
                var newOption = new Option(data.text, data.id, true, true);
                $(referenceId).append(newOption).trigger('change');
            });
        }
        $(referenceId).change(function () {
            var value = $(this).val();
            $(refId).val(value);
        });
    },
    AutoCompleteMultiSelect: function (referenceId, refId, url, dataUrl) {
        $(referenceId).select2({
            multiple: true,
            ajax: {
                url: url,
                type: "POST",
                dataType: 'json',
                width: 'resolve',
                data: function (params) {
                    return {
                        q: params.term
                    };
                }, processResults: function (data, search) {
                    return {
                        results: $.map(data, function (sendData) {
                            return {
                                id: sendData.id,
                                text: sendData.text
                            }
                        })
                    };
                },
                cache: true
            },
            templateResult: function (item) {
                if (item.loading) return item.text;
                return item.text;
            }
        });
        if (dataUrl != null && dataUrl !== "") {
            Common.AjaxJson('POST', dataUrl + "/" + $(refId).val(), "", 'json', function (data) {
                var newOption = new Option(data.text, data.id, true, true);
                $(referenceId).append(newOption).trigger('change');
            });
        }
        $(referenceId).change(function () {
            var value = $(this).val();
            $(refId).val(value);
        });
    },
    AutoCompleteModelMultiSelect: function (referenceId, refId, url, dataUrl, modelId) {
        $(referenceId).select2({
            multiple: true,
            dropdownParent: modelId,
            selectOnBlur: true,
            tags: true,
            width: '100%',
            ajax: {
                url: url,
                type: "POST",
                dataType: 'json',
                width: 'resolve',
                data: function (params) {
                    return {
                        q: params.term
                    };
                }, processResults: function (data, search) {
                    return {
                        results: $.map(data, function (sendData) {
                            return {
                                id: sendData.id,
                                text: sendData.text
                            }
                        })
                    };
                },
                cache: true
            },
            templateResult: function (item) {
                if (item.loading) return item.text;
                return item.text;
            }
        });
        if (dataUrl != null && dataUrl !== "") {
            Common.AjaxJson('POST', dataUrl + "/" + $(refId).val(), "", 'json', function (data) {
                var newOption = new Option(data.text, data.id, true, true);
                $(referenceId).append(newOption).trigger('change');
            });
        }
        $(referenceId).change(function () {
            var value = $(this).val();
            $(refId).val(value);
        });
    },
    isPositiveInteger: function (s) {
        return !!s.match(/^[0-9]+$/);
        // or Rob W suggests
        return /^\d+$/.test(s);
    },
    formatDate: function (data) {
        if (data === null) return "";
        var pattern = /Date\(([^)]+)\)/;
        var results = pattern.exec(data);
        if (results != null) {
            var dt = new Date(parseFloat(results[1]));
            var date = (dt.getMonth().length > 1 ? parseInt(dt.getMonth() + 1) : "0" + parseInt(dt.getMonth() + 1)) + "/" + ("0" + dt.getDate()).slice(-2) + "/" + dt.getFullYear().toString().substr(-2);
            return date;
        }
        else {
            return data;
        }
    },
    formatDateForDatatable: function (data) {
        return this.IsNotNullOrEmpty(data) ? moment(data).format("MM/DD/YY") : "";
    },
    formatTimeForDatatable: function (data) {
        return this.IsNotNullOrEmpty(data) ? moment(data).format("h:mm A") : "";
    },
    formatTime: function (data) {
        if (data === null) return "";
        var pattern = /Date\(([^)]+)\)/;
        var results = pattern.exec(data);
        var dt = new Date(parseFloat(results[1]));
        var date = dt.getHours() + ":" + dt.getMinutes();
        return date;
    },
    IsNotNullOrEmpty: function (ref) { if (ref != null && ref != "") return true; else return false; },
    IsNullOrEmpty: function (ref) { if (ref == null || ref == "") return true; else return false; },
    DisableField: function (input) { input.attr('disabled', 'disabled'); input.addClass('disableField') },
    EnableField: function (input) { var attri = input.attr('disabled'); if (typeof attri !== typeof undefined && attri !== false) { input.removeAttr('disabled'); } },
    convertTime12to24: function (time12H) {
        const [time, modifier] = time12H.split(' ');
        let [hours, minutes] = time.split(':');
        if (hours === '12') {
            hours = '00';
        }
        if (modifier === 'PM') {
            hours = parseInt(hours, 10) + 12;
        }
        return `${hours}:${minutes}`;
    },
    convertStringtoBool: function (string) {
        var boolValue = (/true/i).test(string) //returns true
        return boolValue;
    },
    checkValidTime24HrFormate: function (time) {
        var regexp = /([01]?[0-9]|2[0-3]):[0-5][0-9]/;
        return correct = (time.search(regexp) >= 0) ? true : false;
    },
    onPageLoader: function () {
        mApp.block(".modal-content", {
            overlayColor: "#000000",
            type: "loader",
            state: "primary",
            message: "Processing..."
        })
        mApp.blockPage({
            overlayColor: "#000000",
            type: "loader",
            state: "primary",
            message: "Processing..."
        })
    },
    onPageLoadClose: function () {
        mApp.unblockPage();
        mApp.unblock(".modal-content");
    }
}
// To input only numeric value (from 0 to 9)
$('.numeric').keypress(function (e) {
    if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
        e.preventDefault();
    }
    if ($(this).val().indexOf('.') != -1) {
        if ($(this).val().split(".")[1].length >= 2) {
            return false;
        }
        if ($(this).val().split(".")[1].length > 2) {
            if (isNaN(parseFloat(this.value))) return;
            return false;
        }
    }
});
$('.numericthreedecimal').keypress(function (e) {
    if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
        e.preventDefault();
    }
    if ($(this).val().indexOf('.') != -1) {
        if ($(this).val().split(".")[1].length >= 3) {
            return false;
        }
        if ($(this).val().split(".")[1].length > 3) {
            if (isNaN(parseFloat(this.value))) return;
            return false;
        }
    }
});
$('.numericsixdecimal').keypress(function (e) {
    if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
        e.preventDefault();
    }
    if ($(this).val().indexOf('.') != -1) {
        if ($(this).val().split(".")[1].length >= 6) {
            return false;
        }
    }
});
// To input only character value (from A to Z or a to z)
$('.characteronly').on('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});
$('.numberonly').on('keypress', function (event) {
    if (event.which !== KeyBoard.Backspace && event.which != KeyBoard.Zero && (event.which < KeyBoard.One || event.which > KeyBoard.Nine)) {
        event.preventDefault();
    }
    if ($(this).val().length > 8) {
        event.preventDefault();
    }
    if ($(this).attr("id") == "CarNo") {
        if ($(this).val().length > 6) {
            event.preventDefault();
        }
    }
});

$('.numberonlywithdash').on('keypress', function (event) {
    var regex = /^[0-9-]*$/;
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
    if ($(this).val().indexOf('-') > -1) {
        if (event.key == '-') {
            event.preventDefault();
        }
    }
    if ($(this).val().length > 10) {
        event.preventDefault();
    }
});

$('.numberonlywithnegative').on('keypress', function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && $(this).val().indexOf('-') != -1 && event.which != 45 && event.which != 109 && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});
// disable mousewheel on a input number field when in focus
// (to prevent Cromium browsers change the value when scrolling)
$('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault()
    })
})
$('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll')
})

$.extend($.fn.dataTable.defaults, {
    responsive: true,
    lengthMenu: [25, 50, 75, 100],
    pageLength: 25
});
function NotesLimit(evt, Id) {
    if (evt.value.length > 512) {
        $('#' + Id).html("The character limit exceeded");
    }
    else {
        $('#' + Id).html("");
    }
}
