﻿(function () {
    var model = {
        Id: 0,
        VitalCode: '',
        VitalName: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindVitalDataList();
    });
    //Bind country list
    bindVitalDataList = function () {
        var tblVitalData = $("#tblVitalData").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/VitalData/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "VitalCode", name: "VitalCode" },
                { data: "VitalName", name: "VitalName" },
                { data: "VitalUnit", name: "VitalUnit" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editVitalData(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteVitalData(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/VitalData/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        debugger;
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Open modal
    openVitalDataModal = function () {
        $("#Id").val('');
        $("#VitalCode").val('');
        $("#VitalName").val('');
        $("#VitalUnit").val('');
        bindStatudDropdown();
        $("#VitalDataModal").modal("toggle");
    };
    //Validate country
    IsValidVitalData = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#VitalCode").val())) {
            $("#lblVitalDataCodeError").html("VitalData code is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#VitalName").val())) {
            $("#lblVitalDataNameError").html("Vital Data Name is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#VitalUnit").val())) {
            $("#lblVitalDataUnitError").html("Vital Data Unit is required");
            isValid = false;
        }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.VitalCode = $("#VitalCode").val();
            model.VitalName = $("#VitalName").val();
            model.VitalUnit = $("#VitalUnit").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/VitalData/Create", model, 'application/x-www-form-urlencoded', successCreatedVitalData);
        }
    }
    successCreatedVitalData = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Vital Data Created Successfully");
                $('#tblVitalData').DataTable().ajax.reload();
                $("#VitalDataModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Vital Data Updated Successfully");
                $('#tblVitalData').DataTable().ajax.reload();
                $("#VitalDataModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editVitalData = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/VitalData/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillVitalData);
    }
    successFillVitalData = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#VitalCode").val(response.VitalCode);
            $("#VitalName").val(response.VitalName);
            $("#VitalUnit").val(response.VitalUnit);
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#VitalDataModal").modal("toggle");
        }
    }
    //Delete country
    deleteVitalData = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/VitalData/Delete/' + Id, 'Vital Data Deleted Successfully', 'Vital Data Not Deleted', '', 'Are you sure that you want to delete this VitalData?');
    }
})();