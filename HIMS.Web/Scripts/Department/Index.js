﻿(function () {
    var model = {
        Id: 0,
        DepartmentCode: '',
        DepartmentName: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindDepartmentList();
    });
    //Bind country list
    bindDepartmentList = function () {
        var tblDepartment = $("#tblDepartment").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Department/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "DepartmentCode", name: "DepartmentCode" },
                { data: "DepartmentName", name: "DepartmentName" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editDepartment(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteDepartment(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/Department/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        debugger;
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Open modal
    openDepartmentModal = function () {
        $("#Id").val('');
        $("#DepartmentCode").val('');
        $("#DepartmentName").val('');
        bindStatudDropdown();
        $("#DepartmentModal").modal("toggle");
    };
    //Validate country
    IsValidDepartment = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#DepartmentCode").val())) {
            $("#lblDepartmentCodeError").val("Department code is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#DepartmentName").val())) {
            $("#lblDepartmentNameError").val();
            isValid = false;
        }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.DepartmentCode = $("#DepartmentCode").val();
            model.DepartmentName = $("#DepartmentName").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/Department/Create", model, 'application/x-www-form-urlencoded', successCreatedDepartment);
        }
    }
    successCreatedDepartment = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Department Created Successfully");
                $('#tblDepartment').DataTable().ajax.reload();
                $("#DepartmentModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Department Updated Successfully");
                $('#tblDepartment').DataTable().ajax.reload();
                $("#DepartmentModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editDepartment = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/Department/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillDepartment);
    }
    successFillDepartment = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#DepartmentCode").val(response.DepartmentCode);
            $("#DepartmentName").val(response.DepartmentName);
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#DepartmentModal").modal("toggle");
        }
    }
    //Delete country
    deleteDepartment = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/Department/Delete/' + Id, 'Department Deleted Successfully', 'Department Not Deleted', '', 'Are you sure that you want to delete this Department?');
    }
})();