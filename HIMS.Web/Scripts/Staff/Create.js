﻿(function () {
    $(function () {
        loadFrontImage = function (event) {
            var output = document.getElementById('StaffSignFileImg');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.style.display = "";
        };

        loadBackImage = function (event) {
            var output = document.getElementById('ProfilePhotoFileImg');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.style.display = "";
        };
        bindSaffTypeDropdown();
        bindPrefixDropdown();
        bindStatudDropdown();
        bindGenderDropdown();
        bindBloodGroupDropdown();
        bindDesignationDropdown();
        bindDepartmentDropdown();
        bindSpecialityDropdown();
        bindCountryDropdown();
        bindStateDropdown();
        bindCityDropdown();
    }); 

 //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        $("#Recstatus").empty();
        $.each(response, function (key, value) {
            $("#Recstatus").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#Recstatus').selectpicker('refresh');
    }

    //Bind StaffType Dropdown
    bindSaffTypeDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetSaffTypeDropDownList", "", 'application / x - www - form - urlencoded', successSaffTypeFill);
    };
    successSaffTypeFill = function (response) {
        $("#StaffType").empty();
        $.each(response, function (key, value) {
            $("#StaffType").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#StaffType').selectpicker('refresh');
    }
    //Bind Gender Dropdown
    bindGenderDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetGenderDropDownList", "", 'application / x - www - form - urlencoded', successGenderFill);
    };
    successGenderFill = function (response) {
        $("#Gender").empty();
        $.each(response, function (key, value) {
            $("#Gender").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#Gender').selectpicker('refresh');
    }

    //Bind Prefix Dropdown
    bindBloodGroupDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetBloodGroupDropDownList", "", 'application / x - www - form - urlencoded', successBloodGroupFill);
    };
    successBloodGroupFill = function (response) {
        $("#BloodGroup").empty();
        $.each(response, function (key, value) {
            $("#BloodGroup").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#BloodGroup').selectpicker('refresh');
    }
    //Bind Prefix Dropdown
    bindPrefixDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetPrefixDropDownList", "", 'application / x - www - form - urlencoded', successPrefixFill);
    };
    successPrefixFill = function (response) {
        $("#Prefix").empty();
        $.each(response, function (key, value) {
            $("#Prefix").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#Prefix').selectpicker('refresh');
    }

    //Bind Designation Dropdown
    bindDesignationDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetDesignationDropDownList", "", 'application / x - www - form - urlencoded', successDesignationFill);
    };
    successDesignationFill = function (response) {
        $("#DesigId").empty();
        $.each(response, function (key, value) {
            $("#DesigId").append($("<option></option>").val(value.Id).attr("text", value.DesignationName).html(value.DesignationName));
        });
        $('#DesigId').selectpicker('refresh');
    }

    //Bind Department Dropdown
    bindDepartmentDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetDepartmentDropDownList", "", 'application / x - www - form - urlencoded', successDepartmentFill);
    };
    successDepartmentFill = function (response) {
        $("#DeptId").empty();
        $.each(response, function (key, value) {
            $("#DeptId").append($("<option></option>").val(value.Id).attr("text", value.DepartmentName).html(value.DepartmentName));
        });
        $('#DeptId').selectpicker('refresh');
    }

     //Bind Department Dropdown
    bindSpecialityDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetSpecialityDropDownList", "", 'application / x - www - form - urlencoded', successSpecialityFill);
    };
    successSpecialityFill = function (response) {
        $("#SpeclId").empty();
        $.each(response, function (key, value) {
            $("#SpeclId").append($("<option></option>").val(value.Id).attr("text", value.SpecialityName).html(value.SpecialityName));
        });
        $('#SpeclId').selectpicker('refresh');
    }

    //Bind country dropdown
    bindCountryDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetCountryDropDownList", "", 'application/x-www-form-urlencoded', successCountryFill);
    };
    successCountryFill = function (response) {
        debugger
        $("#CountryId").empty();
        if (response != null) {
            $("#CountryId").append($("<option></option>").val('Select').html('Select'));
            if (response != null) {
                $.each(response, function (key, value) {
                    $("#CountryId").append($("<option></option>").val(value.Id).attr("text", value.Countryname).html(value.Countryname));
                });
            }
        }
        $('#CountryId').selectpicker('refresh');
    };
    //Bind state dropdown
    bindStateDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetStateDropDownList", "", 'application/x-www-form-urlencoded', successStateFill);
    };
    successStateFill = function (response) {
        $("#StateId").empty();
        if (response != null) {
            $("#StateId").append($("<option></option>").val('Select').html('Select'));
            if (response != null) {
                $.each(response, function (key, value) {
                    $("#StateId").append($("<option></option>").val(value.Id).attr("text", value.Statename).html(value.Statename));
                });
            }
        }
        $('#StateId').selectpicker('refresh');
    };

     //Bind City dropdown
    bindCityDropdown = function () {
        Common.AjaxJson('POST', "/Staff/GetCityDropDownList", "", 'application/x-www-form-urlencoded', successCityFill);
    };
    successCityFill = function (response) {
        $("#CityId").empty();
        if (response != null) {
            $("#CityId").append($("<option></option>").val('Select').html('Select'));
            if (response != null) {
                $.each(response, function (key, value) {
                    $("#CityId").append($("<option></option>").val(value.Id).attr("text", value.Cityname).html(value.Cityname));
                });
            }
        }
        $('#CityId').selectpicker('refresh');
    };
})();