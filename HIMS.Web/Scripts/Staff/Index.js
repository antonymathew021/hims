﻿(function () { 
    $(function () {
        //bindStatudDropdown();
        bindStaffList();
    });
    //Bind country list
    bindStaffList = function () {
        var tblStaffs = $("#tblStaff").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Staff/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "StaffType", name: "StaffType" },
                { data: "StaffName", name: "StaffName" },
                { data: "DepartmentName", name: "DepartmentName" },
                { data: "StaffMobile", name: "StaffMobile" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='/Staff/Edit/"+ row["Id"] +"'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteStaff(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
        $('#txtSearch').on('keyup', function () {
            tblStaffs.search(this.value).draw();
        });
    }
})();