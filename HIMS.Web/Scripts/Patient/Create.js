﻿(function () {
    $(function () {
        loadFrontImage = function (event) {
            var output = document.getElementById('imgProfilePhoto');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.style.display = "";
        };
    });
})();