﻿(function () {
    $(function () {
        //bindStatudDropdown();
        bindPatientList();
    });
    //Bind country list
    bindPatientList = function () {
        var tblPatient = $("#tblPatient").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Patient/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                {
                    data: "FirstName", name: "FirstName",
                    render: function (data, type, row, meta) {
                        if (type === 'display' || type === 'filter') {
                            return row.FirstName + ' ' + row.LastName
                        }
                        return data;
                    }
                },
                {
                    data: "BirthDate", name: "BirthDate",
                    render: function (data) {
                        return Common.formatDateForDatatable(data);
                    }
                },
                {
                    data: "Regdate", name: "Regdate",
                    render: function (data) {
                        return Common.formatDateForDatatable(data);
                    }
                },
                {
                    data: "Regtime", name: "Regtime",
                    render: function (data) {
                        return Common.formatTimeForDatatable(data);
                    }
                },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data += '<a href="/Patient/Edit/' + row["Id"] + '" class="m-portlet__nav-link btn m-btn  m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-edit"></i></a>';
                            data += "<a href='javascript:void(0);' onclick='deletePatient(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
        $('#txtSearch').on('keyup', function () {
            tblPatient.search(this.value).draw();
        });
    }


    //Delete country
    deletePatient = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/Patient/Delete/' + Id, 'Patient Deleted Successfully', 'Patient Not Deleted', '', 'Are you sure that you want to delete this Patient?');
    }
})();