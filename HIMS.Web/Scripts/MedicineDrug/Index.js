﻿(function () {
    var model = {
        Id: 0,
        DrugName: '',
        DrugDesc: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindMedicineDrugList();
    });
    //Bind country list
    bindMedicineDrugList = function () {
        var tblMedicineDrug = $("#tblMedicineDrug").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/MedicineDrug/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "DrugName", name: "DrugName" },
                { data: "DrugDesc", name: "DrugDesc" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editMedicineDrug(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteMedicineDrug(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/MedicineDrug/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        debugger;
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Open modal
    openMedicineDrugModal = function () {
        $("#Id").val('');
        $("#DrugName").val('');
        $("#DrugDesc").val('');
        bindStatudDropdown();
        $("#MedicineDrugModal").modal("toggle");
    };
    //Validate country
    IsValidMedicineDrug = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#DrugName").val())) {
            $("#lblDrugNameError").html("MedicineDrug code is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#DrugDesc").val())) {
            $("#lblDrugDescError").html("MedicineDrug Name is required");
            isValid = false;
        }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.DrugName = $("#DrugName").val();
            model.DrugDesc = $("#DrugDesc").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/MedicineDrug/Create", model, 'application/x-www-form-urlencoded', successCreatedMedicineDrug);
        }
    }
    successCreatedMedicineDrug = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Work Type Created Successfully");
                $('#tblMedicineDrug').DataTable().ajax.reload();
                $("#MedicineDrugModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Work Type Updated Successfully");
                $('#tblMedicineDrug').DataTable().ajax.reload();
                $("#MedicineDrugModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editMedicineDrug = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/MedicineDrug/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillMedicineDrug);
    }
    successFillMedicineDrug = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#DrugName").val(response.DrugName);
            $("#DrugDesc").val(response.DrugDesc);
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#MedicineDrugModal").modal("toggle");
        }
    }
    //Delete country
    deleteMedicineDrug = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/MedicineDrug/Delete/' + Id, 'Work Type Deleted Successfully', 'Work Type Not Deleted', '', 'Are you sure that you want to delete this MedicineDrug?');
    }
})();