﻿(function () {
    var model = {
        Id: 0,
        WorktypeCode: '',
        WorktypeName: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindWorkTypeList();
    });
    //Bind country list
    bindWorkTypeList = function () {
        var tblWorkType = $("#tblWorkType").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/WorkType/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "WorktypeCode", name: "WorktypeCode" },
                { data: "WorktypeName", name: "WorktypeName" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editWorkType(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteWorkType(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/WorkType/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        debugger;
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Open modal
    openWorkTypeModal = function () {
        $("#Id").val('');
        $("#WorktypeCode").val('');
        $("#WorktypeName").val('');
        bindStatudDropdown();
        $("#WorkTypeModal").modal("toggle");
    };
    //Validate country
    IsValidWorkType = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#WorktypeCode").val())) {
            $("#lblWorktypeCodeError").html("WorkType code is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#WorktypeName").val())) {
            $("#lblWorktypeNameError").html("WorkType Name is required");
            isValid = false;
        }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.WorktypeCode = $("#WorktypeCode").val();
            model.WorktypeName = $("#WorktypeName").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/WorkType/Create", model, 'application/x-www-form-urlencoded', successCreatedWorkType);
        }
    }
    successCreatedWorkType = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Work Type Created Successfully");
                $('#tblWorkType').DataTable().ajax.reload();
                $("#WorkTypeModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Work Type Updated Successfully");
                $('#tblWorkType').DataTable().ajax.reload();
                $("#WorkTypeModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editWorkType = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/WorkType/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillWorkType);
    }
    successFillWorkType = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#WorktypeCode").val(response.WorktypeCode);
            $("#WorktypeName").val(response.WorktypeName);
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#WorkTypeModal").modal("toggle");
        }
    }
    //Delete country
    deleteWorkType = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/WorkType/Delete/' + Id, 'Work Type Deleted Successfully', 'Work Type Not Deleted', '', 'Are you sure that you want to delete this WorkType?');
    }
})();