﻿(function () {
    $(function () {
    });

    $(".phonenumbermask").inputmask("mask", {
        mask: "(999) 999-9999"
    })
    // toggle password visibility
    $('#Password + .fa').on('click', function () {
        $(this).toggleClass('fa-eye-slash').toggleClass('fa-eye'); // toggle our classes for the eye icon
        var type = $("#Password").attr("type");
        if (type == "text") { $("#Password").attr("type", "password"); }
        else { $("#Password").attr("type", "text"); }
    });

    // toggle password visibility
    $('#ConfirmPassword + .fa').on('click', function () {
        $(this).toggleClass('fa-eye-slash').toggleClass('fa-eye'); // toggle our classes for the eye icon
        var type = $("#ConfirmPassword").attr("type");
        if (type == "text") { $("#ConfirmPassword").attr("type", "password"); }
        else { $("#ConfirmPassword").attr("type", "text"); }
    });
    $('#WorkNumber').focusout(function () {
        var str = $('#WorkNumber').val();
        var _worknumber = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s-]/g, '')
        var lenght = _worknumber.length;
        if (lenght != 10) {
            if ($('#WorkNumber').val() != "") {
                $('#WorkNumber').addClass('border-danger');
                $('#WorkNumber').focus();
            }
            else {
                $('#WorkNumber').removeClass('border-danger');
            }
        }
        else if (lenght == 10) {
            $('#WorkNumber').removeClass('border-danger');
        }
    });

    $('#MobileNumber').focusout(function () {
        var str = $('#MobileNumber').val();
        var _worknumber = str.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s-]/g, '')
        var lenght = _worknumber.length;
        if (lenght != 10) {
            if ($('#MobileNumber').val() != "") {
                $('#MobileNumber').addClass('border-danger');
                $('#MobileNumber').focus();
            }
            else {
                $('#MobileNumber').removeClass('border-danger');
            }
        }
        else if (lenght == 10) {
            $('#MobileNumber').removeClass('border-danger');
        }
    });

    $('form').submit(function () {
        var _workNumberstr = $('#WorkNumber').val();
        var _worknumber = _workNumberstr.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s-]/g, '')
        if (_worknumber.length != 0 && _worknumber.length != 10) {
            $('#WorkNumber').addClass('border-danger');
            $('#WorkNumber').focus();
            return false;
        }
        var _mobileNumberstr = $('#MobileNumber').val();
        var _mobileNumber = _mobileNumberstr.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s-]/g, '')
        if (_mobileNumber.length != 0 && _mobileNumber.length != 10) {
            $('#MobileNumber').addClass('border-danger');
            $('#MobileNumber').focus();
            return false;
        }
    });
    $('select').change(function () {
        $('.filter-option').find('.filter-option-inner').css('color', '#575962')
    }).trigger('change');
})();
