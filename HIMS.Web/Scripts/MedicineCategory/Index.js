﻿(function () {
    var model = {
        Id: 0,
        CategoryName: '',
        CategoryDesc: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindMedicineCategoryList();
    });
    //Bind country list
    bindMedicineCategoryList = function () {
        var tblMedicineCategory = $("#tblMedicineCategory").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/MedicineCategory/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "CategoryName", name: "CategoryName" },
                { data: "CategoryDesc", name: "CategoryDesc" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editMedicineCategory(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteMedicineCategory(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/MedicineCategory/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        debugger;
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Open modal
    openMedicineCategoryModal = function () {
        $("#Id").val('');
        $("#CategoryName").val('');
        $("#CategoryDesc").val('');
        bindStatudDropdown();
        $("#MedicineCategoryModal").modal("toggle");
    };
    //Validate country
    IsValidMedicineCategory = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#CategoryName").val())) {
            $("#lblCategoryNameError").html("MedicineCategory code is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#CategoryDesc").val())) {
            $("#lblCategoryDescError").html("MedicineCategory Name is required");
            isValid = false;
        }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.CategoryName = $("#CategoryName").val();
            model.CategoryDesc = $("#CategoryDesc").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/MedicineCategory/Create", model, 'application/x-www-form-urlencoded', successCreatedMedicineCategory);
        }
    }
    successCreatedMedicineCategory = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Medicine Category Created Successfully");
                $('#tblMedicineCategory').DataTable().ajax.reload();
                $("#MedicineCategoryModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Medicine Category Updated Successfully");
                $('#tblMedicineCategory').DataTable().ajax.reload();
                $("#MedicineCategoryModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editMedicineCategory = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/MedicineCategory/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillMedicineCategory);
    }
    successFillMedicineCategory = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#CategoryName").val(response.CategoryName);
            $("#CategoryDesc").val(response.CategoryDesc);
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#MedicineCategoryModal").modal("toggle");
        }
    }
    //Delete country
    deleteMedicineCategory = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/MedicineCategory/Delete/' + Id, 'Medicine Category Deleted Successfully', 'Medicine Category Not Deleted', '', 'Are you sure that you want to delete this MedicineCategory?');
    }
})();