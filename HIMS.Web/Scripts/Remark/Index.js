﻿(function () {
    var model = {
        Id: 0,
        RemarkCode: '',
        RemarkFor: '',
        RemarkName: '',
        Hinremarks: '',
        Gujremarks: '',
        Marremarks: '',
        Arbremarks: ''
    };
    $(function () {
        //bindStatudDropdown();
        //
        bindRemarkList();
    });
    //Bind country list
    bindRemarkList = function () {
        var tblRemark = $("#tblRemark").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Remark/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "RemarkCode", name: "RemarkCode" },
                { data: "RemarkName", name: "RemarkName" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editRemark(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteRemark(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/Remark/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        debugger;
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }

    //Bind Status Dropdown
    bindRemarkFor = function () {
        Common.AjaxJson('POST', "/Remark/GetRemarkForDropDownList", "", 'application / x - www - form - urlencoded', successRemarkFill);
    };
    successRemarkFill = function (response) {
        debugger;
        $("#ddlRemarkFor").empty();
        $.each(response, function (key, value) {
            $("#ddlRemarkFor").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlRemarkFor').selectpicker('refresh');
    }

    //Open modal
    openRemarkModal = function () {
        $("#Id").val('');
        $("#RemarkCode").val('');
        $("#RemarkName").val('');
        $("#Hinremarks").val('');
        $("#Gujremarks").val('');
        $("#Marremarks").val('');
        $("#Arbremarks").val('');
        bindRemarkFor();
        bindStatudDropdown();
        $("#RemarkModal").modal("toggle");
    };
    //Validate country
    IsValidRemark = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#RemarkCode").val())) {
            $("#lblRemarkCodeError").html("Remark code is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#RemarkName").val())) {
            $("#lblRemarkNameError").html("Remark Name is required");
            isValid = false;
        }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.RemarkCode = $("#RemarkCode").val();
            model.RemarkName = $("#RemarkName").val();

            model.Hinremarks = $("#Hinremarks").val();
            model.Gujremarks = $("#Gujremarks").val();

            model.Marremarks = $("#Marremarks").val();
            model.Arbremarks = $("#Arbremarks").val();
            
            model.RemarkFor = $("#ddlRemarkFor").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/Remark/Create", model, 'application/x-www-form-urlencoded', successCreatedRemark);
        }
    }
    successCreatedRemark = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Work Type Created Successfully");
                $('#tblRemark').DataTable().ajax.reload();
                $("#RemarkModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Work Type Updated Successfully");
                $('#tblRemark').DataTable().ajax.reload();
                $("#RemarkModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editRemark = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/Remark/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillRemark);
    }
    successFillRemark = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#RemarkCode").val(response.RemarkCode);
            $("#RemarkName").val(response.RemarkName); 
            $("#Hinremarks").val(response.Hinremarks);
            $("#Gujremarks").val(response.Gujremarks);
            $("#Marremarks").val(response.Marremarks);
            $("#Arbremarks").val(response.Arbremarks);
            $("#ddlActive").val(response.Recstatus);
            $("#ddlRemarkFor").val(response.RemarkFor);
            $('#ddlActive').selectpicker('refresh');
            $('#ddlRemarkFor').selectpicker('refresh');
            $("#RemarkModal").modal("toggle");
        }
    }
    //Delete country
    deleteRemark = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/Remark/Delete/' + Id, 'Work Type Deleted Successfully', 'Work Type Not Deleted', '', 'Are you sure that you want to delete this Remark?');
    }
})();