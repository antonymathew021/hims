﻿(function () {
    $(function () {
        //bindStatudDropdown();
        bindReferenceList();
    });
    //Bind country list
    bindReferenceList = function () {
        var tblReferences = $("#tblReferences").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Referance/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "Reftype", name: "Reftype" },
                { data: "Refcode", name: "Refcode" },
                { data: "Refname", name: "Refname" },
                {
                    data: "BirthDate", name: "BirthDate",
                    render: function (data) {
                        return Common.formatDateForDatatable(data);
                    }
                },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data += '<a href="/Referance/Edit/' + row["Id"] + '" class="m-portlet__nav-link btn m-btn  m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-edit"></i></a>';
                            data += "<a href='javascript:void(0);' onclick='deleteReference(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
        $('#txtSearch').on('keyup', function () {
            tblReferences.search(this.value).draw();
        });
    }

   
    //Delete country
    deleteReference = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/Referance/Delete/' + Id, 'Reference Deleted Successfully', 'Reference Not Deleted', '', 'Are you sure that you want to delete this Reference?');
    }
})();