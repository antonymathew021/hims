﻿(function () {
    $(function () {
        loadFrontImage = function (event) {
            var output = document.getElementById('StaffSignFileImg');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.style.display = "";
        };
    });
})();