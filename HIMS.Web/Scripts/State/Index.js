﻿(function () {
    var model = {
        Id: 0,
        Statecode: '',
        Statename: '',
        CountryMasterId: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindStateList();
    });
    //Bind country list
    bindStateList = function () {
        var tblStates = $("#tblStates").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/State/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "CountryMaster.Countryname", name: "CountryMaster.Countryname" },
                { data: "Statecode", name: "Statecode" },
                { data: "Statename", name: "Statename" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editState(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteState(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
         $('#txtSearch').on('keyup', function () {
            tblStates.search(this.value).draw();
        });
    }
    //Bind Status Dropdown
    bindStatusDropdown = function () {
        Common.AjaxJson('POST', "/Country/GetStatusDropDownList", "", 'application/x-www-form-urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Bind country dropdown
    bindCountryDropdown = function () {
        Common.AjaxJson('POST', "/Country/GetList", "", 'application/x-www-form-urlencoded', successCountryFill);
    };
    successCountryFill = function (response) {
        $("#ddlCountry").empty();
        $("#ddlCountry").append($("<option></option>").val('Select').html('Select'));
        if (response != null) {
            $.each(response, function (key, value) {
                $("#ddlCountry").append($("<option></option>").val(value.Id).attr("text", value.Countryname).html(value.Countryname));
            });
        }
        $('#ddlCountry').selectpicker('refresh');
    };
    //Open modal
    openStateModal = function () {
        $("#Id").val('');
        $("#Statecode").val('');
        $("#Statename").val('');
        bindStatusDropdown();
        bindCountryDropdown();
        $("#StateModal").modal("toggle");
    };
    //Validate country
    IsValidState = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#ddlCountry").val()) || $("#ddlCountry").val() == "Select" || typeof $('#ddlCountry').val() == typeof undefined) {
            $("#lblCountryError").text("Country is required");
            isValid = false;
        }
        else { $("#lblCountryError").text("")}
        if (Common.IsNullOrEmpty($("#Statecode").val())) {
            $("#lblStatecodeError").text("State code is required");
            isValid = false;
        } else { $("#lblStatecodeError").text("") }
        if (Common.IsNullOrEmpty($("#Statename").val())) {
            $("#lblStatenameError").text("State is required");
            isValid = false;
        } else { $("#lblStatenameError").text("") }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.CountryMasterId = $("#ddlCountry").val();
            model.Statecode = $("#Statecode").val();
            model.Statename = $("#Statename").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/State/Create", model, 'application/x-www-form-urlencoded', successCreatedState);
        }
    }
    successCreatedState = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("State Created Successfully");
                $('#tblStates').DataTable().ajax.reload();
                $("#StateModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("State Updated Successfully");
                $('#tblStates').DataTable().ajax.reload();
                $("#StateModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editState = function (Id, btn) {
        bindCountryDropdown();
        bindStatusDropdown();
        Common.AjaxJson("Get", "/State/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillState);
    }
    successFillState = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#Statecode").val(response.Statecode);
            $("#Statename").val(response.Statename);
            $("#ddlCountry").val(response.CountryMasterId);
            $('#ddlCountry').selectpicker('refresh');
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#StateModal").modal("toggle");
        }
    }
    //Delete country
    deleteState = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/State/Delete/' + Id, 'State Deleted Successfully', 'State Not Deleted', '', 'Are you sure that you want to delete this State?');
    }
})();