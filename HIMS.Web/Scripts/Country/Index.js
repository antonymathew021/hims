﻿(function () {
    var model = {
        Id: 0,
        Countrycode: '',
        Countryname: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindCountryList();
        
    });
    //Bind country list
    bindCountryList = function () {
        var tblCountries = $("#tblCountries").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Country/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            }, 
            columns: [
                { data: "Countrycode", name: "Countrycode" },
                { data: "Countryname", name: "Countryname" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editCountry(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteCountry(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
         $('#txtSearch').on('keyup', function () {
            tblCountries.search(this.value).draw();
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/Country/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Open modal
    openCountryModal = function () {
        $("#Id").val('');
        $("#Countrycode").val('');
        $("#Countryname").val('');
        bindStatudDropdown();
        $("#CountryModal").modal("toggle");
    };
    //Validate country
    IsValidCountry = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#Countrycode").val())) {
            $("#lblCountrycodeError").val("Country code is required");
            isValid = false;
        } else { $("#lblCountrycodeError").val(""); }
        if (Common.IsNullOrEmpty($("#Countryname").val())) {
            $("#lblCountrynameError").val();
            isValid = false;
        } else { $("#lblCountrynameError").val(""); }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.Countrycode = $("#Countrycode").val();
            model.Countryname = $("#Countryname").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/Country/Create", model, 'application/x-www-form-urlencoded', successCreatedCountry);
        }
    }
    successCreatedCountry = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Country Created Successfully");
                $('#tblCountries').DataTable().ajax.reload();
                $("#CountryModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Country Updated Successfully");
                $('#tblCountries').DataTable().ajax.reload();
                $("#CountryModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editCountry = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/Country/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillCountry);
    }
    successFillCountry = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#Countrycode").val(response.Countrycode);
            $("#Countryname").val(response.Countryname);
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#CountryModal").modal("toggle");
        }
    }
    //Delete country
    deleteCountry = function (Id, btn) {
        if (Common.DeleteFromTable(Id, btn, '/Country/Delete/' + Id, 'Country Deleted Successfully', 'Country Not Deleted', '', 'Are you sure that you want to delete this Country?')) {
            $('#tblCountries').DataTable().ajax.reload();
            return true;
        }
        else {
            return false;
        }
    }
})();