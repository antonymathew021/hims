﻿(function () {
    var model = {
        Id: 0,
        Citycode: '',
        Cityname: '',
        CountryMasterId: '',
        StateMasterId: '',
        Recstatus: ''
    };
    $(function () {
        bindCityList();
    });
    bindCityList = function () {
        var tblCity = $("#tblCities").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/City/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "CountryMaster.Countryname", name: "CountryMaster.Countryname" },
                { data: "StateMaster.Statename", name: "StateMaster.Statename" },
                { data: "Citycode", name: "Citycode" },
                { data: "Cityname", name: "Cityname" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editCity(" + row["Id"] + ",this);'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteCity(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
        $('#txtSearch').on('keyup', function () {
            tblCity.search(this.value).draw();
        });
    }
    //Bind Status Dropdown
    bindStatusDropdown = function () {
        Common.AjaxJson('POST', "/Country/GetStatusDropDownList", "", 'application/x-www-form-urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Bind country dropdown
    bindCountryDropdown = function () {
        Common.AjaxJson('POST', "/Country/GetList", "", 'application/x-www-form-urlencoded', successCountryFill);
    };
    successCountryFill = function (response) {
        debugger
        $("#ddlCountry").empty();
        if (response != null) {
            $("#ddlCountry").append($("<option></option>").val('Select').html('Select'));
            if (response != null) {
                $.each(response, function (key, value) {
                    $("#ddlCountry").append($("<option></option>").val(value.Id).attr("text", value.Countryname).html(value.Countryname));
                });
            }
        }
        $('#ddlCountry').selectpicker('refresh');
    };
    //Bind state dropdown
    bindStateDropdown = function () {
        Common.AjaxJson('POST', "/State/GetList", "", 'application/x-www-form-urlencoded', successStateFill);
    };
    successStateFill = function (response) {
        $("#ddlState").empty();
        if (response != null) {
            $("#ddlState").append($("<option></option>").val('Select').html('Select'));
            if (response != null) {
                $.each(response, function (key, value) {
                    $("#ddlState").append($("<option></option>").val(value.Id).attr("text", value.Statename).html(value.Statename));
                });
            }
        }
        $('#ddlState').selectpicker('refresh');
    };
    //Open modal
    openCityModal = function () {
        $("#Id").val('');
        $("#Citycode").val('');
        $("#Cityname").val('');
        bindCountryDropdown();
        bindStateDropdown();
        bindStatusDropdown();
        $("#CityModal").modal("toggle");
    };
    //Validate country
    IsValidCity = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#ddlCountry").val()) || $("#ddlCountry").val() == "Select" || typeof $('#ddlCountry').val() == typeof undefined) {
            $("#lblCountryError").text("Country is required");
            isValid = false;
        }
        else { $("#lblCountryError").text("") }
        if (Common.IsNullOrEmpty($("#ddlState").val()) || $("#ddlState").val() == "Select" || typeof $('#ddlState').val() == typeof undefined) {
            $("#lblStateError").text("State is required");
            isValid = false;
        }
        else { $("#lblStateError").text("") }
        if (Common.IsNullOrEmpty($("#Citycode").val())) {
            $("#lblCitycodeError").text("City code is required");
            isValid = false;
        } else { $("#lblCitycodeError").text("") }
        if (Common.IsNullOrEmpty($("#Cityname").val())) {
            $("#lblCitynameError").text("City is required");
            isValid = false;
        } else { $("#lblCitynameError").text("") }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.CountryMasterId = $("#ddlCountry").val();
            model.StateMasterId = $("#ddlState").val();
            model.Citycode = $("#Citycode").val();
            model.Cityname = $("#Cityname").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/City/Create", model, 'application/x-www-form-urlencoded', successCreatedCity);
        }
    }
    successCreatedCity = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("City Created Successfully");
                $('#tblCities').DataTable().ajax.reload();
                $("#CityModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("City Updated Successfully");
                $('#tblCities').DataTable().ajax.reload();
                $("#CityModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editCity = function (Id, btn) {
        bindCountryDropdown();
        bindStateDropdown();
        bindStatusDropdown();
        Common.AjaxJson("Get", "/City/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillCity);
    }
    successFillCity = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#Citycode").val(response.Citycode);
            $("#Cityname").val(response.Cityname);
            $("#ddlCountry").val(response.CountryMasterId);
            $('#ddlCountry').selectpicker('refresh');
            $("#ddlState").val(response.StateMasterId);
            $('#ddlState').selectpicker('refresh');
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#CityModal").modal("toggle");
        }
    }
    //Delete city
    deleteCity = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/City/Delete/' + Id, 'City Deleted Successfully', 'City Not Deleted', '', 'Are you sure that you want to delete this City?');
    }
})();