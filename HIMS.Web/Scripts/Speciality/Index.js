﻿(function () {
    var model = {
        Id: 0,
        SpecialityCode: '',
        SpecialityName: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindSpecialityList();
    });
    //Bind country list
    bindSpecialityList = function () {
        var tblCountries = $("#tblCountries").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Speciality/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "SpecialityCode", name: "SpecialityCode" },
                { data: "SpecialityName", name: "SpecialityName" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editSpeciality(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteSpeciality(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/Speciality/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        debugger;
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Open modal
    openSpecialityModal = function () {
        $("#Id").val('');
        $("#SpecialityCode").val('');
        $("#SpecialityName").val('');
        bindStatudDropdown();
        $("#SpecialityModal").modal("toggle");
    };
    //Validate country
    IsValidSpeciality = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#SpecialityCode").val())) {
            $("#lblSpecialitycodeError").val("Speciality code is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#SpecialityName").val())) {
            $("#lblSpecialitynameError").val();
            isValid = false;
        }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.SpecialityCode = $("#SpecialityCode").val();
            model.SpecialityName = $("#SpecialityName").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/Speciality/Create", model, 'application/x-www-form-urlencoded', successCreatedSpeciality);
        }
    }
    successCreatedSpeciality = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Speciality Created Successfully");
                $('#tblCountries').DataTable().ajax.reload();
                $("#SpecialityModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Speciality Updated Successfully");
                $('#tblCountries').DataTable().ajax.reload();
                $("#SpecialityModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editSpeciality = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/Speciality/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillSpeciality);
    }
    successFillSpeciality = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#SpecialityCode").val(response.SpecialityCode);
            $("#SpecialityName").val(response.SpecialityName);
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#SpecialityModal").modal("toggle");
        }
    }
    //Delete country
    deleteSpeciality = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/Speciality/Delete/' + Id, 'Speciality Deleted Successfully', 'Speciality Not Deleted', '', 'Are you sure that you want to delete this Speciality?');
    }
})();