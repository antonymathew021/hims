﻿(function () {
    $(function () {
        //bindStatudDropdown();
        bindTieUpList();
    });
    //Bind country list
    bindTieUpList = function () {
        var tblTieUps = $("#tblTieUps").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/TieUp/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "CompanyName", name: "CompanyName" },
                { data: "ContactPerson", name: "ContactPerson" },
                { data: "Mobile", name: "Mobile" },
                {
                    data: "Status", name: "Status",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Status"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Status"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data += '<a href="/TieUp/Edit/' + row["Id"] + '" class="m-portlet__nav-link btn m-btn  m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-edit"></i></a>';
                            data += "<a href='javascript:void(0);' onclick='deleteTieUp(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
        $('#txtSearch').on('keyup', function () {
            tblTieUps.search(this.value).draw();
        });
    }


    //Delete country
    deleteTieUp = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/TieUp/Delete/' + Id, 'TieUp Deleted Successfully', 'TieUp Not Deleted', '', 'Are you sure that you want to delete this TieUp?');
    }
})();