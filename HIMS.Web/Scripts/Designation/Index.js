﻿(function () {
    var model = {
        Id: 0,
        DesignationCode: '',
        DesignationName: '',
        Recstatus: ''
    };
    $(function () {
        //bindStatudDropdown();
        bindDesignationList();
    });
    //Bind country list
    bindDesignationList = function () {
        var tblDesignation = $("#tblDesignation").DataTable({
            language: {
                lengthMenu: "Display _MENU_"
            },
            "responsive": true,
            "dom": "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "/Designation/Index/",
                type: 'POST',
            },
            "language": {
                "search": "",
                "searchPlaceholder": "Search..."
            },
            columns: [
                { data: "DesignationCode", name: "DesignationCode" },
                { data: "DesignationName", name: "DesignationName" },
                {
                    data: "Recstatus", name: "Recstatus",
                    render: function (data, type, row, meta) {
                        if (type === 'display') {
                            if (row["Recstatus"] == "Active") {
                                data = '<span class="badge badge-success">Active</span>';
                            }
                            else if (row["Recstatus"] == "Inactive") {
                                data = '<span class="badge badge-warning">In Active</span>';
                            }
                        }
                        return data;
                    }
                },
                {
                    data: null,
                    render: function (row, type, val, meta) {
                        if (type === 'display') {
                            data = "";
                            data = "<a href='javascript:void(0);' onclick='editDesignation(" + row["Id"] + ",this)'><i class='la la-edit'></i></a>";
                            data += "<a href='javascript:void(0);' onclick='deleteDesignation(" + row["Id"] + ",this);'><i class='la la-trash'></i></a>";
                        }
                        return data;
                    },
                    orderable: false
                }
            ],
            stateSave: false,
            stateDuration: 60 * 10
        });
    }
    //Bind Status Dropdown
    bindStatudDropdown = function () {
        Common.AjaxJson('POST', "/Designation/GetStatusDropDownList", "", 'application / x - www - form - urlencoded', successStatusFill);
    };
    successStatusFill = function (response) {
        debugger;
        $("#ddlActive").empty();
        $.each(response, function (key, value) {
            $("#ddlActive").append($("<option></option>").val(value).attr("text", value).html(value));
        });
        $('#ddlActive').selectpicker('refresh');
    }
    //Open modal
    openDesignationModal = function () {
        $("#Id").val('');
        $("#DesignationCode").val('');
        $("#DesignationName").val('');
        bindStatudDropdown();
        $("#DesignationModal").modal("toggle");
    };
    //Validate country
    IsValidDesignation = function () {
        var isValid = true;
        if (Common.IsNullOrEmpty($("#DesignationCode").val())) {
            $("#lblDesignationCodeError").val("Designation code is required");
            isValid = false;
        }
        if (Common.IsNullOrEmpty($("#DesignationName").val())) {
            $("#lblDesignationNameError").val();
            isValid = false;
        }
        if (isValid) {
            if (Common.IsNullOrEmpty($("#Id").val())) {
                model.Id = 0;
            }
            else {
                model.Id = $("#Id").val();
            }
            model.DesignationCode = $("#DesignationCode").val();
            model.DesignationName = $("#DesignationName").val();
            model.Recstatus = $("#ddlActive").val();
            Common.AjaxJson('POST', "/Designation/Create", model, 'application/x-www-form-urlencoded', successCreatedDesignation);
        }
    }
    successCreatedDesignation = function (response) {
        if (response != null) {
            if (response.status == "Created") {
                Common.SucessToaster("Designation Created Successfully");
                $('#tblDesignation').DataTable().ajax.reload();
                $("#DesignationModal").modal("hide");
            }
            else if (response.status = "Updated") {
                Common.SucessToaster("Designation Updated Successfully");
                $('#tblDesignation').DataTable().ajax.reload();
                $("#DesignationModal").modal("hide");
            }
            else {
                Common.ErrorToaster("Failed To Create/Update");
            }
        }
        else {
            Common.ErrorToaster("Something Went Wrong");
        }
    }
    //Edit country
    editDesignation = function (Id, btn) {
        bindStatudDropdown();
        Common.AjaxJson("Get", "/Designation/Edit/" + Id, "", "application/x-www-form-urlencoded", successFillDesignation);
    }
    successFillDesignation = function (response) {
        if (response != null) {
            $("#Id").val(response.Id);
            $("#DesignationCode").val(response.DesignationCode);
            $("#DesignationName").val(response.DesignationName);
            $("#ddlActive").val(response.Recstatus);
            $('#ddlActive').selectpicker('refresh');
            $("#DesignationModal").modal("toggle");
        }
    }
    //Delete country
    deleteDesignation = function (Id, btn) {
        return Common.DeleteFromTable(Id, btn, '/Designation/Delete/' + Id, 'Designation Deleted Successfully', 'Designation Not Deleted', '', 'Are you sure that you want to delete this Designation?');
    }
})();