﻿(function () {
    var model = {
        Id: 0,
        DoctorId: 0,
        WeekType: 0,
        MorningTimeFrom: '',
        MorningTimeTo: '',
        AfterNoonTimeFrom: '',
        AfterNoonTimeTo: '',
        EveningTimeFrom: '',
        EveningTimeTo: '',
        Recstatus: 'Active'
    };
    $(function () {

    });

    IsValidDoctorSchedule = function () {
        isValid = true;
        if (Common.IsNullOrEmpty($("#DoctorId").val()) || $("#DoctorId").val() == 'Select') {
            $("#lblDoctorIdError").html("Doctor name is required");
            isValid = false;
        }
        else {
            $("#lblDoctorIdError").html("");
        }
        if (Common.IsNullOrEmpty($("#WeekType").val()) || $("#WeekType").val() == 'Select') {
            $("#lblWeekTypeError").html("Day is required");
            isValid = false;
        }
        else {
            $("#lblWeekTypeError").html("");
        }
        if (isValid == true) {
            model.DoctorId = $("#DoctorId").val();
            model.WeekType = $("#WeekType").val();
            model.MorningTimeFrom = $("#MorningTimeFrom").val();
            model.MorningTimeTo = $("#MorningTimeTo").val();
            model.AfterNoonTimeFrom = $("#AfterNoonTimeFrom").val();
            model.AfterNoonTimeTo = $("#AfterNoonTimeTo").val();
            model.EveningTimeFrom = $("#EveningTimeFrom").val();
            model.EveningTimeTo = $("#EveningTimeTo").val();
            saveData(model);
        }
        else return false;
    }
    updateDoctorScheduleDetail = function (index) {
        if ($(index).val() != "Select" && Common.IsNotNullOrEmpty($(index).val())) {
            var doctorId = $("#DoctorId").children("option:selected").val();
            Common.AjaxJson('POST', "/DoctorOpdScheduler/GetListByDoctor/" + doctorId, "", 'application/x-www-form-urlencoded', fillScheduledTable);
        }
        else {
            $("#divTable").empty();
        }
    }

    fillScheduledTable = function (response) {
        $("#divTable").empty();
        var tableContent = '';
        tableContent += '<table class="table table-striped- table-bordered table-hover table-checkable table-striped responsive no-wrap" id="tblDoctorSchedule">';
        tableContent += "<thead><tr>" +
            "<th>Days</th>" +
            "<th>Morning From</th>" +
            "<th>Morning To</th>" +
            "<th>AfterNoon From</th>" +
            "<th>AfterNoon To</th>" +
            "<th>Evening From</th>" +
            "<th>Evening To</th>" +
            "</tr></thead><tbody>";
        if (response != null) {
            for (var i = 0; i < Object.keys(response).length; i++) {
                var weekType = response[i].WeekType;
                var morningTimeFrom = Common.IsNotNullOrEmpty(response[i].MorningTimeFrom) ? Common.formatTimeForDatatable(response[i].MorningTimeFrom) : "";
                var morningTimeTo = Common.IsNotNullOrEmpty(response[i].MorningTimeTo) ? Common.formatTimeForDatatable(response[i].MorningTimeTo) : "";
                var afterNoonTimeFrom = Common.IsNotNullOrEmpty(response[i].AfterNoonTimeFrom) ? Common.formatTimeForDatatable(response[i].AfterNoonTimeFrom) : "";
                var afterNoonTimeTo = Common.IsNotNullOrEmpty(response[i].AfterNoonTimeTo) ? Common.formatTimeForDatatable(response[i].AfterNoonTimeTo) : "";
                var eveningTimeFrom = Common.IsNotNullOrEmpty(response[i].EveningTimeFrom) ? Common.formatTimeForDatatable(response[i].EveningTimeFrom) : "";
                var eveningTimeTo = Common.IsNotNullOrEmpty(response[i].EveningTimeTo) ? Common.formatTimeForDatatable(response[i].EveningTimeTo) : "";
                tableContent += "<tr>" +
                    "<td class='bold'>" + weekType + "</td>" +
                    "<td>" + morningTimeFrom + "</td>" +
                    "<td>" + morningTimeTo + "</td>" +
                    "<td>" + afterNoonTimeFrom + "</td>" +
                    "<td>" + afterNoonTimeTo + "</td>" +
                    "<td>" + eveningTimeFrom + "</td>" +
                    "<td>" + eveningTimeTo + "</td>" +
                    "</tr>";
            }
            tableContent += "</tbody></table>";
            $(tableContent).appendTo("#divTable");
        }
    }
    saveData = function (model) {
        if (model != null) {
            Common.AjaxJson('POST', "/DoctorOpdScheduler/Index", model, 'application / x - www - form - urlencoded', successDoctorScheduled);
        }
    }
    successDoctorScheduled = function (response) {
        if (response != null) {
            fillScheduledTable(response);
        }
    }
    fillTime = function (index) {
        if ($(index).val() != "Select" && Common.IsNotNullOrEmpty($(index).val()) && $("#DoctorId").val() != "Select" && Common.IsNotNullOrEmpty($("#DoctorId").val())) {
            var doctorId = $("#DoctorId").children("option:selected").val();
            Common.AjaxJson('POST', "/DoctorOpdScheduler/GetListByDoctor/" + doctorId, "", 'application/x-www-form-urlencoded', fillTimeSuccess);
        }
        else {
            $("#MorningTimeFrom").val("");
            $("#MorningTimeTo").val("");
            $("#AfterNoonTimeFrom").val("");
            $("#AfterNoonTimeTo").val("");
            $("#EveningTimeFrom").val("");
            $("#EveningTimeTo").val("");
        }
    }
    fillTimeSuccess = function (response) {
        if (response != null) {
            var result = $(response).filter(function (i, n) { return n.WeekType === $("#WeekType").children("option:selected").text() });
            if (result != null) {
                if (Common.IsNotNullOrEmpty(result[0].MorningTimeFrom)) {
                    $("#MorningTimeFrom").timepicker('setTime', Common.formatTime(result[0].MorningTimeFrom));
                }
                else {
                    $("#MorningTimeFrom").val("");
                }
                if (Common.IsNotNullOrEmpty(result[0].MorningTimeTo)) {
                    $("#MorningTimeTo").timepicker('setTime', Common.formatTime(result[0].MorningTimeTo));
                }
                else {
                    $("#MorningTimeTo").val("");
                }
                if (Common.IsNotNullOrEmpty(result[0].AfterNoonTimeFrom)) {
                    $("#AfterNoonTimeFrom").timepicker('setTime', Common.formatTime(result[0].AfterNoonTimeFrom));
                }
                else {
                    $("#AfterNoonTimeFrom").val("");
                }
                if (Common.IsNotNullOrEmpty(result[0].AfterNoonTimeTo)) {
                    $("#AfterNoonTimeTo").timepicker('setTime', Common.formatTime(result[0].AfterNoonTimeTo));
                }
                else {
                    $("#AfterNoonTimeTo").val("");
                }
                if (Common.IsNotNullOrEmpty(result[0].EveningTimeFrom)) {
                    $("#EveningTimeFrom").timepicker('setTime', Common.formatTime(result[0].EveningTimeFrom));
                }
                else {
                    $("#EveningTimeFrom").val("");
                }
                if (Common.IsNotNullOrEmpty(result[0].EveningTimeTo)) {
                    $("#EveningTimeTo").timepicker('setTime', Common.formatTime(result[0].EveningTimeTo));
                }
                else {
                    $("#EveningTimeTo").val("");
                }
            }
        }
    }
})();