var DatatablesSearchOptionsAdvancedSearch = function () {
    $.fn.dataTable.Api.register("column().title()", function () {
        return $(this.header()).text().trim()
    });
    return {
        init: function () {
            var a;
            a = $("#m_table_1").DataTable({
                responsive: !0,
                dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
                //lengthMenu: [5, 10, 25, 50],
                //pageLength: 10,
                language: {
                    lengthMenu: "Display _MENU_"
                },
                searchDelay: 500
            })
        }
    }
}();
jQuery(document).ready(function () {
    DatatablesSearchOptionsAdvancedSearch.init()
});